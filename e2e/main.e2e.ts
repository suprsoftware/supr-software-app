import { expect } from 'chai';
import { SpectronClient, SpectronWindow, SpectronWebContents } from 'spectron';
import commonSetup from './common-setup';

describe('Supr Software App', function () {

  commonSetup.apply(this);

  let client: SpectronClient;
  let browserWindow: SpectronWindow;
  let webContents: SpectronWebContents;

  beforeEach(function() {
    client = this.app.client;
    browserWindow = this.app.browserWindow;
    webContents = this.app.webContents;
  });

  it('creates initial windows', async function () {
    const count = await client.getWindowCount();
    expect(count).to.equal(1);
  });

  it('should be size 1300 x 700', async function () {
    const rectangle = await browserWindow.getBounds();
    expect(rectangle.height).to.equal(700);
    expect(rectangle.width).to.equal(1300);
  });

  it('should display corrent title in webContents', async function () {
    const title = await client.$('.title');
    const text = await title.getText();
    expect(text).to.equal('Supr Software');
  });

  it('should display correct title in tab', async function() {
    const title = await webContents.getTitle();
    expect(title).to.equal('Supr Software');
  });

  it('should use file protocol', async function() {
    const url = new URL((await webContents.getURL()));
    const protocol = url.protocol;
    expect(protocol).to.equal('file:');
  });

  it('should not allow dev tools', async function() {
    let isOpened = await webContents.isDevToolsOpened();
    expect(isOpened).to.be.false;
    await webContents.openDevTools();
    isOpened = await webContents.isDevToolsOpened();
    expect(isOpened).to.be.false;
  });

  it('should not display menu bar', async function() {
    const isMenuVisible = await browserWindow.isMenuBarVisible();
    expect(isMenuVisible).to.be.false;
  });

  it('should minimize', async function() {
    const isMinimizable = await browserWindow.isMinimizable();
    expect(isMinimizable).to.be.true;
  });

  it('should maximize', async function() {
    const isMaximizable = await browserWindow.isMaximizable();
    expect(isMaximizable).to.be.true;
  });

  it('should full screen', async function() {
    const isFullScreenable = await browserWindow.isFullScreenable();
    expect(isFullScreenable).to.be.true;
  });

  it('should resize', async function() {
    const isResizable = await browserWindow.isResizable();
    expect(isResizable).to.be.true;
  });

  it('should close', async function() {
    const isClosable = await browserWindow.isClosable();
    expect(isClosable).to.be.true;
  });

});
