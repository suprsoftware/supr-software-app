"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Logger = void 0;
var winston = require("winston");
require("winston-daily-rotate-file");
var _a = winston.format, combine = _a.combine, timestamp = _a.timestamp, printf = _a.printf, colorize = _a.colorize;
var customLogFormat = printf(function (_a) {
    var level = _a.level, message = _a.message, timestamp = _a.timestamp, process = _a.process;
    return timestamp + " [" + level + "] [" + process + "]: " + message;
});
var args = process.argv.slice(1), serve = args.some(function (val) { return val === '--serve'; });
;
;
var Logger = /** @class */ (function () {
    function Logger(logger) {
        this.consoleTransport = this.createNewConsoleTransport();
        this.logger = logger || this.createNewLogger();
    }
    ;
    Logger.prototype.createNewConsoleTransport = function () {
        return new winston.transports.Console({
            format: combine(timestamp(), colorize(), customLogFormat),
            level: serve ? 'debug' : 'info'
        });
    };
    ;
    Logger.prototype.createNewLogger = function () {
        return winston.createLogger({
            transports: [
                this.consoleTransport,
            ]
        });
    };
    ;
    /**
     * Adds daily file rotation to the logger transport
     * @param {String} dirname - absolute directory path
     */
    Logger.prototype.addDailyFileRotate = function (dirname) {
        this.logger.add(new winston.transports.DailyRotateFile({
            filename: "supr-software-%DATE%.log",
            dirname: dirname,
            maxSize: '8m',
            maxFiles: '7d',
            format: combine(timestamp(), customLogFormat),
            level: 'debug',
        }));
    };
    ;
    /**
     * Creates a child logger with metadata overrides
     * @param {ChildOptions} options - metadata overrides
     * @return {Logger}
     */
    Logger.prototype.child = function (options) {
        return new Logger(this.logger.child(options));
    };
    ;
    Logger.prototype.debug = function (message) {
        this.logger.debug(message);
    };
    ;
    Logger.prototype.error = function (message) {
        this.logger.error(message);
    };
    ;
    Logger.prototype.info = function (message) {
        this.logger.info(message);
    };
    ;
    Logger.prototype.removeConsoleTransport = function () {
        this.logger.remove(this.consoleTransport);
    };
    ;
    return Logger;
}());
exports.Logger = Logger;
;
var mainLogger = (new Logger()).child({ process: 'main' });
global['logger'] = mainLogger;
exports.default = mainLogger;
//# sourceMappingURL=index.js.map