import * as winston from 'winston';
import 'winston-daily-rotate-file';

const { combine, timestamp, printf, colorize } = winston.format;

const customLogFormat = printf(({level, message, timestamp, process}) => {
  return `${timestamp} [${level}] [${process}]: ${message}`;
});

const args = process.argv.slice(1),
  serve = args.some(val => val === '--serve');

export interface ChildOptions {
  process: string,
};

export interface Logger {
  logger: winston.Logger;
  consoleTransport: winston.transports.ConsoleTransportInstance;

  createNewConsoleTransport(): winston.transports.ConsoleTransportInstance;
  createNewLogger(): winston.Logger;
  addDailyFileRotate(dirname: string): void;
  child(options: ChildOptions): Logger;
  debug(message: string): void;
  error(message: string): void;
  info(message: string): void;
  removeConsoleTransport(): void;
};

export class Logger implements Logger {
  constructor(logger?: winston.Logger) {
    this.consoleTransport = this.createNewConsoleTransport();
    this.logger = logger || this.createNewLogger();
  };

  createNewConsoleTransport() {
    return new winston.transports.Console({
      format: combine(
        timestamp(),
        colorize(),
        customLogFormat,
      ),
      level: serve ? 'debug' : 'info'
    });
  };

  createNewLogger() {
    return winston.createLogger({
      transports: [
        this.consoleTransport,
      ]
    });
  };

  /**
   * Adds daily file rotation to the logger transport
   * @param {String} dirname - absolute directory path
   */
  addDailyFileRotate(dirname: string) {
    this.logger.add(new winston.transports.DailyRotateFile({
      filename: `supr-software-%DATE%.log`,
      dirname,
      maxSize: '8m',
      maxFiles: '7d',
      format: combine(
        timestamp(),
        customLogFormat,
      ),
      level: 'debug',
    }));
  };

  /**
   * Creates a child logger with metadata overrides
   * @param {ChildOptions} options - metadata overrides
   * @return {Logger}
   */
  child(options: ChildOptions) {
    return new Logger(this.logger.child(options));
  };

  debug(message: string) {
    this.logger.debug(message);
  };

  error(message: string) {
    this.logger.error(message);
  };

  info(message: string) {
    this.logger.info(message);
  };

  removeConsoleTransport() {
    this.logger.remove(this.consoleTransport);
  };
};
const mainLogger = (new Logger()).child({process: 'main'});
global['logger'] = mainLogger;
export default mainLogger;