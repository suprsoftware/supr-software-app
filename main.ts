import {app, BrowserWindow, ipcMain, session} from 'electron';
import * as path from 'path';
import * as url from 'url';
import * as fs from 'fs-extra';

import logger from './lib/logger';

let mainWindow: BrowserWindow = null;
const args = process.argv.slice(1),
  serve = args.some(val => val === '--serve');

function createMainWindow(): BrowserWindow {
  mainWindow = new BrowserWindow({
    width: 1300,
    height: 700,
    minWidth: 1300,
    minHeight: 700,
    title: 'Supr Software',
    frame: false,
    webPreferences: {
      nodeIntegration: true,
      nodeIntegrationInSubFrames: true,
      allowRunningInsecureContent: true,
      webSecurity: false,
      enableRemoteModule: true,
      devTools: serve,
      session: session.defaultSession
    }
  });

  if (serve) {
    mainWindow.webContents.openDevTools();
    require('electron-reload')(__dirname, {
      electron: require(`${__dirname}/node_modules/electron`)
    });
    mainWindow.loadURL('http://localhost:4200');
  } else {
    mainWindow.loadURL(url.format({
      pathname: path.join(__dirname, 'dist/index.html'),
      protocol: 'file:',
      slashes: true,
    }));
  };

  // event listeners

  mainWindow.on('close', () => {
    logger.removeConsoleTransport();
  });

  mainWindow.on('closed', () => {
    mainWindow = null;
  });

  return mainWindow;
};

function initAppDataDir() {
  const appDataPath = app.getPath('userData');
  initLogDir(appDataPath);
};

function initLogDir(appDataPath: string) {
  const logsDir = path.join(appDataPath, 'logs');
  if (!fs.existsSync(logsDir)) {
    fs.mkdirsSync(logsDir);
    logger.debug('created log directory in userData');
  };
  logger.addDailyFileRotate(logsDir);
  logger.debug(`Logs Directory ${logsDir}`);
  logger.debug('logger initialized');
};

try {
  // This method will be called when Electron has finished
  // initialization and is ready to create browser windows.
  // Some APIs can only be used after this event occurs.
  // Added 400 ms to fix the black background issue while using transparent window. More detais at https://github.com/electron/electron/issues/15947
  app.on('ready', () => {
    initAppDataDir();
    
    setTimeout(createMainWindow, 400);
  });

  // Quit when all windows are closed.
  app.on('window-all-closed', () => {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
      app.quit();
    }
  });

  app.on('activate', () => {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (mainWindow === null) {
      createMainWindow();
    }
  });

  // ipc
  ipcMain.on('mainWindow::ping!', (event, args) => {
    event.reply('pong');
  })

  ipcMain.on('mainWindow::close', () => {
    if (mainWindow.webContents.isDevToolsOpened()) mainWindow.webContents.closeDevTools();
    mainWindow.close();
  });

  ipcMain.on('mainWindow::minimize', () => {
    mainWindow.minimize();
  });

  ipcMain.on('mainWindow::maximize', () => {
    if (mainWindow.isMaximized()) return mainWindow.unmaximize();
    mainWindow.maximize();
  });

} catch (e) {
  // Catch Error
  // throw e;
}
