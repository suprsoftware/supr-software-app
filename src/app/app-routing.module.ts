import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './shared/components';
import { DashboardComponent } from './dashboard/dashboard.component';

import { LoginRoutingModule } from './login/login-routing.module';
import { DashboardRoutingModule } from './dashboard/dashboard-routing.module'

import { AuthGuard } from './auth.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: '**',
    component: PageNotFoundComponent
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    canLoad: [ AuthGuard ],
    canActivate: [ AuthGuard ],
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { useHash: true, relativeLinkResolution: 'legacy' }),
    LoginRoutingModule,
    DashboardRoutingModule,
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
