import { TestBed } from '@angular/core/testing';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { TestScheduler } from 'rxjs/testing';
import { AuthGuard } from './auth.guard';
import { AuthService } from 'app/core/services/auth/auth.service';

describe('AuthGuard', () => {
  let guard: AuthGuard;
  let authService: jasmine.SpyObj<AuthService>;

  beforeEach(() => {
    const authServiceSpy = jasmine.createSpyObj('AuthService', [
      'isActiveKeyValid',
      'redirectToLogin',
    ]);
    TestBed.configureTestingModule({
      imports: [],
      providers: [
        {provide: AuthService, useValue: authServiceSpy}
      ]
    });
    guard = TestBed.inject(AuthGuard);
    authService = TestBed.inject(AuthService) as jasmine.SpyObj<AuthService>;
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });

  describe('canActivate', () => {
    it('should return true if isActiveKeyValid', (done) => {
      authService.isActiveKeyValid.and.returnValue(of(true));
      spyOn(guard, 'canActivate').and.returnValue(authService.isActiveKeyValid());
      (guard.canActivate(null, null) as Observable<boolean>).subscribe((valid) => {
        expect(valid).toBeTrue();
        expect(authService.redirectToLogin).toHaveBeenCalledTimes(0);
        done();
      });
    });
    it('should navigate and return false if isActiveKeyValid', (done) => {
      authService.isActiveKeyValid.and.returnValue(of(false));
      spyOn(guard, 'canActivate')
        .and.returnValue(authService.isActiveKeyValid())
        .and.callThrough();
      (guard.canActivate(null, null) as Observable<boolean>).subscribe((valid) => {
        expect(valid).toBeFalse();
        expect(authService.redirectToLogin).toHaveBeenCalledTimes(1);
        done();
      });
    });
  });

  describe('canActivateChild', () => {
    it('should return true if isActiveKeyValid', (done) => {
      authService.isActiveKeyValid.and.returnValue(of(true));
      spyOn(guard, 'canActivateChild').and.returnValue(authService.isActiveKeyValid());
      (guard.canActivateChild(null, null) as Observable<boolean>).subscribe((valid) => {
        expect(valid).toBeTrue();
        expect(authService.redirectToLogin).toHaveBeenCalledTimes(0);
        done();
      });
    });
    it('should navigate and return false if isActiveKeyValid', (done) => {
      authService.isActiveKeyValid.and.returnValue(of(false));
      spyOn(guard, 'canActivateChild')
        .and.returnValue(authService.isActiveKeyValid())
        .and.callThrough();
      (guard.canActivateChild(null, null) as Observable<boolean>).subscribe((valid) => {
        expect(valid).toBeFalse();
        expect(authService.redirectToLogin).toHaveBeenCalledTimes(1);
        done();
      });
    });
  });

  describe('canLoad', () => {
    it('should return true if isActiveKeyValid', (done) => {
      authService.isActiveKeyValid.and.returnValue(of(true));
      spyOn(guard, 'canLoad').and.returnValue(authService.isActiveKeyValid());
      (guard.canLoad(null, null) as Observable<boolean>).subscribe((valid) => {
        expect(valid).toBeTrue();
        expect(authService.redirectToLogin).toHaveBeenCalledTimes(0);
        done();
      });
    });
    it('should navigate and return false if isActiveKeyValid', (done) => {
      authService.isActiveKeyValid.and.returnValue(of(false));
      spyOn(guard, 'canLoad')
        .and.returnValue(authService.isActiveKeyValid())
        .and.callThrough();
      (guard.canLoad(null, null) as Observable<boolean>).subscribe((valid) => {
        expect(valid).toBeFalse();
        expect(authService.redirectToLogin).toHaveBeenCalledTimes(1);
        done();
      });
    });
  });
});
