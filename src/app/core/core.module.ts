import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';

import { AccountsService } from './services/accounts/accounts.service';
import { AuthService } from './services/auth/auth.service';
import { DbService } from './services/db/db.service';
import { ElectronService } from './services/electron/electron.service';
import { LoggerService } from './services/logger/logger.service';
import { ProfilesService } from './services/profiles/profiles.service';
import { ProxiesService } from './services/proxies/proxies.service';
import { SnackBarService } from './services/snack-bar/snack-bar.service';
import { GoogleSessionService } from './services/google-session/google-session.service';
import { RecaptchaHarvesterService } from './services/recaptcha-harvester/recaptcha-harvester.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    TranslateModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  exports: [
    CommonModule,
    TranslateModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    AccountsService,
    AuthService,
    DbService,
    ElectronService,
    LoggerService,
    ProfilesService,
    ProxiesService,
    SnackBarService,
    GoogleSessionService,
    RecaptchaHarvesterService,
  ],
})
export class CoreModule { }
