import { TestBed } from '@angular/core/testing';
import { Observable } from 'rxjs';
import { LoggerService } from '../logger/logger.service';

import { AccountsService } from './accounts.service';

describe('AccountsService', () => {
  let service: AccountsService;
  let loggerService: jasmine.SpyObj<LoggerService>;

  beforeEach(() => {
    const loggerSpy = jasmine.createSpyObj('LoggerService', [
      'child'
    ]);
    TestBed.configureTestingModule({
      providers: [
        {provide: LoggerService, useValue: loggerSpy}
      ],
    });
    service = TestBed.inject(AccountsService);
    loggerService = TestBed.inject(LoggerService) as jasmine.SpyObj<LoggerService>;
  });

  describe('constructor', () => {
    it('should be created', () => {
      expect(service).toBeTruthy();
    });

    it('should have created a logger child', () => {
      expect(loggerService.child).toHaveBeenCalledTimes(1);
    });
  });
  
  describe('getters', () => {
    describe('accountListMap$', () => {
      it('should return an observable', () => {
        expect(service.accountListMap$).toBeInstanceOf(Observable);
      });
    });

    describe('count', () => {
      it('should return the current amount of accounts', () => {
        expect(service.count).toBe(0);
      });
    });
  });

  describe('methods', () => {
    describe('setAccountList', () => {
      it('should add the account list', () => {
        service.setAccountList(['Supreme Instore', ['Test', 'Test']]);
        expect(service.count).toBe(2);
        service.setAccountList(['Not Supported', ['Test', 'Test']]);
        expect(service.count).toBe(2);
      });
      it('should push latest values to observable', (done) => {
        expect(service.count).toBe(0);
        service.setAccountList(['Supreme Instore', ['Test', 'Test']]);
        expect(service.count).toBe(2);
        service.accountListMap$.subscribe(accountLists => {
          expect(accountLists[0]).toEqual(['Supreme Instore', ['Test', 'Test']]);
          done();
        });
      });
    });

    describe('clearAccountList', () => {
      it('should empty the account list for the specified site', () => {
        service.setAccountList(['Supreme Instore', ['Test', 'Test']]);
        expect(service.count).toBe(2);
        service.clearAccountList('Supreme Instore');
        expect(service.count).toBe(0);
      });
    });
  });
});
