import { Injectable } from '@angular/core';

import { LoggerService } from '../logger/logger.service';

import { Logger } from 'app/lib/models/logger.model';
import { AccountList } from 'app/lib/models/account-list.model';
import { AccountListMap } from 'app/lib/account-list-map';
import { Observable } from 'rxjs';
import * as path from 'path';
import * as fs from 'fs-extra';
import {remote} from 'electron';

@Injectable({
  providedIn: 'root'
})
export class AccountsService {
  private logger: Logger;
  private accountListMap: AccountListMap;
  accountListFilePath: string;

  private path: typeof path;
  private fs: typeof fs;
  private remote: typeof remote;

  constructor(
    private loggerService: LoggerService,
  ) {
    this.logger = this.loggerService.child({process: 'accounts-service'});
    this.path = window.require('path');
    this.fs = window.require('fs-extra');
    this.remote = window.require('electron').remote;

    this.accountListFilePath = this.path.join(this.remote.app.getPath('userData'), 'accounts.json');
    this.accountListMap = new AccountListMap();
    this.initFilePath();
    this.importFromFile(this.accountListFilePath);
  };

  get accountListMap$(): Observable<AccountList[]> {
    return this.accountListMap.asObservable;
  };

  get count() {
    return this.accountListMap.count;
  };

  private initFilePath() {
    if (!this.fs.existsSync(this.accountListFilePath)) {
      this.fs.writeFileSync(this.accountListFilePath, JSON.stringify([], null, 2));
    };
  };

  importFromFile(filepath: string): Promise<boolean> {
    return new Promise((resolve) => {
      try {
        const fileContents = this.fs.readFileSync(filepath, 'utf8')
          .trim();
        const data: [string, string[]][] = JSON.parse(fileContents);
        this.accountListMap.import(data);
        resolve(true);
      } catch(err) {
        // bad import
        console.log(err);
        resolve(false);
      };
    });
  };

  exportToFile(filepath: string): Promise<boolean> {
    return new Promise((resolve) => {
      try {
        const data = this.accountListMap.export();
        this.fs.writeFileSync(
          filepath,
          JSON.stringify(data, null, 2)
        );
        resolve(true);
      } catch(err) {
        // bad export
        console.log(err);
        resolve(false);
      };
    });
  };

  setAccountList(accountList: AccountList) {
    this.accountListMap.setAccountListAndPush(accountList);
  };

  clearAccountList(accountListName: string) {
    this.accountListMap.clearAccountListAndPush(accountListName);
  };

  saveAccountList() {
    return this.exportToFile(this.accountListFilePath);
  };
}
