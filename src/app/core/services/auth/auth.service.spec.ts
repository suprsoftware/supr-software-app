import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AppConfig } from '../../../../environments/environment.dev';

import { AuthService } from './auth.service';
import { LoggerService } from '../logger/logger.service';
import { DbService } from '../db/db.service';

describe('AuthService', () => {
  let service: AuthService;
  let loggerService: jasmine.SpyObj<LoggerService>;
  let dbService: jasmine.SpyObj<DbService>;
  let router: jasmine.SpyObj<Router>;

  beforeEach(() => {
    const loggerSpy = jasmine.createSpyObj('LoggerService', [
      'child'
    ]);
    const dbSpy = jasmine.createSpyObj('DbService', [
      'getUserValueChanges',
      'getUserKeysCollectionValueChanges',
      'bindLicenseKeyToMachine',
      'getLicenseKeyValueChanges'
    ]);
    const routerSpy = jasmine.createSpyObj('Router', [
      'navigate'
    ]);

    TestBed.configureTestingModule({
      imports: [
        AngularFireModule.initializeApp(AppConfig.firebaseConfig),
        AngularFireAuthModule,
        RouterTestingModule,
      ],
      providers: [
        {provide: LoggerService, useValue: loggerSpy},
        {provide: DbService, useValue: dbSpy},
        {provide: Router, useValue: routerSpy},
      ]
    });
    service = TestBed.inject(AuthService);
    loggerService = TestBed.inject(LoggerService) as jasmine.SpyObj<LoggerService>;
    dbService = TestBed.inject(DbService) as jasmine.SpyObj<DbService>;
    router = TestBed.inject(Router) as jasmine.SpyObj<Router>;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
