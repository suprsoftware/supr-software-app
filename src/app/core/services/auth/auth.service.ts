import { Injectable, OnDestroy } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import firebase from 'firebase/app';
import 'firebase/auth';
import { Observable, of, BehaviorSubject, Subscription } from 'rxjs';
import { switchMap, map, take } from 'rxjs/operators';
import { machineIdSync } from 'node-machine-id';

import { DbService } from 'app/core/services/db/db.service';
import { LoggerService } from 'app/core/services/logger/logger.service';
import { User } from 'app/lib/models/user.model';
import { LicenseKey } from 'app/lib/models/license-key.model';
import { Logger } from 'app/lib/models/logger.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService implements OnDestroy {
  loading$: BehaviorSubject<boolean>;
  private user$: Observable<User>;
  private keys$: Observable<LicenseKey[]>;
  private activeKey$: BehaviorSubject<LicenseKey>;
  private licenseKeySubscription: Subscription;

  private logger: Logger;
  private machineIdSync: typeof machineIdSync;
  constructor(
    private auth: AngularFireAuth,
    private db: DbService,
    private loggerService: LoggerService,
    private router: Router,
  ) { 
    this.logger = this.loggerService.child({process: 'auth-service'});
    this.machineIdSync = window.require('node-machine-id').machineIdSync;
    this.loading$ = new BehaviorSubject(false);
    this.activeKey$ = new BehaviorSubject(null);

    this.user$ = this.auth.authState.pipe(
      switchMap((user: firebase.User) => {
        if(!!user) {
          return this.db.getUserValueChanges(user.uid);
        } else {
          return of(null);
        };
      }),
      switchMap((user: User) => {
        this.loading$.next(false);
        if (!!user) {
          return of(user);
        } else {
          this.setNullActiveKeyAndRedirect();
          return of(null);
        };
      })
    );

    this.keys$ = this.user$.pipe(
      switchMap((user: User) => {
        if (!!user) {
          return this.db.getUserKeysCollectionValueChanges(user.uid);
        } else {
          return of([]);
        };
      })
    );
  }

  ngOnDestroy(): void {
    this.licenseKeySubscription.unsubscribe();
  };
  
  get hardwareID(): string {
    return this.machineIdSync();
  };

  get activeKey(): LicenseKey {
    let key = null;
    this.activeKey$.pipe(
      take(1)
    ).subscribe(k => key = k);
    return key;
  };

  get activeKeyAsObservable(): Observable<LicenseKey> {
    return this.activeKey$.pipe();
  };

  get keysAsObservable(): Observable<LicenseKey[]> {
    return this.keys$.pipe();
  };

  get userAsObservable(): Observable<User> {
    return this.user$.pipe();
  };

  login(email: string, password: string): Promise<firebase.auth.UserCredential> {
    this.loading$.next(true);
    return this.auth.signInWithEmailAndPassword(email, password);
  };

  async logout() {
    await this.auth.signOut();
  };

  setAsActiveKey(key: LicenseKey): Promise<void> {
    return new Promise(async (resolve, reject) => {
      try {
        const {user_id, uuid} = key;
        if(!!this.licenseKeySubscription) this.licenseKeySubscription.unsubscribe();
        await this.db.bindLicenseKeyToMachine(key);
        this.licenseKeySubscription = this.db.getLicenseKeyValueChanges(user_id, uuid)
          .subscribe({
            next: (key) => {
              if (this.isKeyInvalid(key) || this.isKeyNotBoundToMachine(key)) {
                this.setNullActiveKeyAndRedirect();
                return resolve();
              };
              this.activeKey$.next(key);
              return resolve();
            },
            error: (err) => {
              this.setNullActiveKeyAndRedirect();
              return reject(err);
            }
          });
      } catch(err) {
        return reject(err);
      };
    });
  };

  isActiveKeyValid() {
    return this.activeKey$.pipe(
      map(key => {
        return this.isKeyValid(key);
      })
    );
  };
  
  async redirectToLogin() {
    await this.router.navigate(['/login']);
  };

  private isKeyNotBoundToMachine(key: LicenseKey) {
    return !this.isKeyBoundToMachine(key);
  };

  private isKeyBoundToMachine(key: LicenseKey) {
    return key.hardware_id === this.hardwareID;
  };

  private isKeyExpired(key: LicenseKey) {
    return !this.isKeyNotExpired(key);
  };

  private isKeyNotExpired(key: LicenseKey) {
    return key.expires_at.toDate() > (new Date());
  };

  private isKeyInvalid(key: LicenseKey) {
    return !this.isKeyValid(key);
  };

  private isKeyValid(key: LicenseKey) {
    if (!key) return false;
    if (this.isKeyExpired(key)) return false;
    if (!!key.hardware_id && this.isKeyNotBoundToMachine(key)) return false;
    return true;
  };

  private setNullActiveKeyAndRedirect() {
    this.activeKey$.next(null);
    return this.redirectToLogin();
  };
}
