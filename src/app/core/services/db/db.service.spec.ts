import { TestBed } from '@angular/core/testing';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AppConfig } from '../../../../environments/environment.dev';

import { DbService } from './db.service';
import { LoggerService } from '../logger/logger.service';
describe('DbService', () => {
  let service: DbService;
  let loggerService: jasmine.SpyObj<LoggerService>;

  beforeEach(() => {
    const loggerSpy = jasmine.createSpyObj('LoggerService', [
      'child'
    ]);

    TestBed.configureTestingModule({
      providers: [
        {provide: LoggerService, useValue: loggerSpy}
      ],
      imports: [
        AngularFireModule.initializeApp(AppConfig.firebaseConfig),
        AngularFirestoreModule
      ]
    });
    service = TestBed.inject(DbService);
    loggerService = TestBed.inject(LoggerService) as jasmine.SpyObj<LoggerService>;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
