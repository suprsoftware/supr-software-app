import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import firebase from 'firebase/app';
import 'firebase/firestore';

import { machineIdSync } from 'node-machine-id';
import * as os from 'os';

import { User } from 'app/lib/models/user.model';
import { LicenseKey } from 'app/lib/models/license-key.model';

import { LoggerService } from 'app/core/services/logger/logger.service';
import { Logger } from 'app/lib/models/logger.model';

@Injectable({
  providedIn: 'root'
})
export class DbService {
  private logger: Logger;
  private machineIdSync: typeof machineIdSync;
  private os: typeof os;
  constructor(
    private firestore: AngularFirestore,
    private loggerService: LoggerService,
  ) { 
    this.logger = this.loggerService.child({process: 'db-service'});
    this.machineIdSync = window.require('node-machine-id').machineIdSync;
    this.os = window.require('os');
  }

  get hardwareID(): string {
    return this.machineIdSync();
  };

  getUserValueChanges(uid: string): Observable<User> {
    return this.getUserDocument(uid).valueChanges();
  };

  getUserDocument(uid: string): AngularFirestoreDocument<User> {
    return this.firestore.doc<User>(`users/${uid}`);
  };

  getUserKeysCollectionValueChanges(user_id: string) {
    return this.getUserAvailableKeysCollection(user_id).valueChanges();
  };

  getUserAvailableKeysCollection(user_id: string): AngularFirestoreCollection<LicenseKey> {
    return this.firestore.collection<LicenseKey>(`users/${user_id}/keys`, ref => {
      return ref
        .where('hardware_id', 'in', ['', this.hardwareID])
        .where('expires_at', '>', new Date())
    });
  };

  getLicenseKeyValueChanges(user_id: string, uuid: string) {
    return this.getLicenseKeyDocument(user_id, uuid).valueChanges();
  };

  getLicenseKeySnapshotChanges(user_id: string, uuid: string) {
    return this.getLicenseKeyDocument(user_id, uuid).snapshotChanges();
  };

  getLicenseKeyDocument(user_id: string, uuid: string): AngularFirestoreDocument<LicenseKey> {
    return this.firestore.doc<LicenseKey>(`users/${user_id}/keys/${uuid}`);
  };

  bindLicenseKeyToMachine(key: LicenseKey) {
    return this.updateLicenseKeyFields(key, {
      hardware_id: this.hardwareID,
      lastlogin_at: firebase.firestore.Timestamp.now(),
      hostname: this.os.hostname(),
    });
  };

  unbindLicenseKeyFromMachine(key: LicenseKey) {
    return this.updateLicenseKeyFields(key, {
      hardware_id: '',
      hostname: '',
    });
  };

  updateLicenseKeyFields({user_id, uuid}: LicenseKey, data: Partial<LicenseKey>) {
    return this.getLicenseKeyDocument(user_id, uuid).update({
      ...data
    });
  };
}
