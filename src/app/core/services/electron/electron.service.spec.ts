import { TestBed } from '@angular/core/testing';
import { LoggerService } from '../logger/logger.service';

import { ElectronService } from './electron.service';

describe('ElectronService', () => {
  let service: ElectronService;
  let loggerService: jasmine.SpyObj<LoggerService>;

  beforeEach(() => {
    const loggerSpy = jasmine.createSpyObj('LoggerService', [
      'child'
    ]);
    TestBed.configureTestingModule({
      providers: [
        {provide: LoggerService, useValue: loggerSpy}
      ]
    });
    service = TestBed.inject(ElectronService);
    loggerService = TestBed.inject(LoggerService) as jasmine.SpyObj<LoggerService>;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
