import { Injectable } from '@angular/core';

// If you import a module but never use any of the imported values other than as TypeScript types,
// the resulting javascript file will look as if you never imported the module at all.
import { ipcRenderer, webFrame, remote } from 'electron';
import * as childProcess from 'child_process';
import * as fs from 'fs';

import { LoggerService } from 'app/core/services/logger/logger.service';
import { Logger } from 'app/lib/models/logger.model';

@Injectable({
  providedIn: 'root'
})
export class ElectronService {
  private logger: Logger;

  private ipcRenderer: typeof ipcRenderer;
  private webFrame: typeof webFrame;
  private remote: typeof remote;
  private childProcess: typeof childProcess;
  private fs: typeof fs;

  constructor(
    private loggerService: LoggerService,
  ) {
    this.logger = this.loggerService.child({process: 'electron-service'});
      this.ipcRenderer = window.require('electron').ipcRenderer;
      this.webFrame = window.require('electron').webFrame;

      // If you wan to use remote object, pleanse set enableRemoteModule to true in main.ts
      this.remote = window.require('electron').remote;

      this.childProcess = window.require('child_process');
      this.fs = window.require('fs');

      // ipcRenderer
      this.ipcRenderer.on('pong', () => {
        console.log('pong!');
      });
  }

  get appVersion(): string {
    const { version } = this.remote.require('./package.json');
      return version;
  };

  closeWindow(): void {
    this.ipcRenderer.send('mainWindow::close');
  };

  minWindow(): void {
    this.ipcRenderer.send('mainWindow::minimize');
  };
  
  maxWindow(): void {
    this.ipcRenderer.send('mainWindow::maximize');
  };

  getCookies(opts: Electron.CookiesGetFilter) {
    return this.remote.session.defaultSession.cookies.get(opts);
  };

  clearCookies() {
    return this.remote.session.defaultSession.clearStorageData({
      storages: ['cookies'],
      quotas: ['persistent']
    });
  };

  openDesktopFolder(): Promise<Electron.OpenDialogReturnValue> {
    return this.remote.dialog.showOpenDialog({
      defaultPath: this.remote.app.getPath('desktop'),
      filters: [
        { name: 'JSON', extensions: ['json'] }
      ],
      properties: [
        'openFile'
      ]
    });
  };

  saveDesktopFolder(): Promise<Electron.SaveDialogReturnValue> {
    return this.remote.dialog.showSaveDialog({
      defaultPath: this.remote.app.getPath('desktop'),
      filters: [
        { name: 'JSON', extensions: ['json'] }
      ],
      properties: [
        'createDirectory',
        'showOverwriteConfirmation',
        'dontAddToRecent'
      ]
    });
  };

}
