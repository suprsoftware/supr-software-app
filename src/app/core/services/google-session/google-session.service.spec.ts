import { TestBed } from '@angular/core/testing';

import { GoogleSessionService } from './google-session.service';
import { LoggerService } from '../logger/logger.service';
import { ProxiesService } from '../proxies/proxies.service';

describe('GoogleSessionService', () => {
  let service: GoogleSessionService;
  let loggerService: jasmine.SpyObj<LoggerService>;
  let proxiesService: jasmine.SpyObj<ProxiesService>;

  beforeEach(() => {
    const loggerSpy = jasmine.createSpyObj('LoggerService', [
      'child'
    ]);
    const proxiesSpy = jasmine.createSpyObj('ProxiesService', [
      'getProxyList'
    ]);

    TestBed.configureTestingModule({
      providers: [
        {provide: LoggerService, useValue: loggerSpy},
        {provide: ProxiesService, useValue: proxiesSpy}
      ]
    });
    service = TestBed.inject(GoogleSessionService);
    loggerService = TestBed.inject(LoggerService) as jasmine.SpyObj<LoggerService>;
    proxiesService = TestBed.inject(ProxiesService) as jasmine.SpyObj<ProxiesService>;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
