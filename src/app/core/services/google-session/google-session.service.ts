import { Injectable } from '@angular/core';
import * as path from 'path';
import * as fs from 'fs-extra';
import { remote } from 'electron';
import { LoggerService } from 'app/core/services/logger/logger.service';
import { Logger } from 'app/lib/models/logger.model';
import { GoogleSessionMap } from 'app/lib/google-session-map';
import { Observable } from 'rxjs';
import { GoogleSession, GoogleSessionInterface } from 'app/lib/models/google-session';
import { ProxiesService } from 'app/core/services/proxies/proxies.service';
import { generateId } from 'app/lib/uuid';

@Injectable({
  providedIn: 'root'
})
export class GoogleSessionService {
  private logger: Logger;
  gSessionFilePath: string;
  gSessionMap: GoogleSessionMap;
  gSessions$: Observable<GoogleSession[]>;

  private path: typeof path;
  private fs: typeof fs;
  private remote: typeof remote;
  
  constructor(
    private loggerService: LoggerService,
    private proxiesService: ProxiesService,
  ) { 
    this.logger = this.loggerService.child({process: 'gsession-service'});
    this.path = window.require('path');
    this.fs = window.require('fs-extra');
    this.remote = window.require('electron').remote;

    this.gSessionFilePath = this.path.join(this.remote.app.getPath('userData'), 'gsession.json');
    this.gSessionMap = new GoogleSessionMap();
    this.gSessions$ = this.gSessionMap.sessionMap$.pipe();
    this.initDefaultSession();
    this.initFilePath();
    this.importFromFile(this.gSessionFilePath);
  }

  initDefaultSession() {
    const defaultSession = new GoogleSession(
      this.proxiesService,
      {
        id: 'default',
        proxyListName: ''
      }
    );
    this.setGoogleSession(defaultSession);
  };

  setGoogleSession(session: GoogleSession) {
    this.gSessionMap.setGoogleSession(session);
  };

  initFilePath() {
    if (!this.fs.existsSync(this.gSessionFilePath)) {
      this.fs.writeFileSync(this.gSessionFilePath, JSON.stringify([], null, 2)); 
    };
  };

  importFromFile(filepath: string): Promise<boolean> {
    return new Promise(resolve => {
      try {
        const fileContents = this.fs.readFileSync(filepath, 'utf8')
          .trim();
        const data: GoogleSessionInterface[] = JSON.parse(fileContents);
        data.forEach(config => {
          if (config.id.toLowerCase() == 'default') return;
          const session = new GoogleSession(this.proxiesService, config);
          this.setGoogleSession(session);
        });
        this.pushLatestValues();
        resolve(true);
      } catch(err) {
        console.log(err);
        resolve(false);
      };
    });
  };

  setAndPushGoogleSession(session: GoogleSession) {
    this.gSessionMap.setGoogleSession(session);
    this.pushLatestValues();
  };

  pushLatestValues() {
    this.gSessionMap.pushLatestValues();
  };

  save() {
    return this.exportToFile(this.gSessionFilePath);
  };

  exportToFile(filepath: string): Promise<boolean> {
    return new Promise(resolve => {
      try {
        const data = this.gSessionMap.export();
        this.fs.writeFileSync(
          filepath,
          JSON.stringify(data, null, 2)
        );
        resolve(true);
      } catch(err) {
        console.log(err);
        resolve(false);
      };
    });
  };

  createNewGoogleSession() {
    const session = new GoogleSession(this.proxiesService, {
      id: `persist:${generateId()}`,
      proxyListName: ''
    });
    this.setAndPushGoogleSession(session);
  };

  removeAndPushGoogleSession(id: string) {
    this.removeGoogleSession(id);
    this.pushLatestValues();
  };

  removeGoogleSession(id: string) {
    this.gSessionMap.remove(id);
  };

  async loginGoogleSession(gSession: GoogleSession) {
    try {
      const activeProxy = await this.setActiveProxyForSession(gSession);
      let browser = new this.remote.BrowserWindow({
        width: 400,
        height: 600,
        minWidth: 400,
        minHeight: 600,
        autoHideMenuBar: true,
        parent: this.remote.getCurrentWindow(),
        webPreferences: {
          session: gSession.electronSession,
          nodeIntegration: false,
          enableRemoteModule: false,
        }
      });
      browser.on('close', () => {
        browser = null;
      });
      browser.webContents.on('login', (event, {url}, {isProxy, scheme, host, port, realm}, cb) => {
        const proxy = activeProxy.proxyURL;
        if (!isProxy) return cb();
        if (!proxy) return cb();
        return cb(proxy.username, proxy.password);
      });
      browser.removeMenu();
      await browser.loadURL('https://accounts.google.com/signin/v2/identifier?service=youtube&uilel=3&passive=true&continue=https%3A%2F%2Fwww.youtube.com%2Fsignin%3Faction_handle_signin%3Dtrue%26app%3Ddesktop%26hl%3Den%26next%3D%252F&hl=en&ec=65620&flowName=GlifWebSignIn&flowEntry=ServiceLogin', {
        userAgent: 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/70.0',
      });
    } catch(err) {
      console.log(err);
    };
  };

  async setActiveProxyForSession({proxyList, electronSession}: GoogleSession) {
    try {
      const proxyPool = proxyList[1];
      if (proxyPool.length == 0) return;
      const randomProxy = proxyPool[Math.floor(Math.random() * proxyPool.length)];
      if (!randomProxy.proxyURL) return;
      await electronSession.setProxy({
        proxyRules: randomProxy.proxyURL.host,
      });
      return randomProxy;
    } catch(err) {
      console.log(err);
    };
  };
  
}
