import { TestBed } from '@angular/core/testing';

import { LoggerService } from './logger.service';

describe('LoggerService', () => {
  let service: LoggerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LoggerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('methods', () => {

    describe('initLogger', () => {});

    describe('debug', () => {});

    describe('error', () => {});

    describe('info', () => {});

    describe('child', () => {});
  });
});
