import { Injectable } from '@angular/core';

import { remote } from 'electron';

import { Logger, ChildOptions } from 'app/lib/models/logger.model';

@Injectable({
  providedIn: 'root'
})
export class LoggerService {
  private remote: typeof remote;
  private logger: Logger;
  constructor() {
    this.remote = window.require('electron').remote;
    this.initLogger();
  };
  
  initLogger() {
    const mainLogger = (this.remote.getGlobal('logger') as Logger);
    if (!mainLogger) return;
    this.logger = mainLogger.child({process: 'logger-service'});
    this.logger.debug('logger initialized');
  };

  debug(message: string) {
    this.logger.debug(message);
  };

  error(message: string) {
    this.logger.error(message);
  };

  info(message: string) {
    this.logger.info(message);
  };

  child(options: ChildOptions) {
    const logger = this.logger.child(options);
    logger.debug('logger initialized');
    return logger;
  };
}
