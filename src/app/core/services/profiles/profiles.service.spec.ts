import { TestBed } from '@angular/core/testing';

import { ProfilesService } from './profiles.service';
import { LoggerService } from '../logger/logger.service';

describe('ProfilesService', () => {
  let service: ProfilesService;
  let loggerService: jasmine.SpyObj<LoggerService>;

  beforeEach(() => {
    const loggerSpy = jasmine.createSpyObj('LoggerService', {
      'child': {
        debug: function() {}
      }
    });
    TestBed.configureTestingModule({
      providers: [
        {provide: LoggerService, useValue: loggerSpy}
      ]
    });
    service = TestBed.inject(ProfilesService);
    loggerService = TestBed.inject(LoggerService) as jasmine.SpyObj<LoggerService>;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('methods', () => {
    describe('saveProfiles', () => {
      it('should call exportToFile with path to profiles.json', () => {
        const exportToFileSpy = spyOn(service, 'exportToFile');
        service.saveProfiles();
        const [filepath] = exportToFileSpy.calls.argsFor(0);
        expect(exportToFileSpy).toHaveBeenCalledTimes(1);
        expect(filepath.endsWith('profiles.json')).toBeTrue();
      });
    });
  });
});
