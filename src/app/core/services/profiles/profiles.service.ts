import { Injectable } from '@angular/core';
import * as path from 'path';
import * as fs from 'fs-extra';
import { remote } from 'electron';
import { Observable } from 'rxjs';

import { LoggerService } from 'app/core/services/logger/logger.service';
import { Logger } from 'app/lib/models/logger.model';
import { ProfileMap } from 'app/lib/profile-map';
import { Profile, ProfileInterface } from 'app/lib/models/profile';

@Injectable({
  providedIn: 'root'
})
export class ProfilesService {
  private logger: Logger;
  profilesFilePath: string;
  profileMap: ProfileMap;
  profiles$: Observable<Profile[]>;

  private path: typeof path;
  private fs: typeof fs;
  private remote: typeof remote;

  constructor(
    private loggerService: LoggerService,
  ) { 
    this.logger = this.loggerService.child({process: 'profiles-service'});
    this.path = window.require('path');
    this.fs = window.require('fs-extra');
    this.remote = window.require('electron').remote;

    this.profilesFilePath = this.path.join(this.remote.app.getPath('userData'), 'profiles.json');
    this.profileMap = new ProfileMap();
    this.profiles$ = this.profileMap.profiles$.pipe();
    this.initFilePath();
    this.importFromFile(this.profilesFilePath);
  }

  get profilesCount() {
    return this.profileMap.size;
  };

  initFilePath() {
    if (!this.fs.existsSync(this.profilesFilePath)) {
      this.fs.writeFileSync(this.profilesFilePath, JSON.stringify([], null, 2));
    };
  };

  importFromFile(filepath: string) {
    return new Promise((resolve) => {
      try {
        const fileContents = this.fs.readFileSync(filepath, 'utf8')
          .trim();
        const data: ProfileInterface[] = JSON.parse(fileContents);
        this.profileMap.importFromInterfacesAndPush(data);
        resolve(true);
      } catch(err) {
        console.log(err);
        resolve(false);
      };
    });
  };

  exportToFile(filepath: string) {
    return new Promise((resolve) => {
      try {
        const exportData = this.profileMap.export();
        this.fs.writeFileSync(
          filepath,
          JSON.stringify(exportData, null, 2)
        );
        resolve(true);
      } catch(err) {
        console.log(err);
        resolve(false);
      };
    });
  };

  hasProfile(profileName: string): boolean {
    return this.profileMap.hasProfile(profileName);
  };

  saveProfileAsInterfaceAndPush(profile: ProfileInterface) {
    const p = new Profile(profile);
    this.profileMap.setProfileAndPush(p);
  };

  deleteProfile(profile: Profile) {
    return this.profileMap.deleteProfileAndPush(profile.name);
  };

  copyProfile(profile: Profile) {
    const profileData = profile.toJSON();
    const copy = new Profile(profileData);
    copy.name = this.getNewProfileName();
    return this.profileMap.setProfileAndPush(copy);
  };

  getNewProfileName() {
    let name = 'New Profile';
    if (this.hasProfile(name)) {
      name += ` ${this.profilesCount}`;
    };
    return name;
  };

  async saveProfiles() {
    await this.exportToFile(this.profilesFilePath);
  };
}
