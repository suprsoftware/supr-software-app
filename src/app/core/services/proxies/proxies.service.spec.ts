import { TestBed } from '@angular/core/testing';

import { ProxiesService } from './proxies.service';
import { LoggerService } from '../logger/logger.service';

describe('ProxiesService', () => {
  let service: ProxiesService;
  let loggerService: jasmine.SpyObj<LoggerService>;

  beforeEach(() => {
    const loggerSpy = jasmine.createSpyObj('LoggerService', [
      'child'
    ]);
    TestBed.configureTestingModule({
      providers: [
        {provide: LoggerService, useValue: loggerSpy}
      ]
    });
    service = TestBed.inject(ProxiesService);
    loggerService = TestBed.inject(LoggerService) as jasmine.SpyObj<LoggerService>;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
