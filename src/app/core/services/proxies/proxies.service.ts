import { Injectable } from '@angular/core';
import * as path from 'path';
import * as fs from 'fs-extra';
import { remote } from 'electron';
import { Observable } from 'rxjs';

import { Proxy } from 'app/lib/models/proxy';
import { ProxyList } from 'app/lib/models/proxy-list.model';
import { ProxyListMap } from 'app/lib/proxy-list-map';

import { LoggerService } from 'app/core/services/logger/logger.service';
import { Logger } from 'app/lib/models/logger.model';

@Injectable({
  providedIn: 'root'
})
export class ProxiesService {
  private logger: Logger;
  proxyListFilePath: string;
  proxyListMap: ProxyListMap;

  private path: typeof path;
  private fs: typeof fs;
  private remote: typeof remote;

  constructor(
    private loggerService: LoggerService,
  ) {
    this.logger = this.loggerService.child({process: 'proxies-service'});
    this.path = window.require('path');
    this.fs = window.require('fs-extra');
    this.remote = window.require('electron').remote;
    
    this.proxyListFilePath = this.path.join(this.remote.app.getPath('userData'), 'proxies.json');
    this.proxyListMap = new ProxyListMap();
    this.initFilePath();
    this.importFromFile(this.proxyListFilePath);
  }

  get proxyCount(): number {
    return this.proxyListMap.count;
  };

  get listSize(): number {
    return this.proxyListMap.size;
  };

  get proxyListMap$(): Observable<ProxyList[]> {
    return this.proxyListMap.asObservable;
  };

  get allProxies(): Proxy[] {
    return Array.from(this.proxyListMap.values).reduce((acc, curr) => {
      curr.forEach(p => acc.push(p));
      return acc;
    }, []);
  };

  get allProxyListNames(): string[] {
    return Array.from(this.proxyListMap.keys);
  };

  get allProxiesAndProxyListNames(): string[] {
    const allProxyStrings = this.allProxies
      .filter(p => !!p.proxyString)
      .map(p => p.proxyString);
    return [...this.allProxyListNames, ...allProxyStrings];
  };

  private initFilePath() {
    if (!this.fs.existsSync(this.proxyListFilePath)) {
      this.fs.writeFileSync(this.proxyListFilePath, JSON.stringify([], null, 2));
    };
  };

  importFromFile(filepath: string): Promise<boolean> {
    return new Promise((resolve) => {
      try {
        const fileContents = this.fs.readFileSync(filepath, 'utf8')
          .trim();
        const data: [string, string[]][] = JSON.parse(fileContents);
        this.proxyListMap.import(data);
        resolve(true);
      } catch(err) {
        // bad import
        console.log(err);
        resolve(false);
      };
    });
  };

  exportToFile(filepath: string): Promise<boolean> {
    return new Promise((resolve) => {
      try {
        const exportData = this.proxyListMap.export();
        this.fs.writeFileSync(
          filepath,
          JSON.stringify(exportData, null, 2)
        );
        resolve(true);
      } catch(err) {
        console.log(err);
        resolve(false);
      };
    });
  };

  async saveProxyList() {
    await this.exportToFile(this.proxyListFilePath);
  };

  getProxyList(proxyListName: string) {
    return this.proxyListMap.getProxyList(proxyListName);
  };

  getProxyPool(proxyListName: string) {
    return this.proxyListMap.getProxyPool(proxyListName);
  };

  deleteOrClearProxyList(proxyListName: string) {
    if (this.isProxyListRemovable(proxyListName)) {
      this.proxyListMap.deleteProxyListAndPush(proxyListName);
    } else {
      this.proxyListMap.clearProxyListAndPush(proxyListName);
    };
  };

  setProxyList(proxyList: ProxyList) {
    this.proxyListMap.setProxyListAndPush(proxyList);
  };

  isProxyListMutable(proxyListName: string) {
    return this.proxyListMap.isMutable(proxyListName);
  };

  isProxyListRemovable(proxyListName: string) {
    return this.proxyListMap.isRemovable(proxyListName);
  };

  getNewProxyListName() {
    return this.proxyListMap.getNewProxyListName();
  };

  hasProxyList(proxyListName: string) {
    return this.proxyListMap.has(proxyListName);
  };

  getProxyByProxyString(proxyString: string) {
    let proxy: Proxy;
    for (let proxyList of Array.from(this.proxyListMap.values)) {
      if (!!proxy) continue;
      proxy = proxyList.find(p => {
        return p.proxyString == proxyString;
      });
    };
    return proxy;
  };
}
