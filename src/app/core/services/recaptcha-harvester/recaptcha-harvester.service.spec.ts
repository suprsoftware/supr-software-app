import { TestBed } from '@angular/core/testing';
import { LoggerService } from '../logger/logger.service';

import { RecaptchaHarvesterService } from './recaptcha-harvester.service';

describe('RecaptchaHarvesterService', () => {
  let service: RecaptchaHarvesterService;
  let loggerService: jasmine.SpyObj<LoggerService>;

  beforeEach(() => {
    const loggerSpy = jasmine.createSpyObj('LoggerService', [
      'child'
    ]);
    TestBed.configureTestingModule({
      providers: [
        {provide: LoggerService, useValue: loggerSpy}
      ]
    });
    service = TestBed.inject(RecaptchaHarvesterService);
    loggerService = TestBed.inject(LoggerService) as jasmine.SpyObj<LoggerService>;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
