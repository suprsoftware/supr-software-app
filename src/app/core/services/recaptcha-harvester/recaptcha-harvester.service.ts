import { Injectable } from '@angular/core';
import { remote } from 'electron';
import { Logger } from 'app/lib/models/logger.model';
import { LoggerService } from '../logger/logger.service';
import { RecaptchaHarvesterMap } from 'app/lib/models/recaptcha-harvester-map';
import { RecaptchaHarvesterQueueMap } from 'app/lib/models/recaptcha-harvester-queue-map';
import { RecaptchaHarvester, RecaptchaHarvesterConstructorOptions } from 'app/lib/models/recaptcha-harvester';
import { HttpClient } from '@angular/common/http';
import { RECAPTCHA_SITEKEYS, SUPPORTED_SITES } from 'app/lib/models/supported-sites';

@Injectable({
  providedIn: 'root'
})
export class RecaptchaHarvesterService {
  private logger: Logger;

  private remote: typeof remote;
  harvesterMap: RecaptchaHarvesterMap;
  queueMap: RecaptchaHarvesterQueueMap;

  constructor(
    private loggerService: LoggerService,
    private http: HttpClient,
  ) { 
    this.logger = this.loggerService.child({process: 'recaptcha-harvester-service'});
    this.remote = window.require('electron').remote;
    this.harvesterMap = new RecaptchaHarvesterMap();
    this.queueMap = new RecaptchaHarvesterQueueMap();
  }

  getRecaptchaHTML(mode: SUPPORTED_SITES) {
    return this.http.get(`https://recaptcha.suprsoftware.io/?sitekey=${RECAPTCHA_SITEKEYS[mode]}&title=${encodeURIComponent(mode)}`, {
      responseType: 'text',
    });
  };
  
  createNewRecaptchaHarvester(options: RecaptchaHarvesterConstructorOptions) {
    const harvester = new RecaptchaHarvester(options);
    harvester.once('ready', () => {
      this.logger.debug(`harvester-${harvester.id} ready`);
      harvester.show();
      this.harvesterMap.set(harvester);
      const queue = this.queueMap.get(options.mode);
      queue.enqueue(harvester);
    });
    harvester.once('closed', () => {
      console.log(`harvester ${harvester.id} closed`);
      const queue = this.queueMap.get(options.mode);
      queue.removeById(harvester.id);
      this.harvesterMap.delete(harvester.id);
    });
    return harvester;
  };

}
