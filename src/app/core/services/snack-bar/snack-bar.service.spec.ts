import { TestBed } from '@angular/core/testing';
import { MatSnackBar, MatSnackBarConfig, MatSnackBarModule } from '@angular/material/snack-bar';
import { SnackBarService } from './snack-bar.service';
import { LoggerService } from '../logger/logger.service';
import { SimpleSnackBarComponent } from 'app/shared/components';

describe('SnackBarService', () => {
  let service: SnackBarService;
  let loggerService: jasmine.SpyObj<LoggerService>;
  let snackbar: jasmine.SpyObj<MatSnackBar>;

  beforeEach(() => {
    const loggerSpy = jasmine.createSpyObj('LoggerService', [
      'child'
    ]);
    const snackbarSpy = jasmine.createSpyObj('MatSnackBar', [
      'openFromComponent'
    ]);

    TestBed.configureTestingModule({
      imports: [
        MatSnackBarModule,
      ],
      providers: [
        {provide: LoggerService, useValue: loggerSpy},
        {provide: MatSnackBar, useValue: snackbarSpy}
      ]
    });
    service = TestBed.inject(SnackBarService);
    loggerService = TestBed.inject(LoggerService) as jasmine.SpyObj<LoggerService>;
    snackbar = TestBed.inject(MatSnackBar) as jasmine.SpyObj<MatSnackBar>;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should spawn a child logger', () => {
    expect(loggerService.child).toHaveBeenCalledOnceWith({process: 'snackbar-service'});
  });

  describe('methods', () => {

    describe('openGood', () => {
      let componentArg: unknown;
      let options: MatSnackBarConfig;
      beforeEach(() => {
        service.openGood('Test');
        [componentArg, options] = snackbar.openFromComponent.calls.argsFor(0);
      });
      it('should use SimpleSnackBarComponent', () => {
        expect(componentArg).toEqual(SimpleSnackBarComponent);
      });
      it('should have a duration of 3000', () => {
        expect(options.duration).toBe(3000);
      });
      it('should display at the top', () => {
        expect(options.verticalPosition).toBe('top');
      });
      it('should prefix with the message passed in', () => {
        expect(options.data.indexOf('Test')).toBe(0);
      });
      it('should have a panelClass of snackbar', () => {
        expect(options.panelClass).toBe('snackbar');
      });
    });
    
    describe('openBad', () => {
      let componentArg: unknown;
      let options: MatSnackBarConfig;
      beforeEach(() => {
        service.openBad('Test');
        [componentArg, options] = snackbar.openFromComponent.calls.argsFor(0);
      });
      it('should use SimpleSnackBarComponent', () => {
        expect(componentArg).toEqual(SimpleSnackBarComponent);
      });
      it('should have a duration of 3000', () => {
        expect(options.duration).toBe(3000);
      });
      it('should display at the top', () => {
        expect(options.verticalPosition).toBe('top');
      });
      it('should prefix with the message passed in', () => {
        expect(options.data.indexOf('Test')).toBe(0);
      });
      it('should have a panelClass of snackbar', () => {
        expect(options.panelClass).toBe('snackbar');
      });
    });
  });
});
