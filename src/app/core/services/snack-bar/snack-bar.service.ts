import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

import { LoggerService } from 'app/core/services/logger/logger.service';
import { Logger } from 'app/lib/models/logger.model';

import { SimpleSnackBarComponent } from 'app/shared/components';

@Injectable({
  providedIn: 'root'
})
export class SnackBarService {
  private logger: Logger;

  constructor(
    private snackbar: MatSnackBar,
    private loggerService: LoggerService,
  ) { 
    this.logger = this.loggerService.child({process: 'snackbar-service'});
  }

  openGood(message: string) {
    this.snackbar.openFromComponent(SimpleSnackBarComponent, {
      duration: 3000,
      verticalPosition: 'top',
      data: message + ' ✅',
      panelClass: 'snackbar',
    });
  };

  openBad(message: string) {
    this.snackbar.openFromComponent(SimpleSnackBarComponent, {
      duration: 3000,
      verticalPosition: 'top',
      data: message + ' ❌',
      panelClass: 'snackbar'
    });
  };
}
