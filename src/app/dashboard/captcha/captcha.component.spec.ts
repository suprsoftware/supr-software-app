import { Component } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { GoogleSessionService } from 'app/core/services/google-session/google-session.service';

import { CaptchaComponent } from './captcha.component';

@Component({selector: 'app-header', template: ''})
class HeaderComponent {}

describe('CaptchaComponent', () => {
  let component: CaptchaComponent;
  let fixture: ComponentFixture<CaptchaComponent>;
  let gSessionService: jasmine.SpyObj<GoogleSessionService>;

  beforeEach(async () => {
    const gSessionSpy = jasmine.createSpyObj('GoogleSessionService', [
      'initDefaultSession',
      'initFilePath',
      'importFromFile'
    ], {
      gSessions$: {
        pipe: function() {}
      }
    });

    await TestBed.configureTestingModule({
      declarations: [ CaptchaComponent, HeaderComponent ],
      providers: [
        {provide: GoogleSessionService, useValue: gSessionSpy}
      ],
      imports: [
        MatProgressSpinnerModule,
      ]
    })
    .compileComponents();

    gSessionService = TestBed.inject(GoogleSessionService) as jasmine.SpyObj<GoogleSessionService>;
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CaptchaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
