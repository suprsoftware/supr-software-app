import { Component, OnInit } from '@angular/core';
import { GoogleSessionService } from 'app/core/services/google-session/google-session.service';
import { RecaptchaHarvesterService } from 'app/core/services/recaptcha-harvester/recaptcha-harvester.service';
import { CreateRecaptchaHarvesterEvent } from 'app/lib/models/events';
import { GoogleSession } from 'app/lib/models/google-session';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-recaptcha',
  templateUrl: './captcha.component.html',
  styleUrls: ['./captcha.component.scss']
})
export class CaptchaComponent implements OnInit {
  gSessions$: Observable<GoogleSession[]>;

  constructor(
    private gSessionService: GoogleSessionService,
    private recaptchaHarvesterService: RecaptchaHarvesterService,
  ) { 
    this.gSessions$ = this.gSessionService.gSessions$.pipe();
  }

  ngOnInit(): void {
    window.sessionStorage.setItem('lastVisitedPage', window.location.hash);
  }

  createNewSessionHandler() {
    this.gSessionService.createNewGoogleSession();
    this.gSessionService.save();
  };

  deleteSessionHandler($event: string) {
    this.gSessionService.removeAndPushGoogleSession($event);
    this.gSessionService.save();
  };

  proxySelectedHandler() {
    this.gSessionService.save();
  };

  loginSessionHandler($event: GoogleSession) {
    this.gSessionService.save();
    this.gSessionService.loginGoogleSession($event);
  };

  async createNewHarvesterHandler({mode, gSession}: CreateRecaptchaHarvesterEvent) {
    try {
      const activeProxy = await this.gSessionService.setActiveProxyForSession(gSession);
      this.recaptchaHarvesterService.getRecaptchaHTML(mode)
        .subscribe(res => {
          this.recaptchaHarvesterService.createNewRecaptchaHarvester({
            mode,
            proxy: activeProxy,
            session: gSession.electronSession,
            data: Buffer.from(res, 'utf-8'),
          });
        });
    } catch(err) {
      console.log(err);
    };
  };
}
