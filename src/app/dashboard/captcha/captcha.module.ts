import { NgModule } from '@angular/core';

import { CoreModule } from 'app/core/core.module';
import { HeaderModule } from '../header/header.module';

import { CaptchaComponent } from './captcha.component';
import { HarvesterCardComponent } from './components';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { SharedModule } from 'app/shared/shared.module';

@NgModule({
  declarations: [
    CaptchaComponent,
    HarvesterCardComponent,
  ],
  imports: [
    CoreModule,
    SharedModule,
    HeaderModule,
    MatProgressSpinnerModule,
    MatIconModule,
    MatButtonModule,
    MatSelectModule,
    MatAutocompleteModule,
  ],
  exports: [

  ]
})
export class CaptchaModule {};