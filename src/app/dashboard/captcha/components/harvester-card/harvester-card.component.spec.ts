import { Component, Input } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatCardModule } from '@angular/material/card';
import { HarvesterCardComponent } from './harvester-card.component';
import { GoogleSession } from 'app/lib/models/google-session';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';

@Component({selector: 'app-autocomplete-proxies', template: ''})
class AutocompleteProxiesComponent {
  @Input() control: FormControl;
  @Input() optionSelectedHandler: ($event: MatAutocompleteSelectedEvent) => void;
};

describe('HarvesterCardComponent', () => {
  let component: HarvesterCardComponent;
  let fixture: ComponentFixture<HarvesterCardComponent>;
  
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HarvesterCardComponent, AutocompleteProxiesComponent ],
      imports: [
        MatCardModule,
        MatIconModule,
        MatButtonModule,
        MatSelectModule,
        FormsModule,
        ReactiveFormsModule,
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HarvesterCardComponent);
    component = fixture.componentInstance;
    component.gSession = {
      id: 'default'
    } as GoogleSession;
    component.index = 0;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
