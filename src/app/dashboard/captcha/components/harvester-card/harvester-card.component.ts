import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { GoogleSession } from 'app/lib/models/google-session';
import { SUPPORTED_SITES } from 'app/lib/models/supported-sites';
import { RecaptchaHarvester } from 'app/lib/models/recaptcha-harvester';
import { Observable } from 'rxjs';
import { FormControl, FormGroup } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { CreateRecaptchaHarvesterEvent } from 'app/lib/models/events';

@Component({
  selector: 'app-harvester-card',
  templateUrl: './harvester-card.component.html',
  styleUrls: ['./harvester-card.component.scss']
})
export class HarvesterCardComponent implements OnInit {
  @Input() gSession: GoogleSession;
  @Input() index: number;
  @Output() createNewSessionEvent = new EventEmitter();
  @Output() deleteSessionEvent = new EventEmitter<string>();
  @Output() proxySelectedEvent = new EventEmitter();
  @Output() loginSessionEvent = new EventEmitter<GoogleSession>();
  @Output() createNewHarvesterEvent = new EventEmitter<CreateRecaptchaHarvesterEvent>();

  form: FormGroup;
  filteredOptions: Observable<string[]>;

  constructor() {
  }

  ngOnInit(): void {
    const defaultWebsite = this.availableModes[0];
    this.form = new FormGroup({
      mode: new FormControl(defaultWebsite),
      proxy: new FormControl(this.gSession.proxyList[0])
    });
    if (this.gSession.isDefault) this.form.controls['proxy'].disable();
  }

  get availableModes() {
    return RecaptchaHarvester.SUPPORTED_SITES;
  };

  get proxyListValue(): string {
    return this.form.controls['proxy'].value;
  };

  get mode() {
    return this.form.controls['mode'].value;
  };
  
  createNewSession() {
    this.createNewSessionEvent.emit();
  };

  deleteSession() {
    this.deleteSessionEvent.emit(this.gSession.id);
  };

  createNewHarvester() {
    this.gSession.setProxyList(this.proxyListValue);
    this.createNewHarvesterEvent.emit({
      mode: this.mode,
      gSession: this.gSession
    });
  };

  loginSession() {
    this.gSession.setProxyList(this.proxyListValue);
    this.loginSessionEvent.emit(this.gSession);
  };

  optionSelectedHandler({option}: MatAutocompleteSelectedEvent) {
    this.gSession.setProxyList(option.value);
    this.proxySelectedEvent.emit();
  };

}
