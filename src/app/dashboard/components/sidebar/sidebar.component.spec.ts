import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ElectronService } from 'app/core/services/electron/electron.service';

import { SidebarComponent } from './sidebar.component';


describe('SidebarComponent', () => {
  let component: SidebarComponent;
  let fixture: ComponentFixture<SidebarComponent>;
  let electronService: jasmine.SpyObj<ElectronService>;

  beforeEach(async () => {
    const electronSpy = jasmine.createSpyObj('ElectronService', [], [
      'appVersion'
    ]);
    await TestBed.configureTestingModule({
      declarations: [ SidebarComponent ],
      providers: [
        {provide: ElectronService, useValue: electronSpy}
      ]
    })
    .compileComponents();

    electronService = TestBed.inject(ElectronService) as jasmine.SpyObj<ElectronService>;
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
