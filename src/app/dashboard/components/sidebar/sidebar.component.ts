import { Component, OnInit } from '@angular/core';

import { ElectronService } from 'app/core/services/electron/electron.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  version: string;
  constructor(
    private electron: ElectronService
  ) { 
    this.version = this.electron.appVersion;
  }

  ngOnInit(): void {
  }

}
