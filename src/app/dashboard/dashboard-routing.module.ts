import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { AuthGuard } from 'app/auth.guard';

import { ProfilesComponent } from './profiles/profiles.component';
import { CaptchaComponent } from './captcha/captcha.component';
import { SettingsComponent } from './settings/settings.component';
import { TasksComponent } from './tasks/tasks.component';

const routes: Routes = [
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivateChild: [ AuthGuard ],
    children: [
      {
        path: 'tasks',
        component: TasksComponent,
      },
      {
        path: 'profiles',
        component: ProfilesComponent,
      },
      {
        path: 'settings',
        component: SettingsComponent,
      },
      {
        path: 'recaptcha',
        component: CaptchaComponent,
      },
    ]
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule, 
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class DashboardRoutingModule {}
