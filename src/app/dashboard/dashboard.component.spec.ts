import { Component } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardComponent } from './dashboard.component';

@Component({selector: 'app-traffic-lights', template: ''})
class TrafficLightsComponent {};

@Component({selector: 'app-sidebar', template: ''})
class SidebarComponent {};

@Component({selector: 'router-outlet', template: ''})
class RouterOutletComponent {};

describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ 
        DashboardComponent, 
        TrafficLightsComponent, 
        SidebarComponent, 
        RouterOutletComponent
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
