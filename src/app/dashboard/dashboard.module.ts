import { NgModule } from '@angular/core';

import { CoreModule } from 'app/core/core.module';
import { SharedModule } from '../shared/shared.module';

import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';

import { HeaderModule } from './header/header.module';
import { ProfilesModule } from './profiles/profiles.module';
import { CaptchaModule } from './captcha/captcha.module';
import { SettingsModule } from './settings/settings.module';
import { TasksModule } from './tasks/tasks.module';

import {
  SidebarComponent,
} from './components';

@NgModule({
  declarations: [
    DashboardComponent,
    SidebarComponent,
  ],
  imports: [
    CoreModule, 
    SharedModule,
    HeaderModule,
    ProfilesModule,
    CaptchaModule,
    SettingsModule,
    TasksModule, 
    DashboardRoutingModule,
  ],
  exports: [
  ],
})
export class DashboardModule {}
