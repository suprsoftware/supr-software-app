import { NgModule } from '@angular/core';

import { CoreModule } from 'app/core/core.module';

import { HeaderComponent } from './header.component';

@NgModule({
  declarations: [
    HeaderComponent,
  ],
  imports: [
    CoreModule,
  ],
  exports: [
    HeaderComponent,
  ]
})
export class HeaderModule {};