import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ProfilesDialogComponent } from './profiles-dialog.component';
import { ProfileDialogData } from 'app/lib/models/profile-dialog.model';
import { Profile } from 'app/lib/models/profile';

describe('ProfilesDialogComponent', () => {
  let component: ProfilesDialogComponent;
  let fixture: ComponentFixture<ProfilesDialogComponent>;

  let matDialogRef: jasmine.SpyObj<MatDialogRef<ProfilesDialogComponent, ProfileDialogData>>;
  let testData: ProfileDialogData;

  beforeEach(async () => {
    const matDialogRefSpy = jasmine.createSpyObj('MatDialogRef', [
      'close'
    ]);

    testData = {
      profileForm: (new Profile()).toFormGroup(),
      immutableProfileName: false,
    };
    await TestBed.configureTestingModule({
      declarations: [ ProfilesDialogComponent ],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        MatDialogModule,
        MatIconModule,
        MatDividerModule,
        MatSelectModule
      ],
      providers: [
        {provide: MatDialogRef, useValue: matDialogRefSpy},
        {provide: MAT_DIALOG_DATA, useValue: testData}
      ],
    })
    .compileComponents();

    matDialogRef = TestBed.inject(MatDialogRef) as jasmine.SpyObj<MatDialogRef<ProfilesDialogComponent, ProfileDialogData>>;
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfilesDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
