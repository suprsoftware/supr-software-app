import { Component, Inject, OnInit, OnDestroy } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

import { ProfileDialogData, ProfileDialogReturn } from 'app/lib/models/profile-dialog.model';
import { CreditCard } from 'app/lib/models/credit-card';
import { Address } from 'app/lib/models/address';
import { BehaviorSubject, Subscription } from 'rxjs';

@Component({
  selector: 'app-profiles-dialog',
  templateUrl: './profiles-dialog.component.html',
  styleUrls: ['./profiles-dialog.component.scss']
})
export class ProfilesDialogComponent implements OnInit, OnDestroy {
  cardExpMonths: string[];
  cardExpYears: string[];
  availableCoutries: string[];
  availableStates: BehaviorSubject<[string, string][]>;

  billingAddressCountrySub: Subscription;
  shippingAddressCountrySub: Subscription;
  billingAddressNameSub: Subscription;
  shippingAddressNameSub: Subscription;
  sameBillingAndShippingSub: Subscription;

  constructor(
    private dialogRef: MatDialogRef<ProfilesDialogComponent, ProfileDialogReturn>,
    @Inject(MAT_DIALOG_DATA) public data: ProfileDialogData
  ) {
    const defaultCountryName = 'United States';
    this.cardExpMonths = CreditCard.expMonths;
    this.cardExpYears = CreditCard.expYears;
    this.availableCoutries = Object.keys(Address.countries);
    this.availableStates = new BehaviorSubject(this.getAvailableStatesFromCountryName(defaultCountryName));
  }

  ngOnInit(): void {
    this.initCountryValueChanges();
    this.initNameOnCardValueChanges();
  }

  ngOnDestroy(): void {
    this.billingAddressCountrySub.unsubscribe();
    this.shippingAddressCountrySub.unsubscribe();
    this.billingAddressNameSub.unsubscribe();
    this.shippingAddressNameSub.unsubscribe();
  };

  initCountryValueChanges() {
    this.billingAddressCountrySub = this.data.profileForm.get('billingAddress.country')
      .valueChanges
      .subscribe((value) => {
        this.availableStates.next(this.getAvailableStatesFromCountryName(value));
      });
    this.shippingAddressCountrySub = this.data.profileForm.get('shippingAddress.country')
      .valueChanges
      .subscribe((value) => {
        this.availableStates.next(this.getAvailableStatesFromCountryName(value));
      });
  };

  initSameBillingAndShippingValueChanges() {

  };

  initNameOnCardValueChanges() {
    this.billingAddressNameSub = this.data.profileForm.get('billingAddress.name')
      .valueChanges
      .subscribe((value) => {
        this.data.profileForm.get('paymentDetails.nameOnCard').setValue(value);
      });
    this.shippingAddressNameSub = this.data.profileForm.get('shippingAddress.name')
      .valueChanges
      .subscribe((value) => {
        this.data.profileForm.get('paymentDetails.nameOnCard').setValue(value);
      });
  };

  getAvailableStatesFromCountry(country: {[key: string]: string}) {
    return Object.entries(country);
  };

  getAvailableStatesFromCountryName(countryName: string): [string, string][] {
    return Object.entries(Address.countries[countryName]);
  };

  save() {
    this.dialogRef.close(this.data.profileForm.getRawValue() as ProfileDialogReturn);
  };

  close() {
    this.dialogRef.close();
  };

}
