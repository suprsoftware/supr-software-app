import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatIconModule } from '@angular/material/icon';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { ProfilesTableComponent } from './profiles-table.component';
import { ProfilesService } from 'app/core/services/profiles/profiles.service';
import { ElectronService } from 'app/core/services/electron/electron.service';
import { SnackBarService } from 'app/core/services/snack-bar/snack-bar.service';
import { of } from 'rxjs';

describe('ProfilesTableComponent', () => {
  let component: ProfilesTableComponent;
  let fixture: ComponentFixture<ProfilesTableComponent>;

  let dialog: jasmine.SpyObj<MatDialog>;
  let profilesService: jasmine.SpyObj<ProfilesService>;
  let electronService: jasmine.SpyObj<ElectronService>;
  let snackBarService: jasmine.SpyObj<SnackBarService>;

  beforeEach(async () => {
    const dialogSpy = jasmine.createSpyObj('MatDialog', [
      'open'
    ]);
    const profilesServiceSpy = jasmine.createSpyObj('ProfilesService', [
      'importFromFile',
      'exportToFile',
      'saveProfile',
      'getNewProfileName',
      'deleteProfile',
      'copyProfile',
    ], {
      profiles$: of(null),
    });
    const electronServiceSpy = jasmine.createSpyObj('ElectronService', [
      'openDesktopFolder',
      'saveDesktopFolder'
    ]);
    const snackBarServiceSpy = jasmine.createSpyObj('SnackBarService', [
      'openGood',
      'openBad',
    ]);

    await TestBed.configureTestingModule({
      declarations: [ ProfilesTableComponent ],
      imports: [
        MatIconModule,
        MatDialogModule,
      ],
      providers: [
        {provide: MatDialog, useValue: dialogSpy},
        {provide: ProfilesService, useValue: profilesServiceSpy},
        {provide: ElectronService, useValue: electronServiceSpy},
        {provide: SnackBarService, useValue: snackBarServiceSpy},
      ]
    })
    .compileComponents();

    dialog = TestBed.inject(MatDialog) as jasmine.SpyObj<MatDialog>;
    profilesService = TestBed.inject(ProfilesService) as jasmine.SpyObj<ProfilesService>;
    electronService = TestBed.inject(ElectronService) as jasmine.SpyObj<ElectronService>;
    snackBarService = TestBed.inject(SnackBarService) as jasmine.SpyObj<SnackBarService>;
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfilesTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
