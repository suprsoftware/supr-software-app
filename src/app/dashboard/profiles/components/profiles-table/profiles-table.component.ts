import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

import { ProfilesService } from 'app/core/services/profiles/profiles.service';
import { ElectronService } from 'app/core/services/electron/electron.service';
import { SnackBarService } from 'app/core/services/snack-bar/snack-bar.service';

import { ProfilesDialogComponent } from '../profiles-dialog/profiles-dialog.component';
import { ProfileDialogData, ProfileDialogReturn } from 'app/lib/models/profile-dialog.model';
import { Profile } from 'app/lib/models/profile';
import { ProfileValidator } from 'app/lib/validators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-profiles-table',
  templateUrl: './profiles-table.component.html',
  styleUrls: ['./profiles-table.component.scss']
})
export class ProfilesTableComponent implements OnInit {
  profileValidator: ProfileValidator;
  profiles$: Observable<Profile[]>;

  constructor(
    private dialog: MatDialog,
    private profilesService: ProfilesService,
    private electronService: ElectronService,
    private snackbarService: SnackBarService,
  ) { 
    this.profileValidator = new ProfileValidator(this.profilesService);
    this.profiles$ = this.profilesService.profiles$.pipe();
  }

  ngOnInit(): void {

  }

  importProfiles() {
    this.electronService.openDesktopFolder()
      .then(({filePaths}) => {
        if (!filePaths[0]) return this.importProfilesFailedHandler();
        const filePath = filePaths[0];
        this.profilesService.importFromFile(filePath)
          .then(didImport => {
            if (!didImport) return this.importProfilesFailedHandler();
            this.profilesService.saveProfiles();
            return this.importProfilesSuccessHandler();
          });
      });
  };

  importProfilesSuccessHandler() {
    this.snackbarService.openGood('Profiles import');
  };

  importProfilesFailedHandler() {
    this.snackbarService.openBad('Profiles import');
  };

  exportProfiles() {
    this.electronService.saveDesktopFolder()
      .then(({filePath}) => {
        if (!filePath) return this.exportProfilesFailedHandler();
        this.profilesService.exportToFile(filePath)
          .then(didExport => {
            if (didExport) return this.exportProfilesSuccessHandler();
            return this.exportProfilesFailedHandler();
          });
      });
  };

  exportProfilesSuccessHandler() {
    this.snackbarService.openGood('Profiles export');
  };

  exportProfilesFailedHandler() {
    this.snackbarService.openBad('Profiles export');
  };

  createProfile() {
    const formGroup = this.createNewFormGroup();
    const dialogRef = this.openProfileDialog({
      profileForm: formGroup,
      immutableProfileName: false,
    });
    this.handleDialogRef(dialogRef);
  };

  editProfile(profile: Profile) {
    const formGroup = this.createFormGroupFromProfile(profile);
    formGroup.controls['name'].disable();
    const dialogRef = this.openProfileDialog({
      profileForm: formGroup,
      immutableProfileName: true,
    });
    this.handleDialogRef(dialogRef);
  };

  handleDialogRef(dialogRef: MatDialogRef<ProfilesDialogComponent, ProfileDialogReturn>) {
    const dialogSub = dialogRef
      .afterClosed()
      .subscribe({
        next: (result) => {
          if (!!result) this.profilesService.saveProfileAsInterfaceAndPush(result);
        },
        complete: () => {
          dialogSub.unsubscribe();
        }
      });
  };

  createNewFormGroup() {
    const newProfile = new Profile();
    newProfile.name = this.getNewProfileName();
    const formGroup = newProfile.toFormGroup();
    this.setAsyncNameValidators(formGroup);
    return formGroup;
  };

  createFormGroupFromProfile(profile: Profile) {
    const formGroup = profile.toFormGroup();
    this.setAsyncNameValidators(formGroup);
    return formGroup;
  };

  setAsyncNameValidators(formGroup: FormGroup) {
    const nameControl = formGroup.controls['name'];
    nameControl.setAsyncValidators(this.profileValidator.name);
  };

  getNewProfileName(): string {
    return this.profilesService.getNewProfileName();
  };

  openProfileDialog(data: ProfileDialogData): MatDialogRef<ProfilesDialogComponent, ProfileDialogReturn> {
    return this.dialog.open(ProfilesDialogComponent, {
      data,
      panelClass: 'dialog-container',
      autoFocus: false,
      minHeight: '600px',
      minWidth: '480px',
      height: '600px',
      width: '480px',
      
    });
  };

  deleteProfile(profile: Profile) {
    return this.profilesService.deleteProfile(profile);
  };

  copyProfile(profile: Profile) {
    return this.profilesService.copyProfile(profile);
  };

}
