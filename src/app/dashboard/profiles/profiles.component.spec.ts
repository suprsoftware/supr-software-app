import { Component } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfilesComponent } from './profiles.component';

import { ProfilesService } from 'app/core/services/profiles/profiles.service';

@Component({selector: 'app-header', template: ''})
class HeaderComponent {};

@Component({selector: 'app-profiles-table', template: ''})
class ProfilesTableComponent {};

@Component({selector: 'app-profiles-grouper', template: ''})
class ProfilesGrouperComponent {};

describe('ProfilesComponent', () => {
  let component: ProfilesComponent;
  let fixture: ComponentFixture<ProfilesComponent>;
  let profilesService: jasmine.SpyObj<ProfilesService>;

  beforeEach(async () => {
    const profilesSpy = jasmine.createSpyObj('ProfilesService', [
      'saveProfiles'
    ]);

    await TestBed.configureTestingModule({
      declarations: [ 
        ProfilesComponent,
        HeaderComponent,
        ProfilesTableComponent,
        ProfilesGrouperComponent
      ],
      providers: [
        {provide: ProfilesService, useValue: profilesSpy},
      ]
    })
    .compileComponents();
    profilesService = TestBed.inject(ProfilesService) as jasmine.SpyObj<ProfilesService>;

  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfilesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('methods', () => {
    describe('ngOnInit', () => {
      it('should set lastVisistedPage', () => {
        const setItemSpy = spyOn(window.sessionStorage, 'setItem');
        component.ngOnInit();
        const [key] = setItemSpy.calls.argsFor(0);
        expect(window.sessionStorage.setItem).toHaveBeenCalledTimes(1);
        expect(key).toBe('lastVisitedPage');
      });
    });

    describe('ngOnDestroy', () => {
      it('should save profiles on destroy', () => {
        component.ngOnDestroy();
        expect(profilesService.saveProfiles).toHaveBeenCalledTimes(1);
      });
    });
  });
});
