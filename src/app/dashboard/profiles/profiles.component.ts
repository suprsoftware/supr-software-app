import { Component, OnInit, OnDestroy } from '@angular/core';

import { ProfilesService } from 'app/core/services/profiles/profiles.service';

@Component({
  selector: 'app-profiles',
  templateUrl: './profiles.component.html',
  styleUrls: ['./profiles.component.scss']
})
export class ProfilesComponent implements OnInit, OnDestroy {

  constructor(
    private profilesService: ProfilesService,
  ) { }

  ngOnInit(): void {
    window.sessionStorage.setItem('lastVisitedPage', window.location.hash);
  }

  ngOnDestroy(): void {
    this.profilesService.saveProfiles();
  };

}
