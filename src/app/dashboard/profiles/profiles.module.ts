import { NgModule } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSelectModule } from '@angular/material/select';
import { MatDividerModule } from '@angular/material/divider';

import { CoreModule } from 'app/core/core.module';
import { SharedModule } from 'app/shared/shared.module';
import { HeaderModule } from 'app/dashboard/header/header.module';

import { ProfilesComponent } from './profiles.component';
import { 
  ProfilesDialogComponent, 
  ProfilesTableComponent,
} from './components';

@NgModule({
  declarations: [
    ProfilesComponent,
    ProfilesDialogComponent,
    ProfilesTableComponent,
  ],
  imports: [
    CoreModule,
    SharedModule,
    HeaderModule,
    MatIconModule,
    MatDialogModule,
    MatSelectModule,
    MatDividerModule,
  ],
  exports: [

  ]
})
export class ProfilesModule {};