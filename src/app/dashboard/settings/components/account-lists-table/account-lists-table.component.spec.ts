import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatIconModule } from '@angular/material/icon';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';

import { AccountListsTableComponent } from './account-lists-table.component';

import { AccountsService } from 'app/core/services/accounts/accounts.service';
import { of } from 'rxjs';
import { ElectronService } from 'app/core/services/electron/electron.service';
import { SnackBarService } from 'app/core/services/snack-bar/snack-bar.service';

describe('AccountListsTableComponent', () => {
  let component: AccountListsTableComponent;
  let fixture: ComponentFixture<AccountListsTableComponent>;

  let accountsService: jasmine.SpyObj<AccountsService>;
  let dialog: jasmine.SpyObj<MatDialog>;
  let electronService: jasmine.SpyObj<ElectronService>;
  let snackBarService: jasmine.SpyObj<SnackBarService>;

  beforeEach(async () => {
    const accountsServiceSpy = jasmine.createSpyObj('AccountsService', [
      'clearAccountList',
      'saveAccountList'
    ], {
      'accountListMap$': of(null),
      'count': 0
    });
    const dialogSpy = jasmine.createSpyObj('MatDialog', [
      'open'
    ]);
    const electronSpy = jasmine.createSpyObj('ElectronService', [
      'openDesktopFolder',
      'saveDesktopFolder'
    ]);
    const snackBarSpy = jasmine.createSpyObj('SnackBarService', [
      'openGood',
      'openBad'
    ]);

    await TestBed.configureTestingModule({
      declarations: [ AccountListsTableComponent ],
      imports: [
        MatIconModule,
        MatDialogModule,
      ],
      providers: [
        {provide: AccountsService, useValue: accountsServiceSpy},
        {provide: MatDialog, useValue: dialogSpy},
        {provide: ElectronService, useValue: electronSpy},
        {provide: SnackBarService, useValue: snackBarSpy}
      ]
    })
    .compileComponents();
    accountsService = TestBed.inject(AccountsService) as jasmine.SpyObj<AccountsService>;
    dialog = TestBed.inject(MatDialog) as jasmine.SpyObj<MatDialog>;
    electronService = TestBed.inject(ElectronService) as jasmine.SpyObj<ElectronService>;
    snackBarService = TestBed.inject(SnackBarService) as jasmine.SpyObj<SnackBarService>;
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountListsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('constructor', () => {
    it('should create', () => {
      expect(component).toBeTruthy();
    });
  });

  describe('getters', () => {
    describe('totalAccounts', () => {
      it('should return the current count of accounts', () => {
        expect(component.totalAccounts).toBe(0);
      });
    });
  });

  describe('methods', () => {
    describe('clearAccountList', () => {
      it('should empty account list', () => {
        component.clearAndSaveAccountList('Not Supported');
        expect(accountsService.clearAccountList).toHaveBeenCalledOnceWith('Not Supported');
      });
    });
  });
});
