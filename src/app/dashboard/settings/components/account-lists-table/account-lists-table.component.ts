import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { AccountsService } from 'app/core/services/accounts/accounts.service';
import { ElectronService } from 'app/core/services/electron/electron.service';
import { SnackBarService } from 'app/core/services/snack-bar/snack-bar.service';
import { AccountList } from 'app/lib/models/account-list.model';
import { TextareaDialogData, TextareaDialogReturn } from 'app/lib/models/textarea-dialog.model';
import { TextareaDialogComponent } from 'app/shared/components';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-account-lists-table',
  templateUrl: './account-lists-table.component.html',
  styleUrls: ['./account-lists-table.component.scss']
})
export class AccountListsTableComponent implements OnInit {
  accountList$: Observable<AccountList[]>;

  constructor(
    private accountsService: AccountsService,
    private electron: ElectronService,
    private snackBar: SnackBarService,
    private dialog: MatDialog,
  ) { 
    this.accountList$ = this.accountsService.accountListMap$.pipe();
  }

  get totalAccounts() {
    return this.accountsService.count;
  };

  ngOnInit(): void {
  }

  importAccountList() {
    this.electron.openDesktopFolder()
      .then(({filePaths}) => {
        if (!filePaths[0]) return this.accountListImportFailed();
        const filepath = filePaths[0];
        this.accountsService.importFromFile(filepath)
          .then(didImport => {
            if (!didImport) return this.accountListImportFailed();
            this.accountsService.saveAccountList();
            return this.accountListImportSuccess();
          });
      });
  };

  accountListImportFailed() {
    this.snackBar.openBad('Account list import');
  };

  accountListImportSuccess() {
    this.snackBar.openGood('Account list import');
  };

  exportAccountList() {
    this.electron.saveDesktopFolder()
      .then(({filePath}) => {
        if (!filePath) return this.accountListExportFailed();
        this.accountsService.exportToFile(filePath)
          .then(didExport => {
            if (didExport) return this.accountListExportSuccess();
            return this.accountListExportFailed();
          });
      });
  };

  accountListExportFailed() {
    this.snackBar.openBad('Account list export');
  };

  accountListExportSuccess() {
    this.snackBar.openGood('Account list export');
  };

  editAccountList(accountList: AccountList) {
    const formArray = this.createAccountListFormArray(accountList);
    const data: TextareaDialogData = {
      formArray,
      immutableTitle: true,
    };
    const dialogRef = this.openAccountListDialog(data);
    const dialogRefSubscription = dialogRef.afterClosed().subscribe({
      next: (result) => {
        this.setAndSaveAccountList(result);
      },
      complete: () => {
        dialogRefSubscription.unsubscribe();
      }
    });
  };


  createAccountListFormArray(accountList: AccountList) {
    const data = accountList[1]
      .join('\n')
      .trim();
    
    return new FormArray([
      new FormControl(accountList[0], [Validators.required]),
      new FormControl(data, [Validators.required])
    ]);
  };

  openAccountListDialog(data: TextareaDialogData): MatDialogRef<TextareaDialogComponent, TextareaDialogReturn> {
    return this.dialog.open(TextareaDialogComponent, {
      data
    });
  };

  setAndSaveAccountList(result: TextareaDialogReturn) {
    if (!result) return;
    const accountListName = result[0];
    const accountListAsString = result[1];
    const accounts = accountListAsString
      .trim()
      .split('\n');
    this.accountsService.setAccountList([accountListName, accounts]);
    this.accountsService.saveAccountList();
  };

  clearAndSaveAccountList(accountListName: string) {
    this.accountsService.clearAccountList(accountListName);
    this.accountsService.saveAccountList();
  };

}
