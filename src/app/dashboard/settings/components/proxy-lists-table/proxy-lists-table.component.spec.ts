import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule, MatDialog } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';

import { ElectronService } from 'app/core/services/electron/electron.service';
import { ProxiesService } from 'app/core/services/proxies/proxies.service';
import { SnackBarService } from 'app/core/services/snack-bar/snack-bar.service';
import { of } from 'rxjs';

import { ProxyListsTableComponent } from './proxy-lists-table.component';

describe('ProxyListsTableComponent', () => {
  let component: ProxyListsTableComponent;
  let fixture: ComponentFixture<ProxyListsTableComponent>;

  let electronService: jasmine.SpyObj<ElectronService>;
  let proxiesService: jasmine.SpyObj<ProxiesService>;
  let snackBarService: jasmine.SpyObj<SnackBarService>;
  let dialog: jasmine.SpyObj<MatDialog>;

  beforeEach(async () => {
    const electronSpy = jasmine.createSpyObj('ElectronService', [
      'openDesktopFolder',
      'saveDesktopFolder'
    ]);
    const proxiesSpy = jasmine.createSpyObj('ProxiesService', [
      'saveProxyList',
      'importFromFile',
      'exportToFile',
      'deleteOrClearProxyList',
      'isProxyListMutable',
      'isInProxyLists',
      'setProxyList',
    ], {
      'proxyListMap$': of(null),
      'proxyCount': 2
    });
    const snackBarSpy = jasmine.createSpyObj('SnackBarService', [
      'openGood',
      'openBad',
    ]);
    const dialogSpy = jasmine.createSpyObj('MatDialog', [
      'open',
    ]);

    await TestBed.configureTestingModule({
      declarations: [ ProxyListsTableComponent ],
      imports: [
        MatDialogModule,
        MatIconModule,
      ],
      providers: [
        {provide: ElectronService, useValue: electronSpy},
        {provide: ProxiesService, useValue: proxiesSpy},
        {provide: SnackBarService, useValue: snackBarSpy},
        {provide: MatDialog, useValue: dialogSpy},
      ]
    })
    .compileComponents();

    electronService = TestBed.inject(ElectronService) as jasmine.SpyObj<ElectronService>;
    proxiesService = TestBed.inject(ProxiesService) as jasmine.SpyObj<ProxiesService>;
    snackBarService = TestBed.inject(SnackBarService) as jasmine.SpyObj<SnackBarService>;
    dialog = TestBed.inject(MatDialog) as jasmine.SpyObj<MatDialog>;
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProxyListsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('getters', () => {
    it('should return the total proxy count', () => {
      expect(component.proxyCount).toBe(2);
    });
  });

  describe('ngOnDestroy', () => {
    it('should save current proxies', () => {
      component.ngOnDestroy();
      expect(proxiesService.saveProxyList).toHaveBeenCalled();
    });
  });
  
  describe('importProxyList', () => {});
  describe('exportProxyList', () => {});
  describe('proxyListImportSuccessHandler', () => {});
  describe('proxyListImportFailedHandler', () => {});
  describe('proxyListExportSuccessHandler', () => {});
  describe('proxyListExportFailedHandler', () => {});
  describe('deleteOrClearProxyList', () => {});
  describe('editProxyList', () => {});
  describe('createProxyList', () => {});
  describe('getNewProxyListName', () => {});
  describe('createProxyListFormArray', () => {});

});
