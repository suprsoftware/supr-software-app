import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormArray, FormControl, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Observable, } from 'rxjs';

import { TextareaDialogComponent } from 'app/shared/components';
import { TextareaDialogData, TextareaDialogReturn } from 'app/lib/models/textarea-dialog.model';
import { Proxy } from 'app/lib/models/proxy';
import { ProxyList } from 'app/lib/models/proxy-list.model';
import { ProxyListValidator } from 'app/lib/validators/';

import { ProxiesService } from 'app/core/services/proxies/proxies.service';
import { ElectronService } from 'app/core/services/electron/electron.service';
import { SnackBarService } from 'app/core/services/snack-bar/snack-bar.service';

@Component({
  selector: 'app-proxy-lists-table',
  templateUrl: './proxy-lists-table.component.html',
  styleUrls: ['./proxy-lists-table.component.scss']
})
export class ProxyListsTableComponent implements OnInit, OnDestroy {

  proxyLists$: Observable<ProxyList[]>;

  constructor(
    private proxyService: ProxiesService,
    private electron: ElectronService,
    private snackBar: SnackBarService,
    private dialog: MatDialog,
  ) { 
    this.proxyLists$ = this.proxyService.proxyListMap$.pipe();
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.proxyService.saveProxyList();
  };

  get proxyCount() {
    return this.proxyService.proxyCount;
  };

  importProxyList() {
    this.electron.openDesktopFolder()
      .then(({filePaths}) => {
        if (!filePaths[0]) return this.proxyListImportFailedHandler();
        const filePath = filePaths[0];
        this.proxyService.importFromFile(filePath)
          .then(didImport => {
            if (!didImport) return this.proxyListImportFailedHandler();
            this.proxyService.saveProxyList();
            return this.proxyListImportSuccessHandler();
          });
      });
  };

  proxyListImportSuccessHandler() {
    this.snackBar.openGood('Proxy list import');
  };

  proxyListImportFailedHandler() {
    this.snackBar.openBad('Proxy list import')
  };

  exportProxyList() {
    this.electron.saveDesktopFolder()
      .then(({filePath}) => {
        if (!filePath) return this.proxyListExportFailedHandler();
        this.proxyService.exportToFile(filePath)
          .then(didExport => {
            if (didExport) return this.proxyListExportSuccessHandler();
            return this.proxyListExportFailedHandler();
          });
      });
  };

  proxyListExportSuccessHandler() {
    this.snackBar.openGood('Proxy list export');
  };

  proxyListExportFailedHandler() {
    this.snackBar.openBad('Proxy list export')
  };

  deleteOrClearProxyList(proxyListName: string) {
    this.proxyService.deleteOrClearProxyList(proxyListName);
    this.proxyService.saveProxyList();
  };

  editProxyList(proxyList: ProxyList) {
    const proxyListName = proxyList[0];
    const proxies = proxyList[1];
    if (!this.proxyService.isProxyListMutable(proxyListName)) return;
    const proxyListAsString = Proxy.proxyListToString(proxies);
    const formArray = this.createProxyListFormArray(proxyListName, proxyListAsString);
    const data: TextareaDialogData = {
      formArray,
      immutableTitle: true,
    };
    const dialogRef = this.openProxyListDialog(data);
    const dialogRefSubscription = dialogRef.afterClosed().subscribe({
      next: (result) => {
        this.saveProxyList(result);
      },
      complete: () => {
        dialogRefSubscription.unsubscribe();
      },
    })
  };

  createProxyList() {
    const defaultProxyListAsString = `hostname:80\nhostname:8080:user:pass`;
    const proxyListName = this.getNewProxyListName();
    const formArray = this.createProxyListFormArray(proxyListName, defaultProxyListAsString);
    const data: TextareaDialogData = {
      formArray,
      immutableTitle: false,
    };
    const dialogRef = this.openProxyListDialog(data);
    const dialogRefSubscription = dialogRef.afterClosed().subscribe({
      next: (result) => {
        this.saveProxyList(result);
      },
      complete: () => {
        dialogRefSubscription.unsubscribe();
      },
    });
  };

  getNewProxyListName() {
    return this.proxyService.getNewProxyListName();
  };

  createProxyListFormArray(proxyListName: string, proxyListAsString: string) {
    const proxyListValidator = new ProxyListValidator(this.proxyService);

    return new FormArray([
      new FormControl(proxyListName, [Validators.required], [proxyListValidator.name]),
      new FormControl(proxyListAsString, [Validators.required, ProxyListValidator.listAsString])
    ]);
  };

  openProxyListDialog(data: TextareaDialogData): MatDialogRef<TextareaDialogComponent, TextareaDialogReturn> {
    return this.dialog.open(TextareaDialogComponent, {
      data
    });
  };

  saveProxyList(result: TextareaDialogReturn) {
    if (!result) return;
    const proxyListName = result[0];
    const proxyListAsString = result[1];
    const proxies = Proxy.stringToProxyList(proxyListAsString);
    this.proxyService.setProxyList([proxyListName, proxies]);
    this.proxyService.saveProxyList();
  };

}
