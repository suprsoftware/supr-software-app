import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { ClipboardModule } from '@angular/cdk/clipboard';
import { SettingsComponent } from './settings.component';

import { AuthService } from 'app/core/services/auth/auth.service';
import { DbService } from 'app/core/services/db/db.service';
import { SnackBarService } from 'app/core/services/snack-bar/snack-bar.service';
import { of } from 'rxjs';

import { LicenseKey } from 'app/lib/models/license-key.model';
import { Component, Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'licenseKey'})
class LicenseKeyPipe implements PipeTransform {
  transform(value) {return value;};
};

@Component({selector: 'app-proxy-lists-table', template: ''})
class ProxyListsTableComponent {};

@Component({selector: 'app-account-lists-table', template: ''})
class AccountListsTableComponent {};

@Component({selector: 'app-header', template: ''})
class HeaderComponent {};

describe('SettingsComponent', () => {
  let component: SettingsComponent;
  let fixture: ComponentFixture<SettingsComponent>;
  let authService: jasmine.SpyObj<AuthService>;
  let dbService: jasmine.SpyObj<DbService>;
  let snackBarService: jasmine.SpyObj<SnackBarService>;
  let httpClient: jasmine.SpyObj<HttpClient>;

  beforeEach(async () => {
    const authSpy = jasmine.createSpyObj('AuthService', [
      'logout',
    ], {
      'activeKeyAsObservable': of({
        expires_at: { 
          toDate: () => new Date(),
        },
        settings: {
          discord_webhook: ''
        }
      } as LicenseKey)
    });
    const dbSpy = jasmine.createSpyObj('DbService', [
      'unbindLicenseKeyFromMachine',
      'updateLicenseKeyFields'
    ]);
    const snackBarSpy = jasmine.createSpyObj('SnackBarService', [
      'openGood'
    ]);
    const httpSpy = jasmine.createSpyObj('HttpClient', [
      'post'
    ]);

    await TestBed.configureTestingModule({
      declarations: [ 
        SettingsComponent, 
        LicenseKeyPipe, 
        ProxyListsTableComponent,
        HeaderComponent,
        AccountListsTableComponent,
      ],
      imports: [
        HttpClientTestingModule,
        FormsModule,
        MatIconModule,
        ClipboardModule,
      ],
      providers: [
        {provide: AuthService, useValue: authSpy},
        {provide: DbService, useValue: dbSpy},
        {provide: SnackBarService, useValue: snackBarSpy},
        {provide: HttpClient, useValue: httpSpy},
      ],
    })
    .compileComponents();
    authService = TestBed.inject(AuthService) as jasmine.SpyObj<AuthService>;
    dbService = TestBed.inject(DbService) as jasmine.SpyObj<DbService>;
    snackBarService = TestBed.inject(SnackBarService) as jasmine.SpyObj<SnackBarService>;
    httpClient = TestBed.inject(HttpClient) as jasmine.SpyObj<HttpClient>;
  });

  beforeEach(() => {
    
    fixture = TestBed.createComponent(SettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('methods', () => {
    describe('ngOnInit', () => {
      it('should set lastVisistedPage', () => {
        const setItemSpy = spyOn(window.sessionStorage, 'setItem');
        component.ngOnInit();
        const [key] = setItemSpy.calls.argsFor(0);
        expect(window.sessionStorage.setItem).toHaveBeenCalledTimes(1);
        expect(key).toBe('lastVisitedPage');
      });
    });
    describe('deactivateMachine', () => {
      it('should unbind license key from machine', async () => {
        await component.deactivateMachine({} as LicenseKey);
        expect(dbService.unbindLicenseKeyFromMachine).toHaveBeenCalledTimes(1);
      });
    });
    describe('logout', () => {
      it('should logout', async () => {
        await component.logout();
        expect(authService.logout).toHaveBeenCalledTimes(1);
      });
    });
    describe('saveWebhookInput', () => {
      it('should update license key fields with testInput value', () => {
        component.saveWebhookInput({} as LicenseKey, {
          value: 'Test Value'
        } as HTMLInputElement);
        const [key, fields] = dbService.updateLicenseKeyFields.calls.argsFor(0);
        expect(key).toEqual({} as LicenseKey);
        expect(fields.settings.discord_webhook).toBe('Test Value');
      });

      it('should reset input value if input value is falsey', () => {
        const testInput = {
          value: ''
        } as HTMLInputElement;
        component.saveWebhookInput({
          settings: {
            discord_webhook: 'Reset Value'
          }
        } as LicenseKey, testInput);
        expect(dbService.updateLicenseKeyFields).toHaveBeenCalledTimes(0);
        expect(testInput.value).toBe('Reset Value');
      });
    });
    describe('clearWebhookInput', () => {
      it('should set the value of the input to ""', () => {
        const testInput = {
          value: 'Value'
        } as HTMLInputElement;
        component.clearWebhookInput(testInput);
        expect(testInput.value).toBe('');
      });
    });
    describe('testWebhook', () => {
      it('should return if the webhook url is ""', () => {
        const testInput = {
          value: ''
        } as HTMLInputElement;
        component.testWebhook(testInput);
        expect(httpClient.post).toHaveBeenCalledTimes(0);
      });
      it('should return if the webhook url is not a discord webhook', () => {
        const testInput = {
          value: 'https://slack.com'
        } as HTMLInputElement;
        component.testWebhook(testInput);
        expect(httpClient.post).toHaveBeenCalledTimes(0);
      });
      it('should post if url is valid', () => {
        const testInput = {
          value: 'https://discordapp.com/api/'
        } as HTMLInputElement;
        httpClient.post.and.returnValue(of(null));
        component.testWebhook(testInput);
        expect(httpClient.post).toHaveBeenCalledTimes(1);
        expect(snackBarService.openGood).toHaveBeenCalledTimes(1);
      });
    });
    describe('isValidWebhook', () => {
      it('should return false for ""', () => {
        expect(component.isValidWebhook("")).toBeFalse();
      });
      it('should return false for "notaurl"', () => {
        expect(component.isValidWebhook("notaurl")).toBeFalse();
      });
      it('should return true for valid webhook', () => {
        expect(component.isValidWebhook('https://discordapp.com/api')).toBeTrue();
      });
    });
    describe('copyLicenseKey', () => {
      it('should present a snackbar', () => {
        component.copyLicenseKey();
        expect(snackBarService.openGood).toHaveBeenCalledTimes(1);
      });
    });
  });
});
