import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { AuthService } from 'app/core/services/auth/auth.service';
import { DbService } from 'app/core/services/db/db.service';
import { SnackBarService } from 'app/core/services/snack-bar/snack-bar.service';
import { LicenseKey } from 'app/lib/models/license-key.model';
import { DISCORD_WEBHOOK_TYPE } from 'app/lib/models/discord-webhook.model';
import { discordWebhookFactory } from 'app/lib/discord-webook-factory';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  activeKey$: Observable<LicenseKey>;

  constructor(
    private auth: AuthService,
    private db: DbService,
    private http: HttpClient,
    private snackBar: SnackBarService,
  ) { 
    this.activeKey$ = this.auth.activeKeyAsObservable.pipe();
  }

  ngOnInit(): void {
    window.sessionStorage.setItem('lastVisitedPage', window.location.hash);
  }

  async deactivateMachine(key: LicenseKey) {
    await this.db.unbindLicenseKeyFromMachine(key);
  };

  async logout() {
    await this.auth.logout();
  };

  saveWebhookInput(key: LicenseKey, webhookInput: HTMLInputElement) {
    const discord_webhook = webhookInput.value;
    if (!!discord_webhook) {
      this.db.updateLicenseKeyFields(key, {
        settings: {
          discord_webhook
        }
      });
    } else {
      webhookInput.value = key.settings.discord_webhook;
    };
  };

  clearWebhookInput(webhookInput: HTMLInputElement) {
    webhookInput.value = '';
  };

  testWebhook(webhookInput: HTMLInputElement) {
    const discord_webhook = webhookInput.value;
    if (!this.isValidWebhook(discord_webhook)) return;
    const payload = discordWebhookFactory(DISCORD_WEBHOOK_TYPE.TEST);
    this.http.post(discord_webhook, payload).subscribe();
    this.snackBar.openGood('Webhook sent');
  };

  isValidWebhook(webhook: string): boolean {
    return !!webhook && webhook.includes('discordapp.com/api');
  };

  copyLicenseKey() {
    this.snackBar.openGood('Copied key');
  };
}
