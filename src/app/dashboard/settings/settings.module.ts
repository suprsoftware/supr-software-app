import { NgModule } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { ClipboardModule } from '@angular/cdk/clipboard';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { CoreModule } from 'app/core/core.module';
import { SharedModule } from 'app/shared/shared.module';
import { HeaderModule } from '../header/header.module';

import { SettingsComponent } from './settings.component';
import { ProxyListsTableComponent, AccountListsTableComponent } from './components/';


@NgModule({
  declarations: [
    SettingsComponent,
    ProxyListsTableComponent,
    AccountListsTableComponent,
  ],
  imports: [
    CoreModule,
    SharedModule,
    HeaderModule,
    MatIconModule,
    MatSnackBarModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    ClipboardModule,
  ],
  exports: [
  ],
})
export class SettingsModule {};