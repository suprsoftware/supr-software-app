import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tasks-table',
  templateUrl: './tasks-table.component.html',
  styleUrls: ['./tasks-table.component.scss']
})
export class TasksTableComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  importTasks() {};

  exportTasks() {};

  createTask() {};

  startAllTasks() {};

  stopAllTasks() {};

  stopAndRemoveAllTasks() {};

}
