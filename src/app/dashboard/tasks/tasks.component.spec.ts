import { Component } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TasksComponent } from './tasks.component';

@Component({ selector: 'app-header', template: ''})
class TasksTableComponent {};

@Component({ selector: 'app-tasks-table', template: ''})
class HeaderComponent {};

describe('TasksComponent', () => {
  let component: TasksComponent;
  let fixture: ComponentFixture<TasksComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TasksComponent, TasksTableComponent, HeaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TasksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('methods', () => {
    describe('ngOnInit', () => {
      it('should set lastVisistedPage', () => {
        const setItemSpy = spyOn(window.sessionStorage, 'setItem');
        component.ngOnInit();
        const [key] = setItemSpy.calls.argsFor(0);
        expect(window.sessionStorage.setItem).toHaveBeenCalledTimes(1);
        expect(key).toBe('lastVisitedPage');
      });
    });
  });
});
