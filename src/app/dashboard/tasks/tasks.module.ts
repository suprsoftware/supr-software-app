import { NgModule } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';

import { CoreModule } from 'app/core/core.module';
import { SharedModule } from 'app/shared/shared.module';
import { HeaderModule } from 'app/dashboard/header/header.module';

import { TasksComponent } from './tasks.component';
import { TasksTableComponent } from './components';

@NgModule({
  declarations: [
    TasksComponent,
    TasksTableComponent,
  ],
  imports: [
    CoreModule,
    SharedModule,
    HeaderModule,
    MatIconModule,
  ],
  exports: [

  ]
})
export class TasksModule {};