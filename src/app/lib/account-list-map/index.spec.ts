import { Observable } from 'rxjs';
import { AccountListMap } from './';

describe('AccountListMap', () => {
  let accountListMap: AccountListMap;
  beforeEach(() => {
    accountListMap = new AccountListMap();
  });
  describe('constructor', () => {
    it('should create', () => {
      expect(accountListMap).toBeTruthy();
    });
  });

  describe('getters', () => {
    describe('size', () => {
      it('should return the size of the map', () => {
        expect(accountListMap.size).toBe(1);
      });
    });
    
    describe('count', () => {
      it('should return the number of accounts in the map', () => {
        expect(accountListMap.count).toBe(0);
      });
    });
    
    describe('asObservable', () => {
      it('should return an observable', (done) => {
        expect(accountListMap.asObservable).toBeInstanceOf(Observable);
        accountListMap.asObservable.subscribe(lists => {
          expect(lists).toBeInstanceOf(Array);
          done();
        });
      });
    });
  });

  describe('static', () => {
    describe('getters', () => {
      describe('supportedSites', () => {
        it('should return an array of supported sitenames', () => {
          expect(AccountListMap.supportedSites).toBeInstanceOf(Array);
          expect(AccountListMap.supportedSites.length).toBe(1);
        });
      });
    });
    describe('methods', () => {
      describe('setAccountList', () => {
        it('should return if site is not supported', () => {
          accountListMap.setAccountList(['Test', []]);
          expect(accountListMap.count).toBe(0);
        });
        it('should set in map if site is supported', () => {
          accountListMap.setAccountList(['Supreme Instore', ['user:pass']]);
          expect(accountListMap.count).toBe(1);
        });
      });

      describe('setAccountListAndPush', () => {
        it('should push latest values after setting account list', (done) => {
          accountListMap.setAccountListAndPush(['Supreme Instore', ['Test', 'Test']]);
          accountListMap.asObservable.subscribe(accountLists => {
            expect(accountLists[0]).toEqual(['Supreme Instore', ['Test', 'Test']]);
            done();
          });
        });
      });

      describe('clearAccountList', () => {
        it('should clear the specified account list', () => {
          accountListMap.setAccountList(['Supreme Instore', ['Test', 'Test']]);
          expect(accountListMap.count).toBe(2);
          accountListMap.clearAccountList('Supreme Instore');
          expect(accountListMap.count).toBe(0);
        });
        it('should not clear if site is not supported', () => {
          accountListMap.setAccountList(['Supreme Instore', ['Test', 'Test']]);
          expect(accountListMap.count).toBe(2);
          accountListMap.clearAccountList('Not Supported');
          expect(accountListMap.count).toBe(2);
        });
      });

      describe('isSiteSupported', () => {
        it('should return true if site is supported', () => {
          expect(accountListMap.isSiteSupported('Supreme Instore')).toBeTrue();
        });
        it('should return false if site is not supported', () => {
          expect(accountListMap.isSiteSupported('Test')).toBeFalse();
          expect(accountListMap.isSiteSupported('')).toBeFalse();
        });
      });
    });
  });

  describe('methods', () => {});
});