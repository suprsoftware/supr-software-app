import { BehaviorSubject, Observable } from 'rxjs';
import { AccountList } from '../models/account-list.model';
import { SUPPORTED_SITES } from '../models/supported-sites';

export class AccountListMap {
  private accountListMap: Map<string, string[]>;
  private accountListMap$: BehaviorSubject<AccountList[]>;

  constructor() {
    this.accountListMap = new Map();
    this.initAccountListMap();

    this.accountListMap$ = new BehaviorSubject(Array.from(this.accountListMap));
  };

  get size(): number {
    return this.accountListMap.size;
  };

  get count(): number {
    return Array.from(this.accountListMap.values())
      .reduce((prev, curr) => prev += curr.length, 0);
  };

  get asObservable(): Observable<AccountList[]> {
    return this.accountListMap$.pipe();
  };

  private initAccountListMap() {
    for (const site of AccountListMap.supportedSites) {
      this.accountListMap.set(site, []);
    };
  };

  private pushLatestValues() {
    this.accountListMap$.next(Array.from(this.accountListMap));
  };

  static get supportedSites(): string[] {
    return [
      SUPPORTED_SITES.SUPREME_INSTORE,
    ];
  };

  setAccountListAndPush(accountList: AccountList) {
    this.setAccountList(accountList);
    this.pushLatestValues();
  };

  setAccountList(accountList: AccountList) {
    const siteName = accountList[0];
    const accounts = accountList[1];
    if (this.isSiteNotSupported(siteName)) return;
    this.accountListMap.set(siteName, accounts);
  };

  clearAccountListAndPush(accountListName: string) {
    this.clearAccountList(accountListName);
    this.pushLatestValues();
  };

  clearAccountList(accountListName: string) {
    if (this.isSiteNotSupported(accountListName)) return;
    this.setAccountList([accountListName, []]);
  };

  isSiteSupported(siteName: string) {
    return AccountListMap.supportedSites.includes(siteName);
  };

  isSiteNotSupported(siteName: string) {
    return !this.isSiteSupported(siteName);
  };

  export(): [string, string[]][] {
    return Array.from(this.accountListMap);
  };

  import(data: [string, string[]][]) {
    for (let entry of data) {
      this.setAccountList(entry);
    };
    this.pushLatestValues();
  };
};