import { discordWebhookFactory } from './';
import { DISCORD_WEBHOOK_COLOR, DISCORD_WEBHOOK_TYPE } from '../models/discord-webhook.model';

describe('Discord Webhook Factory', () => {
  
  it('returns null for unrecognized webhook type', () => {
    expect(discordWebhookFactory('BADTYPE' as DISCORD_WEBHOOK_TYPE)).toBeNull();
  });

  describe('TEST webhook', () => {
    let payload;
    beforeEach(() => {
      payload = discordWebhookFactory(DISCORD_WEBHOOK_TYPE.TEST);
    });

    it('returns a payload', () => {
      expect(payload).toBeInstanceOf(Object);
    });
  
    it('has a username', () => {
      expect('username' in payload).toBeTrue();
    });
  
    it('has a avatar url', () => {
      expect('avatar_url' in payload).toBeTrue();
    });

    it('has embeds', () => {
      expect('embeds' in payload).toBeTrue();
      expect(payload.embeds.length > 0).toBeTrue();
    });
  
    it('has a footer', () => {
      expect('footer' in payload.embeds[0]).toBeTrue();
    });

    it('has correct embed color', () => {
      expect(payload.embeds[0].color).toBe(DISCORD_WEBHOOK_COLOR.GOOD);
    });
  });
});