import { MessageEmbed } from 'discord.js';
import { DISCORD_WEBHOOK_COLOR, DISCORD_WEBHOOK_TYPE } from '../models/discord-webhook.model';
import { version } from '../../../../package.json';

class BaseWebhookMessage {
  username = 'Supr Software Webhook';
  avatar_url = 'https://i.imgur.com/zq0Ou7N.png';
  embed: MessageEmbed;
  constructor() {
    this.embed = new MessageEmbed({
      color: DISCORD_WEBHOOK_COLOR.GOOD,
      footer: {
        text: `Supr Software - ${new Date().toISOString()}`,
        iconURL: 'https://i.imgur.com/zq0Ou7N.png'
      }
    });
  };

  toPayload() {
    return {
      username: this.username,
      avatar_url: this.avatar_url,
      embeds: [
        {...this.embed.toJSON()}
      ]
    };
  };
};

export function discordWebhookFactory(type: DISCORD_WEBHOOK_TYPE) {
  switch (type) {
    case DISCORD_WEBHOOK_TYPE.TEST: {
      const webhook = new BaseWebhookMessage();
      webhook.embed.setTitle('Webhook Success');
      webhook.embed.addField('Version', version, false);
      return webhook.toPayload();
    };
    default: {
      return null;
    };
  }
};