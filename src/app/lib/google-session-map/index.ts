import { BehaviorSubject } from 'rxjs';
import {GoogleSessionInterface, GoogleSession} from 'app/lib/models/google-session';

export class GoogleSessionMap {
  sessionMap: Map<string, GoogleSession>;
  sessionMap$: BehaviorSubject<GoogleSession[]>;

  constructor() {
    this.sessionMap = new Map();
    this.sessionMap$ = new BehaviorSubject(this.values);
  };

  get values(): GoogleSession[] {
    return Array.from(this.sessionMap.values());
  };

  setAndPushGoogleSession(session: GoogleSession) {
    this.setGoogleSession(session);
    this.pushLatestValues();
  };

  setGoogleSession(session: GoogleSession) {
    return this.sessionMap.set(session.id, session);
  };

  pushLatestValues() {
    this.sessionMap$.next(this.values);
  };

  export(): GoogleSessionInterface[] {
    return this.values
      .filter(g => g.isExportable)
      .map(g => g.export())
  };

  remove(id: string) {
    if (this.isNotRemovable(id)) return false;
    return this.sessionMap.delete(id);
  };

  isRemoveable(id: string) {
    return !this.isNotRemovable(id);
  };

  isNotRemovable(id: string) {
    return id.toLowerCase().trim() == 'default';
  };
};