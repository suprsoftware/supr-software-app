import { FormGroup } from '@angular/forms';
import { Address, AddressInterface } from './index';

describe('Address', () => {
  let address: Address;
  let fakeAddressInterface: AddressInterface;
  beforeEach(() => {
    address = new Address();
    fakeAddressInterface = {
      name: 'Fake Name',
      email: 'fake@email.com',
      phone: 'fake',
      country: 'United States',
      line1: 'Fake St',
      line2: 'Unit fake',
      city: 'Fake city',
      state: 'CA',
      postCode: 'Fake code'
    };
  });

  it('should create', () => {
    expect(address).toBeInstanceOf(Address);
    expect(address).toBeTruthy();
  });

  it('should create with address interface', () => {
    address = new Address(fakeAddressInterface);
    expect(address.name).toEqual('Fake Name');
    expect(address.email).toEqual('fake@email.com');
    expect(address.phone).toEqual('fake');
    expect(address.country).toEqual('United States');
    expect(address.line1).toEqual('Fake St');
    expect(address.line2).toEqual('Unit fake');
    expect(address.line3).toEqual('');
    expect(address.city).toEqual('Fake city');
    expect(address.state).toEqual('CA');
    expect(address.postCode).toEqual('Fake code');
  });

  describe('getters', () => {
    it('should get firstName', () => {
      address.name = 'John Doe';
      expect(address.firstName).toBe('John');
      address.name = 'John-Jr Doe';
      expect(address.firstName).toBe('John-Jr');
      address.name = '';
      expect(address.firstName).toBeFalsy();
      address.name = ' John Doe ';
      expect(address.firstName).toBe('John');
      address.name = ' John Doe';
      expect(address.firstName).toBe('John');
      address.name = 'John Jr Doe';
      expect(address.firstName).toBe('John');
    });
    it('should get lastName', () => {
      address.name = 'John Doe';
      expect(address.lastName).toBe('Doe');
      address.name = 'John-Jr Doe';
      expect(address.lastName).toBe('Doe');
      address.name = '';
      expect(address.lastName).toBeFalsy();
      address.name = ' John Doe ';
      expect(address.lastName).toBe('Doe');
      address.name = ' John Doe';
      expect(address.lastName).toBe('Doe');
      address.name = ' John Jr Doe';
      expect(address.lastName).toBe('Doe');
    });
  });

  describe('methods', () => {
    describe('toFormGroup', () => {
      it('should return instance of FormGroup', () => {
        const formGroup = address.toFormGroup();
        expect(formGroup).toBeInstanceOf(FormGroup);
        expect(formGroup.controls['name'].value).toBe('');
      });
      it('should have controls with values used at time of being called', () => {
        address = new Address(fakeAddressInterface);
        const formGroup = address.toFormGroup();
        expect(formGroup).toBeInstanceOf(FormGroup);
        expect(formGroup.controls['name'].value).toBe('Fake Name');
      });
    });
    describe('toJSON', () => {
      let fakeAddressJSON: AddressInterface;
      beforeEach(() => {
        address = new Address(fakeAddressInterface);
        fakeAddressJSON = address.toJSON();
      });
      it('should return address interface', () => {
        expect('name' in fakeAddressJSON).toBeTrue();
        expect('email' in fakeAddressJSON).toBeTrue();
        expect('phone' in fakeAddressJSON).toBeTrue();
        expect('country' in fakeAddressJSON).toBeTrue();
        expect('line1' in fakeAddressJSON).toBeTrue();
        expect('line2' in fakeAddressJSON).toBeTrue();
        expect('line3' in fakeAddressJSON).toBeTrue();
        expect('city' in fakeAddressJSON).toBeTrue();
        expect('state' in fakeAddressJSON).toBeTrue();
        expect('postCode' in fakeAddressJSON).toBeTrue();
      });
      it('should not return a reference to address instance', () => {
        fakeAddressJSON.name = '';
        expect(fakeAddressJSON.name).toEqual('');
        expect(address.name).toEqual('Fake Name');
      });
    });
    describe('fromJSON', () => {
      it('copies properties from interface to instance based on Address.properties', () => {
        address = new Address()
        address.fromJSON(fakeAddressInterface);
        expect(address.name).toEqual('Fake Name');
        expect(address.email).toEqual('fake@email.com');
        expect(address.phone).toEqual('fake');
        expect(address.country).toEqual('United States');
        expect(address.line1).toEqual('Fake St');
        expect(address.line2).toEqual('Unit fake');
        expect(address.line3).toEqual('');
        expect(address.city).toEqual('Fake city');
        expect(address.state).toEqual('CA');
        expect(address.postCode).toEqual('Fake code');
      });
    });
  });

  describe('static', () => {
    describe('get countries', () => {
      it('should return an object of { [key:string]: { [key:string]: string } }', () => {
        const countries = Address.countries;
        expect(Object.keys(countries).every(c => typeof c === 'string')).toBeTrue();
        expect(Object.values(countries).every(c => typeof c === 'object')).toBeTrue();
        expect(Object.values(countries).every(c => {
          return Object.values(c).every(s => typeof s === 'string');
        })).toBeTrue();
      });
    });
    
    describe('get properties', () => {
      it('should return an array of properties that matches AddressInterface', () => {
        const properties = Address.properties;
        expect(properties.includes('name')).toBeTrue();
        expect(properties.includes('email')).toBeTrue();
        expect(properties.includes('phone')).toBeTrue();
        expect(properties.includes('country')).toBeTrue();
        expect(properties.includes('line1')).toBeTrue();
        expect(properties.includes('line2')).toBeTrue();
        expect(properties.includes('line3')).toBeTrue();
        expect(properties.includes('city')).toBeTrue();
        expect(properties.includes('state')).toBeTrue();
        expect(properties.includes('postCode')).toBeTrue();
        expect(properties.length).toBe(10);
      });
    });
  });
});