import { FormGroup, FormControl, Validators } from '@angular/forms';

export interface AddressInterface {
  name: string;
  email: string;
  phone: string;
  country: string;
  line1: string;
  line2?: string;
  line3?: string;
  city: string;
  state: string;
  postCode: string;
};

export class Address implements AddressInterface {
  name: string = '';
  email: string = '';
  phone: string = '';
  country: string = Address.defaultCountry;
  line1: string = '';
  line2: string = '';
  line3: string = '';
  city: string = '';
  state: string = Address.defaultState;
  postCode: string = '';

  constructor(address?: AddressInterface) {
    if (!!address) this.fromJSON(address);
  };

  get firstName() {
    return this.name
      .trim()
      .split(' ')[0];
  };

  get lastName() {
    return this.name
      .trim()
      .split(' ')
      .pop();
  };

  toFormGroup() {
    return new FormGroup({
      'name': new FormControl(this.name, [Validators.required]),
      'email': new FormControl(this.email, [Validators.required, Validators.email]),
      'phone': new FormControl(this.phone, [Validators.required]),
      'country': new FormControl(this.country, [Validators.required]),
      'line1': new FormControl(this.line1, [Validators.required]),
      'line2': new FormControl(this.line2),
      'line3': new FormControl(this.line3),
      'city': new FormControl(this.city, [Validators.required]),
      'state': new FormControl(this.state),
      'postCode': new FormControl(this.postCode, [Validators.required]),
    });
  };
  
  toJSON(): AddressInterface {
    const obj = {};
    for (const prop of Address.properties) {
      obj[prop] = this[prop];
    };
    return obj as AddressInterface;
  };

  fromJSON(address: AddressInterface) {
    for (const prop of Address.properties) {
      switch (prop) {
        case 'country': {
          this[prop] = address[prop] ? address[prop] : Address.defaultCountry;
          break;
        };
        case 'state': {
          this[prop] = address[prop] ? address[prop] : Address.defaultState;
          break;
        };
        default: {
          this[prop] = address[prop] ? address[prop] : '';
          break;
        };
      }
    };
  };

  static get countries() {
    return {
      "United States": {
        "AL": "Alabama",
        "AK": "Alaska",
        "AS": "American Samoa",
        "AZ": "Arizona",
        "AR": "Arkansas",
        "CA": "California",
        "CO": "Colorado",
        "CT": "Connecticut",
        "DE": "Delaware",
        "DC": "District Of Columbia",
        "FM": "Federated States Of Micronesia",
        "FL": "Florida",
        "GA": "Georgia",
        "GU": "Guam",
        "HI": "Hawaii",
        "ID": "Idaho",
        "IL": "Illinois",
        "IN": "Indiana",
        "IA": "Iowa",
        "KS": "Kansas",
        "KY": "Kentucky",
        "LA": "Louisiana",
        "ME": "Maine",
        "MH": "Marshall Islands",
        "MD": "Maryland",
        "MA": "Massachusetts",
        "MI": "Michigan",
        "MN": "Minnesota",
        "MS": "Mississippi",
        "MO": "Missouri",
        "MT": "Montana",
        "NE": "Nebraska",
        "NV": "Nevada",
        "NH": "New Hampshire",
        "NJ": "New Jersey",
        "NM": "New Mexico",
        "NY": "New York",
        "NC": "North Carolina",
        "ND": "North Dakota",
        "MP": "Northern Mariana Islands",
        "OH": "Ohio",
        "OK": "Oklahoma",
        "OR": "Oregon",
        "PW": "Palau",
        "PA": "Pennsylvania",
        "PR": "Puerto Rico",
        "RI": "Rhode Island",
        "SC": "South Carolina",
        "SD": "South Dakota",
        "TN": "Tennessee",
        "TX": "Texas",
        "UT": "Utah",
        "VT": "Vermont",
        "VI": "Virgin Islands",
        "VA": "Virginia",
        "WA": "Washington",
        "WV": "West Virginia",
        "WI": "Wisconsin",
        "WY": "Wyoming"
      },
      "UK (N. Ireland)": {},
      "United Kingdom": {},
      "Canada": {
        "AB": "Alberta",
        "BC": "British Columnbia",
        "MB": "Manitoba",
        "NB": "New Brunswick",
        "NL": "Newfoundland and Labrador",
        "NT": "Northwest Territories",
        "NS": "Nova Scotia",
        "NU": "Nunavut",
        "ON": "Ontario",
        "PE": "Prince Edward Island",
        "QC": "Quebec",
        "SK": "Saskatchewan",
        "YT": "Yukon"
      },
      "Europe": {},
      "Japan": {},
      "China": {},
      "Hong Kong": {},
      "Singapore": {},
      "Austria": {},
      "Belarus": {},
      "Belgium": {},
      "Bulgaria": {},
      "Croatia": {},
      "Czech Republic": {},
      "Denmark": {},
      "Estonia": {},
      "Finland": {},
      "France": {},
      "Germany": {},
      "Greece": {},
      "Hungary": {},
      "Iceland": {},
      "Ireland": {},
      "Italy": {},
      "Latvia": {},
      "Lithuania": {},
      "Luxembourg": {},
      "Monaco": {},
      "Netherlands": {},
      "Norway": {},
      "Poland": {},
      "Portugal": {},
      "Romania": {},
      "Russia": {},
      "Slovakia": {},
      "Slovenia": {},
      "Spain": {},
      "Sweden": {},
      "Switzerland": {},
      "Turkey": {}
    };
  };

  static get properties() {
    return [
      'name',
      'email',
      'phone',
      'country',
      'line1',
      'line2',
      'line3',
      'city',
      'state',
      'postCode',
    ];
  };

  static get defaultCountry() {
    return Object.keys(Address.countries)[0];
  };

  static get defaultState() {
    return Object.keys(Address.countries[Address.defaultCountry])[0];
  };
};
