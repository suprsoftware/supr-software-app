import { CreditCard, CreditCardInterface } from './index';
import { creditCardType } from 'card-validator';
import { FormGroup } from '@angular/forms';

describe('CreditCard', () => {
  let creditCard: CreditCard;
  const CURRENT_YEAR = new Date().getFullYear();

  beforeEach(() => {
    creditCard = new CreditCard();
  });

  it('should create', () => {
    expect(creditCard).toBeTruthy();
    expect(creditCard).toBeInstanceOf(CreditCard);
    expect(creditCard.nameOnCard).toEqual('');
    expect(creditCard.cardNumber).toEqual('');
    expect(creditCard.cardCvv).toEqual('');
    expect(creditCard.cardExpMonth).toEqual('01');
    expect(creditCard.cardExpYear).toEqual(`${CURRENT_YEAR}`);
  });

  it('should create with interface', () => {
    creditCard = new CreditCard({
      nameOnCard: 'John Doe',
      cardNumber: '4111111111111111',
      cardCvv: '123',
      cardExpMonth: '10',
      cardExpYear: '2021'
    });

    expect(creditCard.nameOnCard).toEqual('John Doe');
    expect(creditCard.cardNumber).toEqual('4111111111111111');
    expect(creditCard.cardCvv).toEqual('123');
    expect(creditCard.cardExpMonth).toEqual('10');
    expect(creditCard.cardExpYear).toEqual('2021');
  });

  describe('getters', () => {
    let americanExpressCard1: CreditCard;
    let americanExpressCard2: CreditCard;
    let americanExpressCardCorp: CreditCard;
    let discoverCard1: CreditCard;
    let discoverCard2: CreditCard;
    let masterCard1: CreditCard;
    let masterCard2: CreditCard;
    let visaCard1: CreditCard;
    let visaCard2: CreditCard;
    let visaCard3: CreditCard;

    beforeEach(() => {
      americanExpressCard1 = new CreditCard({
        cardNumber: '378282246310005'
      } as CreditCardInterface);
      americanExpressCard2 = new CreditCard({
        cardNumber: '371449635398431'
      } as CreditCardInterface);
      americanExpressCardCorp = new CreditCard({
        cardNumber: '378734493671000'
      } as CreditCardInterface);
      discoverCard1 = new CreditCard({
        cardNumber: '6011111111111117'
      } as CreditCardInterface);
      discoverCard2 = new CreditCard({
        cardNumber: '6011000990139424'
      } as CreditCardInterface);
      masterCard1 = new CreditCard({
        cardNumber: '5555555555554444'
      } as CreditCardInterface);
      masterCard2 = new CreditCard({
        cardNumber: '5105105105105100'
      } as CreditCardInterface);
      visaCard1 = new CreditCard({
        cardNumber: '4111111111111111'
      } as CreditCardInterface);
      visaCard2 = new CreditCard({
        cardNumber: '4012888888881881'
      } as CreditCardInterface);
      visaCard3 = new CreditCard({
        cardNumber: '4222222222222'
      } as CreditCardInterface);
    });

    describe('cardNumberWithSpaces', () => {
      it('should return formatted card number with appropriate spaces', () => {
        expect(americanExpressCard1.cardNumberWithSpaces.includes(' ')).toBeTrue();
        expect(americanExpressCard2.cardNumberWithSpaces.includes(' ')).toBeTrue();
        expect(americanExpressCardCorp.cardNumberWithSpaces.includes(' ')).toBeTrue();

        expect(discoverCard1.cardNumberWithSpaces.includes(' ')).toBeTrue();
        expect(discoverCard2.cardNumberWithSpaces.includes(' ')).toBeTrue();

        expect(masterCard1.cardNumberWithSpaces.includes(' ')).toBeTrue();
        expect(masterCard2.cardNumberWithSpaces.includes(' ')).toBeTrue();

        expect(visaCard1.cardNumberWithSpaces.includes(' ')).toBeTrue();
        expect(visaCard2.cardNumberWithSpaces.includes(' ')).toBeTrue();
        expect(visaCard3.cardNumberWithSpaces.includes(' ')).toBeTrue();
      });

      it('returns falsy if card number is empty or invalid', () => {
        visaCard3.cardNumber = '';
        expect(visaCard3.cardNumberWithoutSpaces).toBeFalsy();
        visaCard3.cardNumber = 'abc';
        expect(visaCard3.cardNumberWithoutSpaces).toBeFalsy();
      });
    });

    describe('cardNumberWithoutSpaces', () => {
      it('should return card number without spaces', () => {
        expect(americanExpressCard1.cardNumberWithoutSpaces.includes(' ')).toBeFalse();
        expect(americanExpressCard2.cardNumberWithoutSpaces.includes(' ')).toBeFalse();
        expect(americanExpressCardCorp.cardNumberWithoutSpaces.includes(' ')).toBeFalse();

        expect(discoverCard1.cardNumberWithoutSpaces.includes(' ')).toBeFalse();
        expect(discoverCard2.cardNumberWithoutSpaces.includes(' ')).toBeFalse();

        expect(masterCard1.cardNumberWithoutSpaces.includes(' ')).toBeFalse();
        expect(masterCard2.cardNumberWithoutSpaces.includes(' ')).toBeFalse();

        expect(visaCard1.cardNumberWithoutSpaces.includes(' ')).toBeFalse();
        expect(visaCard2.cardNumberWithoutSpaces.includes(' ')).toBeFalse();
        expect(visaCard3.cardNumberWithoutSpaces.includes(' ')).toBeFalse();
      });

      it('returns falsy if card number is empty or invalid', () => {
        visaCard3.cardNumber = '';
        expect(visaCard3.cardNumberWithoutSpaces).toBeFalsy();
        visaCard3.cardNumber = 'abc';
        expect(visaCard3.cardNumberWithoutSpaces).toBeFalsy();
      });
    });

    describe('cardExpMonthStorthand', () => {
      beforeEach(() => {
        visaCard1.cardExpMonth = '01';
        visaCard2.cardExpMonth = '11';
        visaCard3.cardExpMonth = '';
      });
      it('should return 1 digit for months with preceeding 0', () => {
        expect(visaCard1.cardExpMonthShorthand).toEqual('1');
      });

      it('should return 2 digits for months without preceeding 0', () => {
        expect(visaCard2.cardExpMonthShorthand).toEqual('11');
      });

      it('should return "" if months is not set', () => {
        expect(visaCard3.cardExpMonthShorthand).toBeFalsy();
      });
    });

    describe('cardExpYearShorthand', () => {
      beforeEach(() => {
        visaCard1.cardExpYear = '2020';
        visaCard2.cardExpYear = '2021';
        visaCard3.cardExpYear = '';
      });
      it('should return the year up to the 10ths place', () => {
        expect(visaCard1.cardExpYearShorthand).toEqual('20');
        expect(visaCard2.cardExpYearShorthand).toEqual('21');
      });

      it('should return "" if year is not set', () => {
        expect(visaCard3.cardExpYearShorthand).toBeFalsy();
      });
    });

    describe('cardType', () => {
      const types = Object.values(creditCardType.types) as string[];
      it('returns approprate type', () => {
        expect(types.includes(americanExpressCard1.cardType)).toBeTrue();
        expect(types.includes(americanExpressCard2.cardType)).toBeTrue();
        expect(types.includes(americanExpressCardCorp.cardType)).toBeTrue();
        expect(types.includes(discoverCard1.cardType)).toBeTrue();
        expect(types.includes(discoverCard2.cardType)).toBeTrue();
        expect(types.includes(masterCard1.cardType)).toBeTrue();
        expect(types.includes(masterCard2.cardType)).toBeTrue();
        expect(types.includes(visaCard1.cardType)).toBeTrue();
        expect(types.includes(visaCard2.cardType)).toBeTrue();
        expect(types.includes(visaCard3.cardType)).toBeTrue();
      });

      it('returns "" if card number is empty or invalid', () => {
        visaCard3.cardNumber = '';
        expect(visaCard3.cardTypeNice).toBeFalsy();
        visaCard3.cardNumber = 'abc';
        expect(visaCard3.cardTypeNice).toBeFalsy();
      });
    });

    describe('cardTypeNice', () => {
      const niceTypes = [
        'Visa',
        'Mastercard',
        'American Express',
        'Diners Club',
        'Discover',
        'JCB',
        'UnionPay',
        'Maestro',
        'Mir',
        'Elo',
        'Hiper',
        'Hipercard',
      ];
      it('returns appropriate nice type', () => {
        expect(niceTypes.includes(americanExpressCard1.cardTypeNice)).toBeTrue();
        expect(niceTypes.includes(americanExpressCard2.cardTypeNice)).toBeTrue();
        expect(niceTypes.includes(americanExpressCardCorp.cardTypeNice)).toBeTrue();
        expect(niceTypes.includes(discoverCard1.cardTypeNice)).toBeTrue();
        expect(niceTypes.includes(discoverCard2.cardTypeNice)).toBeTrue();
        expect(niceTypes.includes(masterCard1.cardTypeNice)).toBeTrue();
        expect(niceTypes.includes(masterCard2.cardTypeNice)).toBeTrue();
        expect(niceTypes.includes(visaCard1.cardTypeNice)).toBeTrue();
        expect(niceTypes.includes(visaCard2.cardTypeNice)).toBeTrue();
        expect(niceTypes.includes(visaCard3.cardTypeNice)).toBeTrue();
      });
      it('returns "" if card number is empty or invalid', () => {
        visaCard3.cardNumber = '';
        expect(visaCard3.cardTypeNice).toBeFalsy();
        visaCard3.cardNumber = 'abc';
        expect(visaCard3.cardTypeNice).toBeFalsy();
      });
    });
  });

  describe('toJSON', () => {
    it('should return as CreditCardInterface', () => {
      const creditCard = new CreditCard();
      const cardJSON = creditCard.toJSON();
      expect('nameOnCard' in cardJSON).toBeTrue();
      expect('cardNumber' in cardJSON).toBeTrue();
      expect('cardCvv' in cardJSON).toBeTrue();
      expect('cardExpMonth' in cardJSON).toBeTrue();
      expect('cardExpYear' in cardJSON).toBeTrue();
      expect('cardType' in cardJSON).toBeTrue();
    });

    it('should not return a reference to any of the properties', () => {
      const creditCard = new CreditCard();
      let cardJSON = creditCard.toJSON();
      expect(cardJSON.nameOnCard).toBeFalsy();
      cardJSON.nameOnCard = 'John Doe';
      expect(creditCard.nameOnCard).toBeFalsy();
    });
  });

  describe('fromJSON', () => {
    it('should use values from interface arg', () => {
      const creditCard = new CreditCard({
        nameOnCard: 'John Doe',
        cardNumber: '4111111111111111',
        cardCvv: '123',
        cardExpMonth: '10',
        cardExpYear: '2021'
      });

      expect(creditCard.nameOnCard).toEqual('John Doe');
      expect(creditCard.cardNumber).toEqual('4111111111111111');
      expect(creditCard.cardCvv).toEqual('123');
      expect(creditCard.cardExpMonth).toEqual('10');
      expect(creditCard.cardExpYear).toEqual('2021');
    });
  });

  describe('toFormGroup', () => {
    it('should be a FormGroup', () => {
      expect(new CreditCard().toFormGroup()).toBeInstanceOf(FormGroup);
    });

    it('should use existing values when called', () => {
      const creditCard = new CreditCard();
      let formGroup = creditCard.toFormGroup();
      expect(formGroup.controls['nameOnCard'].value).toBeFalsy();
      creditCard.nameOnCard = 'John Doe';
      formGroup = creditCard.toFormGroup();
      expect(formGroup.controls['nameOnCard'].value).toEqual('John Doe');
    });
  });

  describe('static', () => {
    describe('expMonths', () => {
      it('should be a str[] of months 01-12', () => {
        expect(CreditCard.expMonths.every(m => typeof m === 'string')).toBeTrue();
        expect(CreditCard.expMonths.length).toBe(12);
        expect(CreditCard.expMonths.every((m, i) => {
          return parseInt(m, 10) === i+1
        })).toBeTrue();
      });
    });

    describe('expYears', () => {
      it('should be a str[] of this year plus 19 years from now', () => {
        expect(CreditCard.expYears.every(m => typeof m === 'string')).toBeTrue();
        expect(CreditCard.expYears.length).toBe(20);
        expect(CreditCard.expYears.every((y, i) => {
          return parseInt(y, 10) === CURRENT_YEAR+i;
        })).toBeTrue();
      });
    });

    describe('properties', () => {
      it('should return string[] of properties based on CreditCardInterface', () => {
        const properties = CreditCard.properties;
        expect(properties.includes('nameOnCard')).toBeTrue();
        expect(properties.includes('cardNumber')).toBeTrue();
        expect(properties.includes('cardCvv')).toBeTrue();
        expect(properties.includes('cardExpMonth')).toBeTrue();
        expect(properties.includes('cardExpYear')).toBeTrue();
        expect(properties.includes('cardType')).toBeTrue();
        expect(properties.length).toBe(6);
      });
    });

    describe('isValidCreditCardNumber', () => {
      it('should return true if valid', () => {
        const testCardNumbers = [
          '378282246310005',
          '371449635398431',
          '378734493671000',
          '6011111111111117',
          '6011000990139424',
          '5555555555554444',
          '5105105105105100',
          '4111111111111111',
          '4012888888881881',
          '4222222222222',
        ];
        expect(testCardNumbers.every(n => CreditCard.isValidCreditCardNumber(n))).toBeTrue();
      });

      it('should return false if invalid', () => {
        const testCardNumbers = [
          'a',
          '6',
          '',
          '6x'
        ];
        expect(testCardNumbers.every(n => !CreditCard.isValidCreditCardNumber(n))).toBeTrue();
      });
    });
  });
});