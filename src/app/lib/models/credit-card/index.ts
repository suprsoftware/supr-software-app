import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { number as creditCardNumber } from 'card-validator';

export class CreditCardInterface {
  nameOnCard: string;
  cardNumber: string;
  cardType?: string;
  cardExpMonth: string;
  cardExpYear: string;
  cardCvv: string;
};

export class CreditCard implements CreditCardInterface {
  nameOnCard: string = '';
  cardNumber: string = '';
  cardExpMonth: string = CreditCard.expMonths[0];
  cardExpYear: string = CreditCard.expYears[0];
  cardCvv: string = '';

  constructor(card?: CreditCardInterface) {
    if (!!card) this.fromJSON(card);
  };

  get cardNumberWithSpaces() {
    const card = this.getCardObj();
    if (!card) return '';
    const {gaps} = card;
    let digits = this.cardNumberWithoutSpaces.split('');
    for (let i = 0; i < gaps.length; i++) {
      digits.splice(gaps[i] + i, 0, ' ');
    };
    return digits.join('');
  };

  get cardNumberWithoutSpaces() {
    if (CreditCard.isValidCreditCardNumber(this.cardNumber)) {
      return this.cardNumber
        .split(' ')
        .join('')
        .trim();
    } else {
      return '';
    };
  };

  get cardExpMonthShorthand () {
    let month = this.cardExpMonth;
    if (month[0] === '0') return month.slice(-1);
    return month;
  };

  get cardExpYearShorthand () {
    return this.cardExpYear.slice(-2);
  };

  get cardType() {
    const card = this.getCardObj();
    if (!card) return '';
    return card.type;
  };

  get cardTypeNice() {
    const card = this.getCardObj();
    if (!card) return '';
    return card.niceType;
  };

  private getCardObj() {
    const {card} = creditCardNumber(this.cardNumberWithoutSpaces);
    return card;
  };

  toFormGroup() {
    const controls = this.toFormControls();
    return new FormGroup(controls);
  };

  private toFormControls(): {[key: string]: AbstractControl} {
    const controls: {[key: string]: AbstractControl} = {};
    for (const prop of CreditCard.properties) {
      if (prop === 'cardType') continue;
      controls[prop] = new FormControl(this[prop], [Validators.required]);
    };
    return controls;
  };
  
  toJSON(): CreditCardInterface {
    const obj = {};
    for (const prop of CreditCard.properties) {
      obj[prop] = this[prop];
    };
    return obj as CreditCardInterface;
  };

  fromJSON(card: CreditCardInterface) {
    for (const prop of CreditCard.properties) {
      if (prop === 'cardType') continue;
      this[prop] = card[prop];
    };
  };

  static get expMonths() {
    return [
      '01',
      '02',
      '03',
      '04',
      '05',
      '06',
      '07',
      '08',
      '09',
      '10',
      '11',
      '12',
    ];
  };

  static get expYears() {
    const currDate = new Date();
    const currYear = currDate.getFullYear();
    return [...new Array(20)]
      .map((curr, idx, arr) => currYear + idx)
      .map(y => String(y));
  };

  static get properties() {
    return [
      'nameOnCard',
      'cardNumber',
      'cardCvv',
      'cardExpMonth',
      'cardExpYear',
      'cardType',
    ];
  };
  
  static isValidCreditCardNumber(cardNumber: string) {
    const {card} = creditCardNumber(cardNumber);
    return !!card;
  };
  
};