import { GoogleSession } from "../../google-session";
import { SUPPORTED_SITES } from "../../supported-sites";

export interface CreateRecaptchaHarvesterEvent {
  mode: SUPPORTED_SITES;
  gSession: GoogleSession;
};