import { GoogleSession, GoogleSessionInterface } from './index';
import {session, remote} from 'electron';
import { TestBed } from '@angular/core/testing';
import { ProxiesService } from 'app/core/services/proxies/proxies.service';
import { Proxy } from 'app/lib/models/proxy';
import { ProxyList } from '../proxy-list.model';

describe('GoogleSession', () => {
  let testData: GoogleSessionInterface;
  let proxiesService: jasmine.SpyObj<ProxiesService>;

  beforeEach(() => {
    testData = {
      id: 'default',
      proxyListName: ''
    };
    spyOn(remote.session, 'fromPartition').and.returnValue({} as session);
    spyOnProperty(remote.session, 'defaultSession').and.returnValue({} as session);

    const proxiesSpy = jasmine.createSpyObj('ProxiesService', [
      'hasProxyList',
      'getProxyByProxyString',
      'getProxyList'
    ]);

    TestBed.configureTestingModule({
      providers: [
        {provide: ProxiesService, useValue: proxiesSpy}
      ]
    });
    proxiesService = TestBed.inject(ProxiesService) as jasmine.SpyObj<ProxiesService>;
  });

  describe('constructor', () => {
    it('should be created', () => {
      expect(new GoogleSession(proxiesService)).toBeTruthy();
      expect(GoogleSession.count).toBeGreaterThan(0);
    });
  });

  describe('get', () => {
    describe('isPersistent', () => {
      it('detects persistence if name starts with persist:', () => {
        testData.id = 'persist:randomName';
        let g = new GoogleSession(proxiesService, testData);
        expect(g.isPersistent).toBeTrue();
        expect(g.electronSession).toBeTruthy();

        testData.id = 'randomName';
        g = new GoogleSession(proxiesService, testData);
        expect(g.isPersistent).toBeFalse();
        expect(g.electronSession).toBeTruthy();
      });
      it('detects persistence if default session', () => {
        testData.id = 'default';
        let g = new GoogleSession(proxiesService, testData);
        expect(g.isPersistent).toBeTrue();
        expect(g.electronSession).toBeTruthy();
      });
    });
  });

  describe('static', () => {
    it('should increment count after every construction', () => {
      let count = GoogleSession.count;
      new GoogleSession(proxiesService);
      expect(GoogleSession.count > count).toBeTrue();
      count = GoogleSession.count;
      new GoogleSession(proxiesService);
      expect(GoogleSession.count > count).toBeTrue();
    });
  });

  describe('methods', () => {

  });

  describe('default session', () => {
    let d: GoogleSession;
    let localhostProxyListStub: ProxyList;

    beforeEach(() => {
      localhostProxyListStub = ['Localhost', [Proxy.LOCALHOST]];
      proxiesService.getProxyList.and.returnValue(localhostProxyListStub);
      d = new GoogleSession(proxiesService, {
        id: 'default',
        proxyListName: ''
      });
    });

    it('should always set localhost proxy list', () => {
      expect(d.isDefault).toBeTrue();
      expect(d.proxyList).toEqual(localhostProxyListStub);

      d = new GoogleSession(proxiesService, {
        id: 'default',
        proxyListName: 'host:port'
      });
      expect(d.isDefault).toBeTrue();
      expect(d.proxyList).toEqual(localhostProxyListStub);
    });


  });

  describe('non default session', () => {
    let d: GoogleSession;
    let localhostProxyListStub: ProxyList;

    beforeEach(() => {
      localhostProxyListStub = ['Localhost', [Proxy.LOCALHOST]];
    });

    it('should set localhost proxy list if invalid proxy string', () => {
      proxiesService.getProxyList.and.returnValue(localhostProxyListStub);
      d = new GoogleSession(proxiesService, {
        id: 'test',
        proxyListName: 'host:port'
      });
      expect(d.isDefault).toBeFalse();
      expect(d.proxyList).toEqual(localhostProxyListStub);
    });

    it('should set proxy list if valid proxy string and in proxies service', () => {
      const proxyStub = new Proxy('host:80');
      const spy = proxiesService.getProxyByProxyString.and.returnValue(proxyStub);
      d = new GoogleSession(proxiesService, {
        id: 'test',
        proxyListName: 'host:80'
      });
      expect(d.isDefault).toBeFalse();
      expect(spy).toHaveBeenCalledOnceWith('host:80');
      expect(d.proxyList).toEqual(['host:80', [proxyStub]]);
    });

    it('should set proxy list if valid proxy string and not in proxies service', () => {
      const proxyStub = new Proxy('host:80');
      d = new GoogleSession(proxiesService, {
        id: 'test',
        proxyListName: 'host:80'
      });
      expect(d.isDefault).toBeFalse();
      expect(d.proxyList).toEqual(['host:80', [proxyStub]]);
    });

    it('should set proxy list if proxy list is in proxies service', () => {
      const proxyStub = new Proxy('host:80');
      proxiesService.hasProxyList.and.returnValue(true);
      proxiesService.getProxyList.and.returnValue(['testList', [proxyStub]]);
      d = new GoogleSession(proxiesService, {
        id: 'test',
        proxyListName: 'testList'
      });
      expect(d.isDefault).toBeFalse();
      expect(d.proxyList).toEqual(['testList', [proxyStub]]);
    });

    it('should set localhost proxy list if proxy list not in proxies service', () => {
      proxiesService.hasProxyList.and.returnValue(false);
      proxiesService.getProxyList.and.returnValue(localhostProxyListStub);
      d = new GoogleSession(proxiesService, {
        id: 'test',
        proxyListName: 'notInList'
      });
      expect(d.isDefault).toBeFalse();
      expect(d.proxyList).toEqual(localhostProxyListStub);
    });
  });
});