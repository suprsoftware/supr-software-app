import { ProxiesService } from 'app/core/services/proxies/proxies.service';
import { remote, session } from 'electron';
import { ProxyList } from '../proxy-list.model';
import { Proxy } from '../proxy';
import { SUPPORTED_SITES } from '../supported-sites';

export interface GoogleSessionInterface {
  id: string;
  proxyListName: string;
};

export class GoogleSession {
  static count: number = 0;

  id: string;
  electronSession: session;
  proxyList: ProxyList;

  constructor(
    private proxiesService: ProxiesService,
    config?: GoogleSessionInterface
  ) {
    GoogleSession.count++;
    this.import(config);
  };

  static get supportedSites() {
    return [
      SUPPORTED_SITES.SUPREME_INSTORE,
      SUPPORTED_SITES.SUPREME_WEBSTORE,
    ];
  };

  get isPersistent(): boolean {
    return this.id.startsWith('persist:') || this.id.toLowerCase() == 'default';
  };

  get isExportable() {
    return !this.isDefault;
  };

  get isDefault() {
    if (this.id.toLowerCase() == 'default') return true;
    return false;
  };

  import(config: GoogleSessionInterface) {
    this.id = config?.id;
    this.electronSession = this.getSessionFromId(this.id);
    this.proxyList = this.getProxyList(config?.proxyListName);
  };

  getSessionFromId(id: string) {
    if (!id) return undefined;
    if (id.toLowerCase() == 'default') return remote.session.defaultSession;
    return remote.session.fromPartition(id, {cache: true}); // experimental cache: true
  };
  
  setProxyList(proxyStringOrProxyListName: string) {
    this.proxyList = this.getProxyList(proxyStringOrProxyListName);
  };

  getProxyList(proxyStringOrProxyListName: string): ProxyList {
    const localhostProxyList = this.proxiesService.getProxyList('Localhost');
    if (!proxyStringOrProxyListName || this.isDefault) return localhostProxyList;
    if (this.proxiesService.hasProxyList(proxyStringOrProxyListName)) {
      return this.proxiesService.getProxyList(proxyStringOrProxyListName);
    };
    if (Proxy.isValidProxyString(proxyStringOrProxyListName)) {
      return [
        proxyStringOrProxyListName,
        [this.proxiesService.getProxyByProxyString(proxyStringOrProxyListName) || new Proxy(proxyStringOrProxyListName)]
      ];
    };
    return localhostProxyList;
  };

  export(): GoogleSessionInterface {
    return {
      id: this.id,
      proxyListName: this.proxyList[0]
    };
  };
  
};