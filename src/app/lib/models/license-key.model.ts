import firebase from "firebase";
import 'firebase/firestore';

export interface LicenseKey {
  activated_at: firebase.firestore.Timestamp;
  created_at: firebase.firestore.Timestamp;
  expires_at: firebase.firestore.Timestamp;
  hardware_id: string;
  hostname: string;
  lastlogin_at: firebase.firestore.Timestamp;
  renewal_period: RENEWAL_PERIOD;
  role: ROLE;
  settings: LicenseKeySettings;
  user_id: string;
  uuid: string;
};

export interface LicenseKeySettings {
  discord_webhook: string;
};

export enum RENEWAL_PERIOD {
  WEEKLY = 7,
  MONTHLY = 30,
  YEARLY = 365,
  SS = 147,
  FAW = 147,
  LIFETIME = 36500
};

export enum ROLE {
  ADMIN = 'ADMIN',
  STAFF = 'STAFF',
  USER_RENEWAL = 'USER_RENEWAL',
  USER_LIFETIME = 'USER_LIFETIME'
};