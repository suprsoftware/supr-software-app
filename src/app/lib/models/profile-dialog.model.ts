import { FormGroup } from '@angular/forms';

import { ProfileInterface } from './profile';

export interface ProfileDialogData {
  profileForm: FormGroup;
  immutableProfileName: boolean;
};

export interface ProfileDialogReturn extends ProfileInterface {
};