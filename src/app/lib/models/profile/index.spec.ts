import { Profile, ProfileInterface } from './index';
import { Address } from '../address';
import { CreditCard } from '../credit-card';
import { FormGroup } from '@angular/forms';

describe('Profile', () => {
  let profile: Profile;

  beforeEach(() => {
    profile = new Profile();
  });

  it('should create', () => {
    expect(profile).toBeTruthy();
    expect(profile).toBeInstanceOf(Profile);
    expect(profile.billingAddress).toBeInstanceOf(Address);
    expect(profile.shippingAddress).toBeInstanceOf(Address);
    expect(profile.paymentDetails).toBeInstanceOf(CreditCard);
  });

  it('should create with profile interface', () => {
    profile = new Profile({
      name: 'Name',
      billingAddress: {
        name: 'Billing Name',
      },
      shippingAddress: {
        name: 'Shipping Name',
      },
      paymentDetails: {
        nameOnCard: 'Billing Name',
      },
    } as ProfileInterface);

    expect(profile.name).toEqual('Name');
    expect(profile.billingAddress.name).toEqual('Billing Name');
    expect(profile.shippingAddress.name).toEqual('Shipping Name');
    expect(profile.paymentDetails.nameOnCard).toEqual('Billing Name');
  });

  describe('toFormGroup', () => {
    it('creates form group', () => {
      expect(profile.toFormGroup()).toBeInstanceOf(FormGroup);
      profile.sameBillingAndShippingAddress = false;
      expect(profile.toFormGroup()).toBeInstanceOf(FormGroup);
    });
  });

  describe('toFormGroupSameBillingAndShipping', () => {
    let formGroup: FormGroup;
    beforeEach(() => {
      profile.billingAddress.name = 'Test Name';
      formGroup = profile.toFormGroupSameBillingAndShipping();
    });
    it('should have same form control for billing and shipping addresses', () => {
      expect(formGroup.get('billingAddress.name').value).toEqual('Test Name');
      expect(formGroup.get('shippingAddress.name').value).toEqual('Test Name');

      formGroup.get('billingAddress.name').setValue('New Name');
      expect(formGroup.get('billingAddress.name').value).toEqual('New Name');
      expect(formGroup.get('shippingAddress.name').value).toEqual('New Name');

      formGroup.get('shippingAddress.name').setValue('New New Name');
      expect(formGroup.get('billingAddress.name').value).toEqual('New New Name');
      expect(formGroup.get('shippingAddress.name').value).toEqual('New New Name');
    });
  });

  describe('toFormGroupDiffBillingAndShipping', () => {
    let formGroup: FormGroup;
    beforeEach(() => {
      profile.sameBillingAndShippingAddress = false;
      profile.billingAddress.name = 'Test Name';
      profile.shippingAddress.name = 'Test Name';
      formGroup = profile.toFormGroupDiffBillingAndShipping();
    });

    it('should not have same form control for billing and shipping address', () => {
      expect(formGroup.get('billingAddress.name').value).toEqual('Test Name');
      expect(formGroup.get('shippingAddress.name').value).toEqual('Test Name');

      formGroup.get('billingAddress.name').setValue('Billing Name');
      expect(formGroup.get('billingAddress.name').value).toEqual('Billing Name');
      expect(formGroup.get('shippingAddress.name').value).toEqual('Test Name');

      formGroup.get('shippingAddress.name').setValue('Shipping Name');
      expect(formGroup.get('billingAddress.name').value).toEqual('Billing Name');
      expect(formGroup.get('shippingAddress.name').value).toEqual('Shipping Name');
    });

  });

  describe('toJSON', () => {
    beforeEach(() => {
      profile.name = 'Test Profile';
      profile.billingAddress.name = 'Test Billing Name';
      profile.shippingAddress.name = 'Test Shipping Name';
      profile.paymentDetails.nameOnCard = 'Test Billing Name';
    });

    it('should return object with same profile name', () => {
      const profileJSON = profile.toJSON();
      expect(profileJSON.name).toEqual('Test Profile');
    });

    it('should return object with same billingAddress name', () => {
      const profileJSON = profile.toJSON();
      expect(profileJSON.billingAddress.name).toEqual('Test Billing Name');
    });

    it('should return object with same shippingAddress name', () => {
      const profileJSON = profile.toJSON();
      expect(profileJSON.shippingAddress.name).toEqual('Test Shipping Name');
    });

    it('should return object with same paymentDetails name', () => {
      const profileJSON = profile.toJSON();
      expect(profileJSON.paymentDetails.nameOnCard).toEqual('Test Billing Name');
    });

    it('should not return a reference', () => {
      const profileJSON = profile.toJSON();
      profile.name = '';
      profile.billingAddress.name = '';
      profile.shippingAddress.name = '';
      profile.paymentDetails.nameOnCard = '';
      expect(profileJSON.name).toEqual('Test Profile');
      expect(profileJSON.billingAddress.name).toEqual('Test Billing Name');
      expect(profileJSON.shippingAddress.name).toEqual('Test Shipping Name');
      expect(profileJSON.paymentDetails.nameOnCard).toEqual('Test Billing Name');
    });

  });

});