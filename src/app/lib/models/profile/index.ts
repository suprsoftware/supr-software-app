import { FormGroup, FormControl, Validators } from '@angular/forms';

import { Address, AddressInterface } from '../address';
import { CreditCard, CreditCardInterface } from '../credit-card';

export interface ProfileInterface {
  name: string;
  billingAddress: AddressInterface;
  shippingAddress: AddressInterface;
  paymentDetails: CreditCardInterface;
  sameBillingAndShippingAddress: boolean;
  onlyCheckoutOnce: boolean;
};

export class Profile implements ProfileInterface {
  name: string;
  billingAddress: Address;
  shippingAddress: Address;
  paymentDetails: CreditCard;
  sameBillingAndShippingAddress: boolean;
  onlyCheckoutOnce: boolean;

  constructor(profile?: ProfileInterface) {
    this.name = profile?.name;
    this.billingAddress = new Address(profile?.billingAddress);
    this.shippingAddress = new Address(profile?.shippingAddress);
    this.paymentDetails = new CreditCard(profile?.paymentDetails);
    this.sameBillingAndShippingAddress = profile?.sameBillingAndShippingAddress || true;
    this.onlyCheckoutOnce =profile?.onlyCheckoutOnce || true;
  };

  toFormGroup() {
    return this.sameBillingAndShippingAddress ?
      this.toFormGroupSameBillingAndShipping() :
      this.toFormGroupDiffBillingAndShipping();
  };

  toFormGroupSameBillingAndShipping(): FormGroup {
    const formGroup = this.toFormGroupDiffBillingAndShipping();
    formGroup.setControl('shippingAddress', formGroup.get('billingAddress'));
    return formGroup;
  };

  toFormGroupDiffBillingAndShipping(): FormGroup {
    return new FormGroup({
      'name': new FormControl(this.name, [Validators.required]),
      'billingAddress': this.billingAddress.toFormGroup(),
      'shippingAddress': this.shippingAddress.toFormGroup(),
      'paymentDetails': this.paymentDetails.toFormGroup(),
      'sameBillingAndShippingAddress': new FormControl(this.sameBillingAndShippingAddress),
      'onlyCheckoutOnce': new FormControl(this.onlyCheckoutOnce)
    });
  };

  toJSON(): ProfileInterface {
    return {
      name: this.name,
      billingAddress: {...this.billingAddress.toJSON()},
      shippingAddress: {...this.shippingAddress.toJSON()},
      paymentDetails: {...this.paymentDetails.toJSON()},
      sameBillingAndShippingAddress: this.sameBillingAndShippingAddress,
      onlyCheckoutOnce: this.onlyCheckoutOnce,
    };
  };
};