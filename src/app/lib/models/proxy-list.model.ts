import { Proxy } from './proxy';

export type ProxyPool = Proxy[];

export interface ProxyList {
  [0]: string,
  [1]: ProxyPool
};