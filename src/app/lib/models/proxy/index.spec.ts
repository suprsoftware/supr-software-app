import { Proxy } from './index';

describe('Proxy', () => {

  it('should create', () => {
    expect(new Proxy('good:80')).toBeInstanceOf(Proxy);
  });

  describe('static', () => {

    describe('isValidProxy', () => {
      it('returns false on invalid proxy', () => {
        expect(Proxy.isValidProxy(new Proxy(''))).toBeFalse();
        expect(Proxy.isValidProxy(new Proxy('localhost:80'))).toBeFalse();
        expect(Proxy.isValidProxy(new Proxy('Localhost:80'))).toBeFalse();
        expect(Proxy.isValidProxy(new Proxy('127.0.0.1:80'))).toBeFalse();
        expect(Proxy.isValidProxy(new Proxy('bad:bad'))).toBeFalse();
        expect(Proxy.isValidProxy(new Proxy('bad:80:'))).toBeFalse();
        expect(Proxy.isValidProxy(new Proxy('bad:80:user'))).toBeFalse();
        expect(Proxy.isValidProxy(new Proxy('bad:80:user:'))).toBeFalse();
        expect(Proxy.isValidProxy(new Proxy('bad:80:user:pass:'))).toBeFalse();
        expect(Proxy.isValidProxy(new Proxy('bad:80:user:pass:word'))).toBeFalse();
        expect(Proxy.isValidProxy(new Proxy('good:80:user:pass\ngoodaswell:80'))).toBeFalse();
        expect(Proxy.isValidProxy(new Proxy('good:80:user:pass\ngoodaswell:80:user:pass'))).toBeFalse();
      });

      it('returns true on valid proxy', () => {
        expect(Proxy.isValidProxy(new Proxy('127.0.0.2:80'))).toBeTrue();
        expect(Proxy.isValidProxy(new Proxy('hostname:80'))).toBeTrue();
        expect(Proxy.isValidProxy(new Proxy('hostname:80:user:pass'))).toBeTrue();
      });
    });

    describe('isValidProxyString', () => {
      it('returns false on invalid proxy strings', () => {
        expect(Proxy.isValidProxyString('')).toBeFalse();
        expect(Proxy.isValidProxyString('localhost:80')).toBeFalse();
        expect(Proxy.isValidProxyString('Localhost:80')).toBeFalse();
        expect(Proxy.isValidProxyString('127.0.0.1:80')).toBeFalse();
        expect(Proxy.isValidProxyString('bad:bad')).toBeFalse();
        expect(Proxy.isValidProxyString('bad:80:')).toBeFalse();
        expect(Proxy.isValidProxyString('bad:80:user')).toBeFalse();
        expect(Proxy.isValidProxyString('bad:80:user:')).toBeFalse();
        expect(Proxy.isValidProxyString('bad:80:user:pass:')).toBeFalse();
        expect(Proxy.isValidProxyString('bad:80:user:pass:word')).toBeFalse();
      });

      it('returns true on valid proxy strings', () => {
        expect(Proxy.isValidProxyString('127.0.0.2:80')).toBeTrue();
        expect(Proxy.isValidProxyString('hostname:80')).toBeTrue();
        expect(Proxy.isValidProxyString('hostname:80:user:pass')).toBeTrue();
        expect(Proxy.isValidProxyString('good:80:user:pass\ngoodaswell:80')).toBeTrue();
        expect(Proxy.isValidProxyString('good:80:user:pass\ngoodaswell:80:user:pass')).toBeTrue();
      });
    });

    describe('getURL', () => {
      it('returns "" on invalid proxy string', () => {
        expect(Proxy.getURL('')).toBeFalsy();
        expect(Proxy.getURL('bad:bad')).toBeFalsy();
        expect(Proxy.getURL('bad:80:')).toBeFalsy();
        expect(Proxy.getURL('bad:80:user')).toBeFalsy();
        expect(Proxy.getURL('bad:80:user:')).toBeFalsy();
        expect(Proxy.getURL('bad:80:user:pass:')).toBeFalsy();
        expect(Proxy.getURL('bad:80:user:pass:word')).toBeFalsy();
        expect(Proxy.getURL('good:80:user:pass\ngoodaswell:80')).toBeFalsy();
        expect(Proxy.getURL('good:80:user:pass\ngoodaswell:80:user:pass')).toBeFalsy();
      });
      it('returns url on valid proxy string', () => {
        expect(Proxy.getURL('good:80')).toBeTruthy();
        expect(Proxy.getURL('good:80:user:pass')).toBeTruthy();
        expect(Proxy.getURL('good:80') instanceof URL).toBeTrue();
        expect(Proxy.getURL('good:80:user:pass') instanceof URL).toBeTrue();
      });
    });

    describe('getURLString', () => {
      it('returns "" on invalid proxy string', () => {
        expect(Proxy.getURLString('')).toBeFalsy();
        expect(Proxy.getURLString('bad:bad')).toBeFalsy();
        expect(Proxy.getURLString('bad:80:')).toBeFalsy();
        expect(Proxy.getURLString('bad:80:user')).toBeFalsy();
        expect(Proxy.getURLString('bad:80:user:')).toBeFalsy();
        expect(Proxy.getURLString('bad:80:user:pass:')).toBeFalsy();
        expect(Proxy.getURLString('bad:80:user:pass:word')).toBeFalsy();
        expect(Proxy.getURLString('good:80:user:pass\ngoodaswell:80')).toBeFalsy();
        expect(Proxy.getURLString('good:80:user:pass\ngoodaswell:80:user:pass')).toBeFalsy();
      });
      it('returns url on valid proxy string', () => {
        expect(Proxy.getURLString('good:80')).toBeTruthy();
        expect(Proxy.getURLString('good:80:user:pass')).toBeTruthy();
      });
    });

    describe('stringToProxyList', () => {
      it('returns only valid proxies', () => {
        const proxyListAsString = 'good:80\ngood:80:user:pass\nbad:bad\nbad:80:user\nbad:80:user:pass:';
        const proxies = Proxy.stringToProxyList(proxyListAsString);
        expect(proxies.every(p => p instanceof Proxy)).toBeTrue();
        expect(proxies.length).toBe(2);
      });
    });

    describe('stringListToProxyList', () => {
      it('returns only valid proxies', () => {
        const proxyStringList = [
          'good:80',
          'good:80:user:pass',
          'bad:bad',
          'bad:80:user',
          'bad:80:user:pass:'
        ];
        const proxies = Proxy.stringListToProxyList(proxyStringList);
        expect(proxies.every(p => p instanceof Proxy)).toBeTrue();
        expect(proxies.length).toBe(2);
      });
    });

    describe('proxyListToStringList', () => {
      it('returns only valid proxies as strings', () => {
        const proxies = [
          new Proxy('good:80'),
          new Proxy('good:80:user:pass'),
          new Proxy('bad:bad'),
          new Proxy('bad:80:user'),
          new Proxy('bad:80:user:pass:')
        ];
        const proxyListAsStringList = Proxy.proxyListToStringList(proxies);
        expect(proxyListAsStringList.length).toBe(2);
        expect(proxyListAsStringList.every(p => typeof p === 'string'));
      });
    });

    describe('proxyListToString', () => {
      it('returns only valid proxies as string', () => {
        const proxies = [
          new Proxy('good:80'),
          new Proxy('good:80:user:pass'),
          new Proxy('bad:bad'),
          new Proxy('bad:80:user'),
          new Proxy('bad:80:user:pass:')
        ];
        const proxyListAsString = Proxy.proxyListToString(proxies);
        expect(proxyListAsString).toBeTruthy();
        expect(typeof proxyListAsString === 'string').toBeTrue();
      });
    });
  });
});