export class Proxy {
  proxyString: string;

  constructor(proxyString: string) {
    this.proxyString = proxyString;
  };

  get proxyURL(): URL {
    return Proxy.getURL(this.proxyString);
  };

  get proxyURLString(): string {
    return Proxy.getURLString(this.proxyString);
  };

  static getURL(proxyString: string): URL {
    if (Proxy.isValidProxyString(proxyString)) {
      try {
        return new URL(Proxy.getURLString(proxyString));
      } catch(err) {
        return undefined;
      };
    };
    return undefined;
  };

  static getURLString(proxyString: string) {
    if(!Proxy.isValidProxyString(proxyString)) return '';
    const proxyArgs = proxyString.split(':');
    if (proxyArgs.length == 2) {
      return `http://${proxyArgs[0]}:${proxyArgs[1]}`;
    } else if (proxyArgs.length == 4) {
      return `http://${proxyArgs[2]}:${proxyArgs[3]}@${proxyArgs[0]}:${proxyArgs[1]}`;
    } else {
      return '';
    };
  };

  static isValidProxyString(proxyString: string) {
    const proxyRegex = /^([^:\s]+:[^:\s\D]+)$|^([^:\s]+:[^:\s\D]+:[^:\s]+:[^:\s]+)$/gmi;
    if (proxyString === '') return false;
    if (proxyString.toLowerCase().includes('localhost')) return false;
    if (proxyString.toLowerCase().includes('127.0.0.1')) return false;
    return proxyRegex.test(proxyString);
  };

  static stringToProxyList(proxyListAsString: string): Proxy[] {
    return proxyListAsString
      .trim()
      .split('\n')
      .filter(line => Proxy.isValidProxyString(line))
      .map(line => new Proxy(line));
  };

  static stringListToProxyList(proxyStringList: string[]): Proxy[] {
    return proxyStringList
      .filter(line => Proxy.isValidProxyString(line))
      .map(line => new Proxy(line));
  };

  static proxyListToStringList(proxies: Proxy[]): string[] {
    return proxies
      .filter(proxy => Proxy.isValidProxy(proxy))
      .map(proxy => proxy.proxyString);
  };

  static proxyListToString(proxies: Proxy[]): string {
    return proxies
      .filter(proxy => Proxy.isValidProxy(proxy))
      .map(p => p.proxyString)
      .join('\n');
  };

  static isValidProxy(proxy: Proxy) {
    return !!proxy.proxyURL;
  };

  static get LOCALHOST() {
    return LOCALHOST;
  };
};

const LOCALHOST = new Proxy('Localhost');