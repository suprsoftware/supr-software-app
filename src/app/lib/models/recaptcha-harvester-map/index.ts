import { RecaptchaHarvester } from "../recaptcha-harvester";

export class RecaptchaHarvesterMap {
  private map: Map<string, RecaptchaHarvester>;
  constructor() {
    this.map = new Map();
  };

  get size() {
    return this.map.size;
  };

  get(id: string) {
    return this.map.get(id);
  };

  set(harvester: RecaptchaHarvester) {
    return this.map.set(harvester.id, harvester);
  };

  delete(id: string) {
    return this.map.delete(id);
  };

};