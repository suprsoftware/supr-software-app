import { RecaptchaHarvester } from "../recaptcha-harvester";
import { RecaptchaHarvesterQueue } from "../recaptcha-harvester-queue";
import { SUPPORTED_SITES } from "../supported-sites";

export class RecaptchaHarvesterQueueMap {
  private map: Map<SUPPORTED_SITES, RecaptchaHarvesterQueue<RecaptchaHarvester>>;

  constructor() {
    this.map = new Map();
    this.init();
  };

  get size() {
    return this.map.size;
  };

  init() {
    for (let key of RecaptchaHarvester.SUPPORTED_SITES) {
      this.set(key, new RecaptchaHarvesterQueue());
    };
  };

  get(key: SUPPORTED_SITES) {
    return this.map.get(key);
  };

  set(key: SUPPORTED_SITES, queue: RecaptchaHarvesterQueue<RecaptchaHarvester>) {
    return this.map.set(key, queue);
  };

};