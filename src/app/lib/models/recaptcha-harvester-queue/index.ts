import { RECAPTCHA_HARVESTER_STATUS } from '../recaptcha-harvester';

interface RecaptchaHarvester {
  id: string;
  status: RECAPTCHA_HARVESTER_STATUS;
};

export class RecaptchaHarvesterQueue<T extends RecaptchaHarvester> {
  private harvesters: T[];

  constructor() {
    this.harvesters = [];
  };

  get isEmpty(): boolean {
    return this.size == 0;
  };

  get size(): number {
    return this.harvesters.length;
  };

  enqueue(harvester: T): void {
    const i = this.harvesters.push(harvester) - 1;
    this.maxHeapifyUp(i);
  };

  maxHeapifyUp(i: number) {
    while (i > 0 && this.harvesters[this.getParentIndex(i)].status < this.harvesters[i].status) {
      const parentIndex = this.getParentIndex(i);
      this.swap(i, parentIndex);
      i = parentIndex;
    };
  };

  getParentIndex(i: number) {
    return Math.floor((i-1) / 2);
  };

  swap(i: number, j: number) {
    const temp = this.harvesters[i];
    this.harvesters[i] = this.harvesters[j];
    this.harvesters[j] = temp;
  };

  dequeue(): T {
    if (this.isEmpty) return;
    const lastIndex = this.size - 1;
    this.swap(0, lastIndex);
    const harvester = this.harvesters.pop();
    this.maxHeapifyDown(0);
    return harvester;
  };

  maxHeapifyDown(i: number) {
    let l = this.getLeftIndex(i);
    let r = this.getRightIndex(i);
    let largest;
    if (l < this.size && this.harvesters[l].status > this.harvesters[i].status) {
      largest = l;
    } else {
      largest = i;
    };
    if (r < this.size && this.harvesters[r].status > this.harvesters[largest].status) {
      largest = r;
    };
    if (largest != i) {
      this.swap(i, largest);
      this.maxHeapifyDown(largest);
    };
  };

  getLeftIndex(i: number) {
    return (2*i)+1;
  };

  getRightIndex(i: number) {
    return (2*i)+2;
  };

  removeById(id: string) {
    this.harvesters = this.harvesters.filter(h => h.id != id);
    this.buildMaxHeap();
  };

  buildMaxHeap() {
    const lastRootIndex = this.getParentIndex(this.size-1);
    for (let i = lastRootIndex; i >= 0; i--) {
      this.maxHeapifyDown(i);
    };
  };
};
