import { remote } from 'electron';
import { Proxy } from '../proxy/index';
import { SUPPORTED_SITES, RECAPTCHA_URL } from '../supported-sites';
import { Logger } from '../logger.model';
import wait from 'app/lib/wait';
import { EventEmitter } from 'events';
import { generateId } from 'app/lib/uuid';

export interface RecaptchaHarvesterConstructorOptions {
  mode: SUPPORTED_SITES;
  proxy: Proxy;
  session: Electron.session;
  data: Buffer;
};

export enum RECAPTCHA_HARVESTER_STATUS {
  UNAVAILABLE,
  AVAILABLE,
  OVERRIDE
};

export class RecaptchaHarvester extends EventEmitter {
  private logger: Logger;
  private session: Electron.session;
  private browser: Electron.BrowserWindow;
  private data: Buffer;

  readonly id: string;
  mode: SUPPORTED_SITES;
  proxy: Proxy;
  status: RECAPTCHA_HARVESTER_STATUS;

  constructor(options: RecaptchaHarvesterConstructorOptions) {
    super();
    this.id = generateId(3);
    this.mode = options.mode;
    this.proxy = options.proxy;
    this.session = options.session;
    this.data = options.data;
    this.unavailable();
    this.initLogger();
    this.initBrowser();
  };

  get webContents() {
    if (!this.browser) return;
    return this.browser.webContents;
  };

  initLogger() {
    const mainLogger = remote.getGlobal('logger') as Logger;
    this.logger = mainLogger.child({process: `harvester-${this.id}`});
    this.logger.debug('logger initialized');
  };

  async initBrowser() {
    let browser = new remote.BrowserWindow({
      width: 400,
      height: 600,
      minWidth: 400,
      minHeight: 600,
      autoHideMenuBar: true,
      show: false,
      parent: remote.getCurrentWindow(),
      webPreferences: {
        nodeIntegration: true,
        enableRemoteModule: true,
        allowRunningInsecureContent: true,
        webSecurity: false,
        session: this.session,
        javascript: true,
        worldSafeExecuteJavaScript: true,
      }
      
    });
    browser.removeMenu();
    browser.on('close', () => {
      this.emit('close', this.id);
    });
    browser.on('closed', () => {
      this.emit('closed', this.id);
      browser = null;
    });
    browser.webContents.on('login', (event, {url}, {isProxy, scheme, host, port, realm}, cb) => {
      const proxy = this.proxy.proxyURL;
      if (isProxy) {
        cb(proxy.username, proxy.password);
      } else {
        cb();
      };
    });
    this.setBufferIntercept();
    this.browser = browser;
    await this.loadBrowser();
  };

  setBufferIntercept() {
    if (this.session.protocol.isProtocolIntercepted('http')) this.session.protocol.uninterceptProtocol('http');
    this.session.protocol.interceptBufferProtocol('http', (req, cb) => {
      if (req.url.includes(RECAPTCHA_URL[this.mode])) {
        cb({
          data: this.data,
        });
      } else {
        cb({});
      };
    });
  };

  async loadBrowser() {
    try {
      await this.loadGoogle();
      await wait(Math.floor(Math.random() * 150) + 400);
      await this.loadGoogleSearch();
      await wait(Math.floor(Math.random() * 150) + 400);
      await this.browser.loadURL(RECAPTCHA_URL[this.mode]);
      this.emit('ready');
      this.available();
    } catch(err) {
      this.logger.error('error loading browser data');
      await this.loadBrowser();
    };
  };

  async loadGoogle() {
    return this.browser.loadURL('https://www.google.com/');
  };

  async loadGoogleSearch() {
    return this.browser.loadURL('https://www.google.com/search?sxsrf=ALeKk01yfumCpM5gdDlJIppFmjvwo_7vmg%3A1590588807593&source=hp&ei=h3XOXoHIIaWO9PwP5IadyAU&q=youtube videos&oq=asd&gs_lcp=CgZwc3ktYWIQAzIECCMQJzIFCAAQkQIyAggAMgIIADICCAAyAggAMgIIADICCAAyAggAMgIIADoICAAQgwEQkQI6BQgAEIMBOgQIABAKOgcIIxDqAhAnUOQOWMFdYMFfaAVwAHgBgAFqiAGJBJIBAzUuMZgBAKABAaoBB2d3cy13aXqwAQo&sclient=psy-ab&ved=0ahUKEwjBp-6GndTpAhUlB50JHWRDB1kQ4dUDCAk&uact=5');
  };

  available() {
    this.status = RECAPTCHA_HARVESTER_STATUS.AVAILABLE;
  };

  unavailable() {
    this.status = RECAPTCHA_HARVESTER_STATUS.UNAVAILABLE;
  };

  override() {
    this.status = RECAPTCHA_HARVESTER_STATUS.OVERRIDE;
  };

  show() {
    this.browser.show();
  };

  hide() {
    this.browser.hide();
  };

  static get SUPPORTED_SITES() {
    return [
      SUPPORTED_SITES.SUPREME_INSTORE,
      SUPPORTED_SITES.SUPREME_WEBSTORE
    ];
  };
};