export enum SUPPORTED_SITES {
  SUPREME_WEBSTORE = 'Supreme Webstore',
  SUPREME_INSTORE = 'Supreme Instore',
};

export enum RECAPTCHA_URL { // these are sites supported by the captcha harvester
  'Supreme Webstore' = 'http://www.supremenewyork.com/mobile/',
  'Supreme Instore' = 'http://register.supremenewyork.com/',
};

export enum RECAPTCHA_SITEKEYS {
  'Supreme Webstore' = '6LeWwRkUAAAAAOBsau7KpuC9AV-6J8mhw4AjC3Xz',
  'Supreme Instore' = '6LfEPy8UAAAAAAVCidikQMCi4wIm_D-UMWSSJKHq',
};