import { FormArray } from '@angular/forms';
import { MatDialogConfig } from '@angular/material/dialog';

export interface TextareaDialogData {
  formArray: FormArray;
  immutableTitle: boolean;
};

export interface TextareaDialogReturn {
  0: string;
  1: string;
};

export const DEFAULT_CONFIG: MatDialogConfig  = {
  panelClass: 'dialog-container',
  autoFocus: false,
  height: '600px',
  width: '480px',
  minHeight: '600px',
  minWidth: '480px',
  closeOnNavigation: true,
  hasBackdrop: true,
};