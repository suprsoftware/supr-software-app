import { ProfileMap } from './';
import { Profile, ProfileInterface } from '../models/profile';

describe('ProfileMap', () => {
  let profileMap: ProfileMap;
  let fakeProfileInterface: ProfileInterface;

  beforeEach(() => {
    fakeProfileInterface = {name: 'Test'} as ProfileInterface;
    profileMap = new ProfileMap();
  }); 

  describe('getters', () => {
    it('should get size of map', () => {
      profileMap.setProfile(new Profile({name: 'Test1'} as ProfileInterface));
      expect(profileMap.size).toBe(1);
      profileMap.setProfile(new Profile({name: 'Test2'} as ProfileInterface));
      expect(profileMap.size).toBe(2);
    });
  });

  describe('methods', () => {

    describe('importFromInterfacesAndPush', () => {
      it('should call import and pushLatestValues', () => {
        spyOn(profileMap, 'importFromInterfaces');
        spyOn(profileMap, 'pushLatestValues');
        const fakeProfileInterfaces = [
          {name: 'Test1'} as ProfileInterface,
          {name: 'Test2'} as ProfileInterface,
          {name: 'Test3'} as ProfileInterface,
          {name: 'Test4'} as ProfileInterface,
        ];
        profileMap.importFromInterfacesAndPush(fakeProfileInterfaces);
        expect(profileMap.importFromInterfaces).toHaveBeenCalledOnceWith(fakeProfileInterfaces);
        expect(profileMap.pushLatestValues).toHaveBeenCalledTimes(1);
      });
    });

    describe('importFromInterfaces', () => {
      it('should import same amount of profiles passed in', () => {
        const fakeProfileInterfaces = [
          {name: 'Test1'} as ProfileInterface,
          {name: 'Test2'} as ProfileInterface,
          {name: 'Test3'} as ProfileInterface,
          {name: 'Test4'} as ProfileInterface,
        ];
        profileMap.importFromInterfaces(fakeProfileInterfaces);
        expect(profileMap.size).toBe(fakeProfileInterfaces.length);
      });
    });

    describe('setProfileAsInterface', () => {
      it('should set profile as interface', () => {
        profileMap.setProfileAsInterface(fakeProfileInterface);
        expect(profileMap.size).toBe(1);
        expect(profileMap.hasProfile('Test')).toBeTrue();
      });
    });

    describe('export', () => {
      it('should export profile map as array of profile interfaces', () => {
        const fakeProfileInterfaces = [
          {name: 'Test1'} as ProfileInterface,
          {name: 'Test2'} as ProfileInterface,
          {name: 'Test3'} as ProfileInterface,
          {name: 'Test4'} as ProfileInterface,
        ];
        profileMap.importFromInterfaces(fakeProfileInterfaces);
        const exportData = profileMap.export();
        expect(exportData).toBeInstanceOf(Array);
        expect(exportData.every(p => {
          return 'name' in p &&
                 'billingAddress' in p &&
                 'shippingAddress' in p &&
                 'paymentDetails' in p
        })).toBeTrue();
      });
    });

    describe('hasProfile', () => {
      it('should return boolean if profile exists in map', () => {
        expect(profileMap.hasProfile('Test')).toBeFalse();
        profileMap.setProfileAsInterface(fakeProfileInterface);
        expect(profileMap.hasProfile('Test')).toBeTrue();
      });
    });

    describe('deleteProfileAndPush', () => {
      it('should delete profile and push latest values', () => {
        spyOn(profileMap, 'deleteProfile');
        spyOn(profileMap, 'pushLatestValues');

        profileMap.deleteProfileAndPush('Test');
        expect(profileMap.deleteProfile).toHaveBeenCalledWith('Test');
        expect(profileMap.pushLatestValues).toHaveBeenCalledTimes(1);
      });
    });

    describe('deleteProfile', () => {
      it('should return true if profile was deleted', () => {
        profileMap.setProfileAsInterface(fakeProfileInterface);
        expect(profileMap.hasProfile('Test')).toBeTrue();
        expect(profileMap.deleteProfile('Test')).toBeTrue();
      });

      it('should return false if profile did not exist', () => {
        expect(profileMap.hasProfile('Test')).toBeFalse();
        expect(profileMap.deleteProfile('Test')).toBeFalse();
      });
    });

    describe('getProfile', () => {
      it('should return a reference to desited profile', () => {
        profileMap.setProfile(new Profile(fakeProfileInterface));
        expect(profileMap.hasProfile('Test')).toBeTrue();
        const profileRef = profileMap.getProfile('Test');
        profileRef.billingAddress.name = 'John Doe';
        expect(profileMap.export()[0].billingAddress.name).toBe('John Doe');
      });
    });

    describe('setProfileAndPush', () => {
      it('should set profile and push latest values', () => {
        spyOn(profileMap, 'setProfile');
        spyOn(profileMap, 'pushLatestValues');
        
        profileMap.setProfileAndPush(new Profile());
        expect(profileMap.setProfile).toHaveBeenCalledTimes(1);
        expect(profileMap.pushLatestValues).toHaveBeenCalledTimes(1);
      });
    });

    describe('setProfile', () => {
      it('should set profile', () => {
        const fakeProfile = new Profile(fakeProfileInterface);
        expect(profileMap.hasProfile('Test')).toBeFalse();
        profileMap.setProfile(fakeProfile);
        expect(profileMap.hasProfile('Test')).toBeTrue();
      });
      
    });

    describe('pushLatestValues', () => {
      it('should push the latest values', () => {
        spyOn(profileMap.profiles$, 'next');
        
        profileMap.setProfileAsInterface(fakeProfileInterface);
        profileMap.pushLatestValues();
        expect(profileMap.profiles$.next).toHaveBeenCalledTimes(1);
      });
    });

  });

});