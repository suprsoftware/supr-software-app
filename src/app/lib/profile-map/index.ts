import { BehaviorSubject } from 'rxjs';
import { Profile, ProfileInterface } from '../models/profile';

export class ProfileMap {
  profileMap: Map<string, Profile>;
  profiles$: BehaviorSubject<Profile[]>;

  constructor() {
    this.profileMap = new Map();
    this.profiles$ = new BehaviorSubject(Array.from(this.profileMap.values()));
  };

  get size(): number {
    return this.profileMap.size;
  };

  importFromInterfacesAndPush(profiles: ProfileInterface[]) {
    this.importFromInterfaces(profiles);
    this.pushLatestValues();
  };

  importFromInterfaces(profiles: ProfileInterface[]) {
    for (const p of profiles) {
      this.setProfileAsInterface(p);
    };
  };

  setProfileAsInterface(profile: ProfileInterface) {
    this.setProfile(new Profile(profile));
  };

  export(): ProfileInterface[] {
    const profilesArray = Array.from(this.profileMap.values());
    return profilesArray
      .map(p => p.toJSON());
  };

  deleteProfileAndPush(profileName: string) {
    const didDelete = this.deleteProfile(profileName);
    this.pushLatestValues();
    return didDelete;
  };

  deleteProfile(profileName: string): boolean {
    return this.profileMap.delete(profileName);
  };

  getProfile(profileName: string): Profile {
    return this.profileMap.get(profileName);
  };

  setProfileAndPush(profile: Profile) {
    this.setProfile(profile);
    this.pushLatestValues();
  };

  setProfile(profile: Profile) {
    return this.profileMap.set(profile.name, profile);
  };

  pushLatestValues() {
    this.profiles$.next(Array.from(this.profileMap.values()));
  };

  hasProfile(profileName: string): boolean {
    return this.profileMap.has(profileName);
  };

};