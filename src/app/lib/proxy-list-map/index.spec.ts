import { Observable } from 'rxjs';
import { TestScheduler } from 'rxjs/testing';
import { ProxyListMap } from './';
import { Proxy } from 'app/lib/models/proxy';
import { ProxyList } from '../models/proxy-list.model';

let proxyListMap: ProxyListMap;
let fakeImportData: [string, string[]][];
let fakeProxyList: ProxyList;

describe('ProxyListMap', () => {
  beforeEach(() => {
    proxyListMap = new ProxyListMap();
    fakeImportData = [
      ['Test', ['hostname:80', 'hostname:8080:user:pass']],
    ];
    fakeProxyList = ['Test', [new Proxy('hostname:80'), new Proxy('hostname:8080:user:pass')]];
  });

  it('should create', () => {
    expect(proxyListMap).toBeInstanceOf(ProxyListMap);
    expect(proxyListMap.size).toEqual(2);
    expect(proxyListMap.has('Localhost')).toBeTrue();
    expect(proxyListMap.has('Services')).toBeTrue();
  });

  it('should return correct size of map', () => {
    proxyListMap.import(fakeImportData);
    expect(proxyListMap.size).toEqual(3);
    proxyListMap.deleteProxyList('Test');
    expect(proxyListMap.size).toEqual(2);
    proxyListMap.deleteProxyList('Localhost');
    expect(proxyListMap.size).toEqual(2);
    proxyListMap.deleteProxyList('Services');
    expect(proxyListMap.size).toEqual(2);
  });

  it('should return correct count of proxies', () => {
    expect(proxyListMap.count).toEqual(1);
    proxyListMap.import(fakeImportData);
    expect(proxyListMap.count).toEqual(3);
    proxyListMap.deleteProxyList('Test');
    expect(proxyListMap.count).toEqual(1);
  });

  it('should return an observable of proxy lists', () => {
    expect(proxyListMap.asObservable).toBeInstanceOf(Observable);
    const scheduler = new TestScheduler((actual, expected) => {
      expect(actual).toEqual(expected);
    });
    scheduler.run(({expectObservable}) => {
      const expectedMarble = '(a)';
      const expectedProxyLists = {
        'a': [['Localhost', [Proxy.LOCALHOST]],['Services', []]],
      };
      expectObservable(proxyListMap.asObservable).toBe(expectedMarble, expectedProxyLists);
    });
  });

  it('should import', () => {
    let proxyLists$ = proxyListMap.asObservable;
    const scheduler = new TestScheduler((actual, expected) => {
      fakeImportData.push(['BadList', ['bad:proxy', 'bad:80:']]);
      proxyListMap.import(fakeImportData);
      expect(proxyListMap.size).toBe(4);
      expect(proxyListMap.count).toBe(3);
      expect(actual).toEqual(expected);
    });
    scheduler.run(({expectObservable}) => {
      const expectedMarble = '(ab)';
      const expectedProxyLists = {
        'a': [
          ['Localhost', [Proxy.LOCALHOST]],
          ['Services', []],
        ],
        'b': [
          ['Localhost', [Proxy.LOCALHOST]],
          ['Services', []],
          fakeProxyList,
          ['BadList', []],
        ],
      };
      expectObservable(proxyLists$).toBe(expectedMarble, expectedProxyLists);
    });
  });

  it('should export', () => {
    proxyListMap.setProxyList(fakeProxyList);
    const data = proxyListMap.export();
    expect(data.length).toBe(2);
    expect(data.every(e => proxyListMap.isMutable(e[0]))).toBeTrue();
    expect(data[1][0]).toEqual(fakeImportData[0][0]);
    expect(data[1][1]).toEqual(fakeImportData[0][1]);
  });

  it('should setProxyList', () => {
    expect(proxyListMap.setProxyList(fakeProxyList)).toBeTrue();
    expect(proxyListMap.setProxyList(['Services', []])).toBeTrue();
    expect(proxyListMap.setProxyList(['Localhost', [Proxy.LOCALHOST]])).toBeFalse();
    expect(proxyListMap.setProxyList(['localhost', [Proxy.LOCALHOST]])).toBeFalse();
  });

  it('should deleteProxyList', () => {
    expect(proxyListMap.setProxyList(fakeProxyList)).toBeTrue();
    expect(proxyListMap.deleteProxyList('Test')).toBeTrue();
    expect(proxyListMap.deleteProxyList('Services')).toBeFalse();
    expect(proxyListMap.deleteProxyList('Localhost')).toBeFalse();
    expect(proxyListMap.deleteProxyList('localhost')).toBeFalse();
  });

  it('should clearProxyList', () => {
    expect(proxyListMap.setProxyList(fakeProxyList)).toBeTrue();
    expect(proxyListMap.clearProxyList('Test')).toBeTrue();
    expect(proxyListMap.clearProxyList('Services')).toBeTrue();
    expect(proxyListMap.clearProxyList('Localhost')).toBeFalse();
    expect(proxyListMap.clearProxyList('localhost')).toBeFalse();
  });

  it('should pushLatestValues', () => {
    let proxyLists$ = proxyListMap.asObservable;
    const scheduler = new TestScheduler((actual, expected) => {
      proxyListMap.setProxyListAndPush(fakeProxyList);
      expect(actual).toEqual(expected);
    });
    scheduler.run(({expectObservable}) => {
      const expectedMarble = '(ab)';
      const expectedProxyLists = {
        'a': [
          ['Localhost', [Proxy.LOCALHOST]],
          ['Services', []],
        ],
        'b': [
          ['Localhost', [Proxy.LOCALHOST]],
          ['Services', []],
          fakeProxyList
        ],
      };
      expectObservable(proxyLists$).toBe(expectedMarble, expectedProxyLists);
    });
  });

  it('should setProxyListAndPush', () => {
    let proxyLists$ = proxyListMap.asObservable;
    const scheduler = new TestScheduler((actual, expected) => {
      proxyListMap.setProxyListAndPush(fakeProxyList);
      expect(actual).toEqual(expected);
    });
    scheduler.run(({expectObservable}) => {
      const expectedMarble = '(ab)';
      const expectedProxyLists = {
        'a': [
          ['Localhost', [Proxy.LOCALHOST]],
          ['Services', []],
        ],
        'b': [
          ['Localhost', [Proxy.LOCALHOST]],
          ['Services', []],
          fakeProxyList
        ],
      };
      expectObservable(proxyLists$).toBe(expectedMarble, expectedProxyLists);
    });
  });

  it('should deleteProxyListAndPush', () => {
    let proxyLists$ = proxyListMap.asObservable;
    const scheduler = new TestScheduler((actual, expected) => {
      proxyListMap.setProxyListAndPush(fakeProxyList);
      proxyListMap.deleteProxyListAndPush('Test');
      proxyListMap.deleteProxyListAndPush('Services');
      proxyListMap.deleteProxyListAndPush('Localhost');
      expect(actual).toEqual(expected);
    });
    scheduler.run(({expectObservable}) => {
      const expectedMarble = '(abcde)';
      const expectedProxyLists = {
        'a': [
          ['Localhost', [Proxy.LOCALHOST]],
          ['Services', []],
        ],
        'b': [
          ['Localhost', [Proxy.LOCALHOST]],
          ['Services', []],
          fakeProxyList
        ],
        'c': [
          ['Localhost', [Proxy.LOCALHOST]],
          ['Services', []]
        ],
        'd': [
          ['Localhost', [Proxy.LOCALHOST]],
          ['Services', []]
        ],
        'e': [
          ['Localhost', [Proxy.LOCALHOST]],
          ['Services', []]
        ],
      };
      expectObservable(proxyLists$).toBe(expectedMarble, expectedProxyLists);
    });
  });

  it('should clearProxyListAndPush', () => {
    let proxyLists$ = proxyListMap.asObservable;
    const fakeProxies = fakeProxyList[1];
    const fakeProxyListName = fakeProxyList[0];
    const scheduler = new TestScheduler((actual, expected) => {
      proxyListMap.setProxyListAndPush(['Services', fakeProxies]);
      proxyListMap.setProxyListAndPush(fakeProxyList);
      proxyListMap.clearProxyListAndPush('Services');
      proxyListMap.clearProxyListAndPush(fakeProxyListName);
      proxyListMap.clearProxyListAndPush('Localhost');
      expect(actual).toEqual(expected);
    });
    scheduler.run(({expectObservable}) => {
      const expectedMarble = '(abcdef)';
      const expectedProxyLists = {
        'a': [
          ['Localhost', [Proxy.LOCALHOST]],
          ['Services', []],
        ],
        'b': [
          ['Localhost', [Proxy.LOCALHOST]],
          ['Services', fakeProxies],
        ],
        'c': [
          ['Localhost', [Proxy.LOCALHOST]],
          ['Services', fakeProxies],
          fakeProxyList
        ],
        'd': [
          ['Localhost', [Proxy.LOCALHOST]],
          ['Services', []],
          fakeProxyList
        ],
        'e': [
          ['Localhost', [Proxy.LOCALHOST]],
          ['Services', []],
          [fakeProxyListName, []]
        ],
        'f': [
          ['Localhost', [Proxy.LOCALHOST]],
          ['Services', []],
          [fakeProxyListName, []]
        ],
      };
      expectObservable(proxyLists$).toBe(expectedMarble, expectedProxyLists);
    });
  });

  describe('isExportable', () => {
    beforeEach(() => {
      proxyListMap.setProxyList(fakeProxyList);
    });
    it('should return true if list is exportable', () => {
      expect(proxyListMap.isExportable('Services')).toBeTrue();
      expect(proxyListMap.isExportable('Test')).toBeTrue();
    });
    it('should return false if list is not exportable', () => {
      expect(proxyListMap.isExportable('Localhost')).toBeFalse();
    });
  });

  describe('isMutable', () => {
    beforeEach(() => {
      proxyListMap.setProxyList(fakeProxyList);
    });
    it('should return true if list is mutable', () => {
      expect(proxyListMap.isMutable('Services')).toBeTrue();
      expect(proxyListMap.isMutable('Test')).toBeTrue();
    });
    it('should return false if list is not mutable', () => {
      expect(proxyListMap.isMutable('Localhost')).toBeFalse();
    });
  });

  describe('isImmutable', () => {
    beforeEach(() => {
      proxyListMap.setProxyList(fakeProxyList);
    });
    it('should return true if list is immutable', () => {
      expect(proxyListMap.isImmutable('Localhost')).toBeTrue();
    });
    it('should return false if list is not immutable', () => {
      expect(proxyListMap.isImmutable('Services')).toBeFalse();
      expect(proxyListMap.isImmutable('Test')).toBeFalse();
    });
  });

  describe('isRemovable', () => {
    beforeEach(() => {
      proxyListMap.setProxyList(fakeProxyList);
    });
    it('should return true if list is removable', () => {
      expect(proxyListMap.isRemovable('Test')).toBeTrue();
    });
    it('should return false if list is not removable', () => {
      expect(proxyListMap.isRemovable('Localhost')).toBeFalse();
      expect(proxyListMap.isRemovable('Services')).toBeFalse();
    });
  });

  describe('isIrremovable', () => {
    beforeEach(() => {
      proxyListMap.setProxyList(fakeProxyList);
    });
    it('should return true if list is irremovable', () => {
      expect(proxyListMap.isIrremovable('Localhost')).toBeTrue();
      expect(proxyListMap.isIrremovable('Services')).toBeTrue();
    });
    it('should return false if list is not irremovable', () => {
      expect(proxyListMap.isIrremovable('Test')).toBeFalse();
    });
  });

  describe('has', () => {
    beforeEach(() => {
      proxyListMap.setProxyList(fakeProxyList);
    });
    it('should return true if in proxy list map', () => {
      expect(proxyListMap.has('Localhost')).toBeTrue();
      expect(proxyListMap.has('Services')).toBeTrue();
      expect(proxyListMap.has('Test')).toBeTrue();
    });
    it('should return false if not in proxy list map', () => {
      expect(proxyListMap.has('DNE')).toBeFalse();
    });
  });

  describe('getProxyList', () => {
    it('should return reference if list is mutable', () => {
      const serviceProxyList = proxyListMap.getProxyPool('Services');
      expect(serviceProxyList.length).toBe(0);
      serviceProxyList.push(new Proxy('goodhost:8080'));
      expect(serviceProxyList.length).toBe(1);
      expect(proxyListMap.getProxyPool('Services').length).toBe(1);
    });

    it('should not return reference if list is immutable', () => {
      const localhostProxyList = proxyListMap.getProxyPool('Localhost');
      expect(localhostProxyList.length).toBe(1);
      localhostProxyList.push(new Proxy('goodhost:8080'));
      expect(localhostProxyList.length).toBe(2);
      expect(proxyListMap.getProxyPool('Localhost').length).toBe(1);
    });
  });

});