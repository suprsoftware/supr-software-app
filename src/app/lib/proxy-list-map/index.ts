import { BehaviorSubject, Observable } from 'rxjs';
import { Proxy } from '../models/proxy';
import { ProxyList } from '../models/proxy-list.model';

export class ProxyListMap {
  private proxyListMap: Map<string, Proxy[]>;
  private proxyListMap$: BehaviorSubject<ProxyList[]>;

  constructor() {
    this.proxyListMap = new Map();
    this.proxyListMap.set('Localhost', [Proxy.LOCALHOST]);
    this.proxyListMap.set('Services', []);
    this.proxyListMap$ = new BehaviorSubject(Array.from(this.proxyListMap));
  };

  get size(): number {
    return this.proxyListMap.size;
  };

  get count(): number {
    return Array.from(this.proxyListMap.values())
      .reduce((prev, curr) => prev += curr.length, 0);
  };

  get asObservable(): Observable<ProxyList[]> {
    return this.proxyListMap$.pipe();
  };

  get values(): IterableIterator<Proxy[]> {
    return this.proxyListMap.values();
  };

  get keys(): IterableIterator<string> {
    return this.proxyListMap.keys();
  };

  import(data: [string, string[]][]) {
    for (let entry of data) {
      this.importEntry(entry);
    };
    this.pushLatestValues();
  };

  private importEntry(entry: [string, string[]]) {
    const [proxyListName, proxyStringList] = entry;
    if (this.isImmutable(proxyListName)) return;
    const proxies = Proxy.stringListToProxyList(proxyStringList);
    this.setProxyList([proxyListName, proxies]);
  };

  setProxyList(proxyList: ProxyList) {
    if (this.isImmutable(proxyList[0])) return false;
    const proxyListName = proxyList[0];
    const proxies = proxyList[1];
    this.proxyListMap.set(proxyListName, proxies);
    return true;
  };

  private pushLatestValues() {
    this.proxyListMap$.next(Array.from(this.proxyListMap));
  };

  setProxyListAndPush(proxyList: ProxyList) {
    this.setProxyList(proxyList);
    this.pushLatestValues();
  };

  export(): [string, string[]][] {
    const data: [string, string[]][] = [];
    for (let [name, proxies] of Array.from(this.proxyListMap)) {
      if (this.isExportable(name)) {
        data.push([name, Proxy.proxyListToStringList(proxies)]);
      };
    };
    return data;
  };

  isExportable(proxyListName: string) {
    return this.isMutable(proxyListName);
  };

  deleteProxyListAndPush(proxyListName: string) {
    this.deleteProxyList(proxyListName);
    this.pushLatestValues();
  };

  deleteProxyList(proxyListName: string) {
    if (this.isIrremovable(proxyListName)) return false;
    return this.proxyListMap.delete(proxyListName);
  };

  clearProxyListAndPush(proxyListName: string) {
    this.clearProxyList(proxyListName);
    this.pushLatestValues();
  };

  clearProxyList(proxyListName: string) {
    if (this.isImmutable(proxyListName)) return false;
    this.proxyListMap.set(proxyListName, []);
    return true;
  };

  isIrremovable(proxyListName: string) {
    return !this.isRemovable(proxyListName);
  };

  isRemovable(proxyListName: string) {
    if(proxyListName.toLowerCase() === 'localhost') return false;
    if(proxyListName.toLowerCase() === 'services') return false;
    return this.proxyListMap.has(proxyListName);
  };

  isImmutable(proxyListName: string) {
    return !this.isMutable(proxyListName);
  };

  isMutable(proxyListName: string) {
    if (proxyListName.toLowerCase() === 'localhost') return false;
    return true;
  };

  getProxyList(proxyListName: string): ProxyList {
    const proxyPool = this.getProxyPool(proxyListName);
    return [proxyListName, proxyPool];
  };

  getProxyPool(proxyListName: string) {
    if (this.isImmutable(proxyListName)) return this.copyProxies(proxyListName); 
    return this.proxyListMap.get(proxyListName);
  };

  getNewProxyListName() {
    let name = 'New Proxy List';
    if (this.has(name)) {
      name += ` ${this.size + 1}`;
    };
    return name;
  };

  has(proxyListName: string) {
    return this.proxyListMap.has(proxyListName);
  };

  copyProxies(proxyListName: string): Proxy[] {
    return this.proxyListMap.get(proxyListName)
      .map(proxy => new Proxy(proxy.proxyString));
  };
};