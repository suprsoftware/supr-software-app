const crypto = window.require('crypto');

export function generateId(bytes=5): string {
  if (bytes > 2**31 -1) return '';
  return crypto.randomBytes(bytes).toString('hex');
};