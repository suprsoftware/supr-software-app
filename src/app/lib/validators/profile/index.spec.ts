import { FormControl } from '@angular/forms';
import { ProfileValidator } from './';
import { ProfilesService } from 'app/core/services/profiles/profiles.service';

describe('ProfileValidator', () => {
  let profilesServiceSpy: jasmine.SpyObj<ProfilesService>;
  let profileValidator: ProfileValidator;

  beforeEach(() => {
    profilesServiceSpy = jasmine.createSpyObj('ProfilesService', [
      'hasProfile'
    ]);
    profileValidator = new ProfileValidator(profilesServiceSpy);
  });

  it('should create', () => {
    expect(profileValidator).toBeTruthy();
    expect(profileValidator).toBeInstanceOf(ProfileValidator);
  });

  it('should return null if valid profile name', async () => {
    profilesServiceSpy.hasProfile.and.returnValue(false);
    const control = new FormControl('Test');
    expect(await profileValidator.name(control)).toBeNull();
    expect(profilesServiceSpy.hasProfile).toHaveBeenCalledWith('Test');
    expect(profilesServiceSpy.hasProfile).toHaveBeenCalledTimes(1);
  });

  it('should return invalidProfileName if invalid profile name', async () => {
    profilesServiceSpy.hasProfile.and.returnValue(true);
    const control = new FormControl('Test');
    expect((await profileValidator.name(control)).invalidProfileName).toBeTruthy();
    expect(profilesServiceSpy.hasProfile).toHaveBeenCalledWith('Test');
    expect(profilesServiceSpy.hasProfile).toHaveBeenCalledTimes(1);
  });

});