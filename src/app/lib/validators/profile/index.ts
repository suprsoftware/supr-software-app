import { Injectable } from '@angular/core';
import { AbstractControl, ValidationErrors } from '@angular/forms';
import { Observable } from 'rxjs';

import { ProfilesService } from 'app/core/services/profiles/profiles.service';

@Injectable({providedIn: 'root'})
export class ProfileValidator {
  constructor(
    private profilesService: ProfilesService,
  ) {};

  get name() {
    return this._name.bind(this);
  };

  private _name(control: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors| null> {
    return new Promise((resolve) => {
      const value = control.value;
      if (this.profilesService.hasProfile(value)) {
        resolve({
          'invalidProfileName': 'Profile name is already in use.'
        });
      } else {
        resolve(null);
      };
    });
  };
};