import { FormControl } from '@angular/forms';
import { ProxyListValidator } from './';
import { ProxiesService } from 'app/core/services/proxies/proxies.service';

describe('ProxyListValidator', () => {
  let proxyListValidator: ProxyListValidator;
  let proxyServiceSpy: jasmine.SpyObj<ProxiesService>;

  beforeEach(() => {
    proxyServiceSpy = jasmine.createSpyObj('ProxiesService', [
      'hasProxyList',
      'isProxyListMutable'
    ]);
    proxyListValidator = new ProxyListValidator(proxyServiceSpy);
  });

  describe('name', () => {
    it('returns an function', () => {
      expect(proxyListValidator.name).toBeInstanceOf(Function);
    });

    it('returns null if valid proxy list name', async () => {
      proxyServiceSpy.hasProxyList.and.returnValue(false);
      proxyServiceSpy.isProxyListMutable.and.returnValue(true);
      const control = new FormControl('Test');
      expect(await proxyListValidator.name(control)).toBeNull();
      expect(proxyServiceSpy.hasProxyList).toHaveBeenCalledTimes(1);
      expect(proxyServiceSpy.isProxyListMutable).toHaveBeenCalledTimes(1);
    });

    it('returns invalidProxyListName if hasProxyList', async () => {
      proxyServiceSpy.hasProxyList.and.returnValue(true);
      proxyServiceSpy.isProxyListMutable.and.returnValue(true);
      const control = new FormControl('Test');
      expect((await proxyListValidator.name(control)).invalidProxyListName).toBeTruthy();
      expect(proxyServiceSpy.hasProxyList).toHaveBeenCalledTimes(1);
      expect(proxyServiceSpy.isProxyListMutable).toHaveBeenCalledTimes(1);
    });

    it('returns invalidProxyListName if isProxyListMutable', async () => {
      proxyServiceSpy.hasProxyList.and.returnValue(false);
      proxyServiceSpy.isProxyListMutable.and.returnValue(false);
      const control = new FormControl('Test');
      expect((await proxyListValidator.name(control)).invalidProxyListName).toBeTruthy();
      expect(proxyServiceSpy.hasProxyList).toHaveBeenCalledTimes(1);
      expect(proxyServiceSpy.isProxyListMutable).toHaveBeenCalledTimes(1);
    });
  });

  describe('listAsString', () => {
    it('returns null if valid proxy string', () => {
      const control = new FormControl('good:80');
      expect(ProxyListValidator.listAsString(control)).toBeNull();
      control.setValue('good:80:as:well');
      expect(ProxyListValidator.listAsString(control)).toBeNull();
    });

    it('returns invalidProxyListAsString if invalid proxy string', () => {
      const control = new FormControl('bad:string');
      expect(ProxyListValidator.listAsString(control).invalidProxyListAsString).toBeTruthy();
      control.setValue('');
      expect(ProxyListValidator.listAsString(control).invalidProxyListAsString).toBeTruthy();
      control.setValue('bad:');
      expect(ProxyListValidator.listAsString(control).invalidProxyListAsString).toBeTruthy();
      control.setValue('bad:80:');
      expect(ProxyListValidator.listAsString(control).invalidProxyListAsString).toBeTruthy();
      control.setValue('bad:80:user');
      expect(ProxyListValidator.listAsString(control).invalidProxyListAsString).toBeTruthy();
      control.setValue('bad:80:user:pass:');
      expect(ProxyListValidator.listAsString(control).invalidProxyListAsString).toBeTruthy();
      control.setValue('bad:80:user:pass: ');
      expect(ProxyListValidator.listAsString(control).invalidProxyListAsString).toBeTruthy();
      control.setValue('bad:80:user:pass:\ngood:80');
      expect(ProxyListValidator.listAsString(control).invalidProxyListAsString).toBeTruthy();
      control.setValue('bad:80:user:pass\ngood:80:');
      expect(ProxyListValidator.listAsString(control).invalidProxyListAsString).toBeTruthy();
    });
  });
});