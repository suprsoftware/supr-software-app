import { Injectable } from '@angular/core';
import { AbstractControl, ValidationErrors } from '@angular/forms';
import { Observable } from 'rxjs';

import { ProxiesService } from 'app/core/services/proxies/proxies.service';
import { Proxy } from 'app/lib/models/proxy';

@Injectable({providedIn: 'root'})
export class ProxyListValidator {
  constructor(
    private proxyService: ProxiesService,
  ) {};

  get name() {
    return this._name.bind(this);
  };

  private _name(control: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors| null> {
    return new Promise((resolve) => {
      const value = control.value;
      const notUnique = this.proxyService.hasProxyList(value);
      const immutable = !this.proxyService.isProxyListMutable(value);
      if (notUnique || immutable) {
        resolve({ 'invalidProxyListName': 'Proxy list name is a protected name or already in use.' });
      } else {
        resolve(null);
      }
    });
  };

  static listAsString(control: AbstractControl): ValidationErrors | null {
    const value: string = control.value;
    const valid = value
      .trim()
      .split('\n')
      .every(line => Proxy.isValidProxyString(line));
    if (!valid) {
      return {
        'invalidProxyListAsString': 'Invalid proxy string format.'
      };
    };
    return null;
  };
};

