import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatListModule, MatSelectionListChange } from '@angular/material/list';
import { LicenseKeyListComponent } from './license-key-list.component';

import { AuthService } from 'app/core/services/auth/auth.service';
import { of } from 'rxjs';
import { LicenseKey } from 'app/lib/models/license-key.model';


describe('LicenseKeyListComponent', () => {
  let component: LicenseKeyListComponent;
  let fixture: ComponentFixture<LicenseKeyListComponent>;
  let authService: jasmine.SpyObj<AuthService>;

  beforeEach(async () => {
    const authServiceSpy = jasmine.createSpyObj('AuthService', 
      {
        'login': () => {},
      },
      {
        'hardwareID': 'hwid',
        'keysAsObservable': of(null),
      }
    );

    await TestBed.configureTestingModule({
      declarations: [ LicenseKeyListComponent ],
      imports: [
        MatListModule
      ],
      providers: [
        { provide: AuthService, useValue: authServiceSpy }
      ]
    })
    .compileComponents();

    authService = TestBed.inject(AuthService) as jasmine.SpyObj<AuthService>;
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LicenseKeyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit the license key selection', () => {
    spyOn(component, 'onSelectionChangeHandler').and.callThrough();
    spyOn(component.keySelection, 'emit');

    let event = {
      options: [
        {
          value: {uuid: 'test'},
          selected: false,
        }
      ],
      source: {
        deselectAll: function(){}
      }
    };
    spyOnAllFunctions(event.source);
    
    component.onSelectionChangeHandler(event as MatSelectionListChange);
    expect(component.keySelection.emit).toHaveBeenCalledTimes(1);
    expect(component.keySelection.emit).toHaveBeenCalledWith(event.options[0].value as LicenseKey);
    expect(event.options[0].selected).toBeTrue();
    expect(event.source.deselectAll).toHaveBeenCalledTimes(1);
  });
});
