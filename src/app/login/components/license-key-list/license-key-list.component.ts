import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { MatSelectionListChange } from '@angular/material/list';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';

import { AuthService } from 'app/core/services/auth/auth.service';
import { LicenseKey } from 'app/lib/models/license-key.model';


@Component({
  selector: 'app-license-key-list',
  templateUrl: './license-key-list.component.html',
  styleUrls: ['./license-key-list.component.scss']
})
export class LicenseKeyListComponent implements OnInit {
  keys$: Observable<LicenseKey[]>;
  hardwareID: string;
  keySelections: LicenseKey[];

  @Output() keySelectedEvent = new EventEmitter<LicenseKey>();

  constructor(
    private auth: AuthService
  ) { 
    this.hardwareID = this.auth.hardwareID;
    this.keys$ = this.auth.keysAsObservable.pipe();
  }

  ngOnInit(): void {
    this.keys$.pipe(
      first()
    ).subscribe(keys => {
      const firstKey = keys[0];
      this.keySelections = [firstKey];
      this.keySelectedEvent.emit(firstKey);
    });
  }

  onSelectionChangeHandler(event: MatSelectionListChange) {
    const {options, source} = event;
    source.deselectAll();
    options[0].selected = true;
    this.keySelectedEvent.emit(options[0].value);
  };

  compareWithHandler(option: LicenseKey, selected: LicenseKey) {
    return option.uuid === selected.uuid;
  };

}
