import { Component } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { AuthService } from 'app/core/services/auth/auth.service';
import { SnackBarService } from 'app/core/services/snack-bar/snack-bar.service';

import { LoginComponent } from './login.component';

import { BehaviorSubject, of } from 'rxjs';
import { LicenseKey } from 'app/lib/models/license-key.model';

import firebase from 'firebase/app';
import 'firebase/auth';

@Component({selector: 'app-traffic-lights', template: ''})
class TrafficLightsComponent {
}

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let router: jasmine.SpyObj<Router>;
  let authService: jasmine.SpyObj<AuthService>;
  let snackbarService: jasmine.SpyObj<SnackBarService>;
  let loading$: BehaviorSubject<boolean> = new BehaviorSubject(false);

  beforeEach(async () => {  
    window.sessionStorage.clear();
    const routerSpy = jasmine.createSpyObj('Router', ['navigate']);
    const authServiceSpy = jasmine.createSpyObj('AuthService', {
      'login': () => {},
      'logout': () => {},
      'setAsActiveKey': (key: LicenseKey) => {}
    }, {
      'loading$': loading$,
      'userAsObservable': of(null),
    });
    const snackbarServiceSpy = jasmine.createSpyObj('SnackBarService', [
      'openGood',
      'openBad'
    ])

    await TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule,
        MatSnackBarModule,
        RouterTestingModule,
      ],
      declarations: [ LoginComponent, TrafficLightsComponent ],
      providers: [
        {provide: Router, useValue: routerSpy},
        {provide: AuthService, useValue: authServiceSpy},
        {provide: SnackBarService, useValue: snackbarServiceSpy}
      ]
    })
    .compileComponents();
    router = TestBed.inject(Router) as jasmine.SpyObj<Router>;
    authService = TestBed.inject(AuthService) as jasmine.SpyObj<AuthService>;
    snackbarService = TestBed.inject(SnackBarService) as jasmine.SpyObj<SnackBarService>;
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should init with null as selectedKey', () => {
    expect(component.selectedKey).toBeNull();
  });

  it('should get last visited page', async () => {
    expect(await component.getLastVisitedPage()).toBe('/dashboard/tasks');
    window.sessionStorage.setItem('lastVisitedPage', '#/dashboard/settings');
    expect(await component.getLastVisitedPage()).toBe('/dashboard/settings');
  });

  it('navigates to /dashboard/tasks without lastVisitedPage', async () => {
    await component.goToDashboard();
    expect(router.navigate).toHaveBeenCalledTimes(1);
    expect(router.navigate).toHaveBeenCalledWith(['/dashboard/tasks']);
  });

  it('navigates to dashboard/settings with lastVisitedPage', async () => {
    window.sessionStorage.setItem('lastVisitedPage', '#/dashboard/settings');
    await component.goToDashboard();
    expect(router.navigate).toHaveBeenCalledTimes(1);
    expect(router.navigate).toHaveBeenCalledWith(['/dashboard/settings']);
  });

  it('sets selected key onKeySelectionHandler', () => {
    const fakeKey = {
      uuid: 'fake'
    } as LicenseKey;
    component.onKeySelectionHandler(fakeKey);
    expect(component.selectedKey).toBe(fakeKey);
  });

  it('logs user out', async () => {
    await component.logout();
    expect(authService.logout).toHaveBeenCalledTimes(1);
  });

  it('removes lastVisitedPage on logout', async () => {
    spyOn(window.sessionStorage, 'removeItem');
    await component.logout();
    expect(window.sessionStorage.removeItem).toHaveBeenCalledOnceWith('lastVisitedPage');

  });

  it('logs user in', () => {
    authService.login
      .and.returnValue(Promise.resolve({} as firebase.auth.UserCredential));
    component.loginForm.controls['email'].setValue('email');
    component.loginForm.controls['password'].setValue('password');
    component.login();
    expect(authService.login).toHaveBeenCalledWith('email', 'password');
  });

  it('form control password can be reset', () => {
    component.loginForm.controls['password'].setValue('password');
    component.loginForm.controls['password'].reset('');
    expect(component.loginForm.controls['password'].value).toBe('');
  });

  it('sets invalid email error', () => {
    spyOn(component.loading$, 'next');
    component.loginErrorHandler({
      code: 'auth/invalid-email'
    });
    expect(component.loading$.next).toHaveBeenCalledWith(false);
    expect(component.loginForm.controls['email'].errors.invalidEmail).toBeTrue();
    expect(snackbarService.openBad).toHaveBeenCalledTimes(1);
  });

  it('sets disabled user error', () => {
    spyOn(component.loading$, 'next');
    component.loginErrorHandler({
      code: 'auth/user-disabled'
    });
    expect(component.loading$.next).toHaveBeenCalledWith(false);
    expect(component.loginForm.controls['email'].errors.disabledUser).toBeTrue();
    expect(snackbarService.openBad).toHaveBeenCalledTimes(1);
  });

  it('sets invalid user error', () => {
    spyOn(component.loading$, 'next');
    component.loginErrorHandler({
      code: 'auth/user-not-found'
    });
    expect(component.loading$.next).toHaveBeenCalledWith(false);
    expect(component.loginForm.controls['email'].errors.invalidUser).toBeTrue();
    expect(snackbarService.openBad).toHaveBeenCalledTimes(1);
  });

  it('sets invalid password error', () => {
    spyOn(component.loading$, 'next');
    component.loginErrorHandler({
      code: 'auth/wrong-password'
    });
    expect(component.loading$.next).toHaveBeenCalledWith(false);
    expect(component.loginForm.controls['password'].errors.invalidPassword).toBeTrue();
    expect(snackbarService.openBad).toHaveBeenCalledTimes(1);
  });

  

});
