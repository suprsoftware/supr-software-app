import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';

import { AuthService } from 'app/core/services/auth/auth.service';
import { SnackBarService } from 'app/core/services/snack-bar/snack-bar.service';

import { LicenseKey } from '../lib/models/license-key.model';
import { User } from '../lib/models/user.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  selectedKey: LicenseKey;
  loading$: BehaviorSubject<boolean>;
  user$: Observable<User>;

  constructor(
    private auth: AuthService,
    private router: Router,
    private snackBar: SnackBarService,
  ) { 
    this.loading$ = this.auth.loading$;
    this.user$ = this.auth.userAsObservable;
  }

  ngOnInit(): void {
    this.selectedKey = null;
    this.loginForm  = new FormGroup({
      email: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });
  }

  login() {
    const {email, password} = this.loginForm.value;
    this.auth.login(email, password)
      .catch(this.loginErrorHandler)
      .finally(() => {
        this.loginForm.controls['password'].reset('');
      })
  };

  loginErrorHandler(err) {
    this.loading$.next(false);
    switch (err.code) {
      case 'auth/invalid-email':
        this.loginForm.controls['email'].setErrors({'invalidEmail': true});
        this.snackBar.openBad('Invalid email');
        break;
      case 'auth/user-disabled':
        this.loginForm.controls['email'].setErrors({'disabledUser': true});
        this.snackBar.openBad('Disabled user');
        break;
      case 'auth/user-not-found':
        this.loginForm.controls['email'].setErrors({'invalidUser': true});
        this.snackBar.openBad('Invalid user');
        break;
      case 'auth/wrong-password':
        this.loginForm.controls['password'].setErrors({'invalidPassword': true});
        this.snackBar.openBad('Invalid password');
        break;
    };
  };

  async logout() {
    window.sessionStorage.removeItem('lastVisitedPage');
    await this.auth.logout();
  };

  onKeySelectedHandler(event: LicenseKey) {
    this.selectedKey = event;
  };

  async goToDashboard() {
    this.loading$.next(true);
    await this.auth.setAsActiveKey(this.selectedKey);
    const lastVisitedPage = await this.getLastVisitedPage();
    await this.router.navigate([lastVisitedPage]);
    this.loading$.next(false);
  };

  async getLastVisitedPage() {
    let lastVisitedPage = window.sessionStorage.getItem('lastVisitedPage');
    if (!!lastVisitedPage) return lastVisitedPage.slice(1);
    return '/dashboard/tasks';
  };
}
