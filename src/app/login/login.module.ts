import { NgModule } from '@angular/core';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatListModule } from '@angular/material/list';

import { CoreModule } from 'app/core/core.module';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { SharedModule } from '../shared/shared.module';
import { LicenseKeyListComponent } from './components/license-key-list/license-key-list.component';


@NgModule({
  declarations: [
    LoginComponent, 
    LicenseKeyListComponent
  ],
  imports: [
    CoreModule,
    SharedModule, 
    LoginRoutingModule,
    MatProgressSpinnerModule,
    MatListModule,
  ],
  exports: [
  ]
})
export class LoginModule {}
