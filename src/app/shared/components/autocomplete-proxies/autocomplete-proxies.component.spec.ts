import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { ProxiesService } from 'app/core/services/proxies/proxies.service';

import { AutocompleteProxiesComponent } from './autocomplete-proxies.component';

describe('AutocompleteProxiesComponent', () => {
  let component: AutocompleteProxiesComponent;
  let fixture: ComponentFixture<AutocompleteProxiesComponent>;
  let proxiesService: jasmine.SpyObj<ProxiesService>;

  beforeEach(async () => {
    const proxiesSpy = jasmine.createSpyObj('ProxiesService', [], {
      allProxiesAndProxyListNames: []
    });

    await TestBed.configureTestingModule({
      declarations: [ AutocompleteProxiesComponent ],
      imports: [
        MatAutocompleteModule,
        FormsModule,
        ReactiveFormsModule,
      ],
      providers: [
        {provide: ProxiesService, useValue: proxiesSpy}
      ]
    })
    .compileComponents();

    proxiesService = TestBed.inject(ProxiesService) as jasmine.SpyObj<ProxiesService>;
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AutocompleteProxiesComponent);
    component = fixture.componentInstance;
    component.control = new FormControl('Localhost');
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
