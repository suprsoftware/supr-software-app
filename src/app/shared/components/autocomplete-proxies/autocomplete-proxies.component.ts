import { Component, OnInit, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { ProxiesService } from 'app/core/services/proxies/proxies.service';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

@Component({
  selector: 'app-autocomplete-proxies',
  templateUrl: './autocomplete-proxies.component.html',
  styleUrls: ['./autocomplete-proxies.component.scss']
})
export class AutocompleteProxiesComponent implements OnInit {
  @Input() control: FormControl;
  @Input() optionSelectedHandler: ($event: MatAutocompleteSelectedEvent) => void;

  filteredOptions: Observable<string[]>;

  constructor(
    private proxiesService: ProxiesService,
  ) { }

  ngOnInit(): void {
    this.filteredOptions = this.control.valueChanges
      .pipe(
        startWith(''),
        map((value: string) => value.toLowerCase()),
        map(filteredValue => {
          const allOptions = this.proxiesService.allProxiesAndProxyListNames
            .filter(value => value.toLowerCase().includes(filteredValue));
          return [...new Set(allOptions)];
        })
      );
  }

}
