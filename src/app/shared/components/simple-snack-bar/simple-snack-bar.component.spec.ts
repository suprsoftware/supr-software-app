import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MAT_SNACK_BAR_DATA } from '@angular/material/snack-bar';

import { SimpleSnackBarComponent } from './simple-snack-bar.component';

describe('SimpleSnackBarComponent', () => {
  let component: SimpleSnackBarComponent;
  let fixture: ComponentFixture<SimpleSnackBarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SimpleSnackBarComponent ],
      providers: [
        {provide: MAT_SNACK_BAR_DATA, useValue: 'test'}
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SimpleSnackBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should use injected data', () => {
    expect(component.data).toBe('test');
  });

  it('should display injected data', () => {
    const nativeElement: HTMLElement = fixture.nativeElement;
    const span = nativeElement.querySelector('span');
    expect(span.textContent).toEqual('test');
  });
});
