import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { FormArray, FormControl, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';

import { TextareaDialogComponent } from './textarea-dialog.component';
import { TextareaDialogData, TextareaDialogReturn } from 'app/lib/models/textarea-dialog.model';
import { ProxyListValidator } from 'app/lib/validators/proxy-list';

describe('TextareaDialogComponent', () => {
  let component: TextareaDialogComponent;
  let fixture: ComponentFixture<TextareaDialogComponent>;

  let matDialogRef: jasmine.SpyObj<MatDialogRef<TextareaDialogComponent, TextareaDialogReturn>>;
  let testData: TextareaDialogData;

  beforeEach(async () => {
    const matDialogRefSpy = jasmine.createSpyObj('MatDialogRef', [
      'close'
    ]);
    
    testData = {
      formArray: new FormArray([
        new FormControl('List', [Validators.required]),
        new FormControl('host:80', [Validators.required, ProxyListValidator.listAsString]),
      ]),
      immutableTitle: true
    };
    
    await TestBed.configureTestingModule({
      declarations: [ TextareaDialogComponent ],
      providers: [
        { provide: MatDialogRef, useValue: matDialogRefSpy },
        { provide: MAT_DIALOG_DATA, useValue: testData }
      ],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        MatDialogModule,
        MatIconModule
      ]
    })
    .compileComponents();

    matDialogRef = TestBed.inject(MatDialogRef) as jasmine.SpyObj<MatDialogRef<TextareaDialogComponent, TextareaDialogReturn>>;
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TextareaDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should close', () => {
    component.close();
    expect(matDialogRef.close).toHaveBeenCalledTimes(1);
    expect(matDialogRef.close).toHaveBeenCalledWith();
  });

  describe('should disable first control of formArray if immutableTitle', () => {
    beforeEach(() => {
      testData.formArray.controls[0].enable();
    });
    it('is true', () => {
      testData.immutableTitle = true;
      fixture = TestBed.createComponent(TextareaDialogComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
      expect(component.data.formArray.controls[0].disabled).toBeTrue();
    });

    it('is false', () => {
      testData.immutableTitle = false;
      fixture = TestBed.createComponent(TextareaDialogComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
      expect(component.data.formArray.controls[0].disabled).toBeFalse();
    });
  });

  it('should save', () => {
    component.save();
    expect(matDialogRef.close).toHaveBeenCalledTimes(1);
    expect(matDialogRef.close).toHaveBeenCalledWith(['List', 'host:80']);
  });

  it('should disable form on empty text area', () => {
    component.data.formArray.controls[1].setValue('');
    expect(component.data.formArray.invalid).toBeTrue();
  });

  it('should disable form on malformed proxyString', () => {
    component.data.formArray.controls[1].setValue('bad:proxy');
    expect(component.data.formArray.invalid).toBeTrue();
  });

});
