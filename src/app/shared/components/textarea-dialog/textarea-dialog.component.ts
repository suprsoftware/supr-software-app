import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material/dialog';


import { TextareaDialogData, TextareaDialogReturn, DEFAULT_CONFIG } from 'app/lib/models/textarea-dialog.model';

@Component({
  selector: 'app-textarea-dialog',
  templateUrl: './textarea-dialog.component.html',
  styleUrls: ['./textarea-dialog.component.scss']
})
export class TextareaDialogComponent implements OnInit {
  constructor(
    private dialogRef: MatDialogRef<TextareaDialogComponent, TextareaDialogReturn>,
    @Inject(MAT_DIALOG_DATA) public data: TextareaDialogData
  ) { 
    if (data.immutableTitle) data.formArray.controls[0].disable();
  }

  ngOnInit(): void {
  }

  close() {
    this.dialogRef.close();
  };

  save() {
    const result: [string, string] = [
      this.data.formArray.controls[0].value,
      this.data.formArray.controls[1].value,
    ];
    this.dialogRef.close(result as TextareaDialogReturn);
  };

}
