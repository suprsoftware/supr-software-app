import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ElectronService } from 'app/core/services/electron/electron.service';

import { TrafficLightsComponent } from './traffic-lights.component';

describe('TrafficLightsComponent', () => {
  let component: TrafficLightsComponent;
  let fixture: ComponentFixture<TrafficLightsComponent>;
  let electronServiceSpy:  jasmine.SpyObj<ElectronService>;

  beforeEach(async () => {
    const spy = jasmine.createSpyObj('ElectronService', [
      'closeWindow',
      'minWindow',
      'maxWindow',
    ]);
    await TestBed.configureTestingModule({
      declarations: [ TrafficLightsComponent ],
      providers: [
        {provide: ElectronService, useValue: spy}
      ],
    })
    .compileComponents();

    electronServiceSpy = TestBed.inject(ElectronService) as jasmine.SpyObj<ElectronService>;
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TrafficLightsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should close window', () => {
    component.closeWindow();
    expect(electronServiceSpy.closeWindow.calls.count()).toBe(1);
  });

  it('should minimize window', () => {
    component.minWindow();
    expect(electronServiceSpy.minWindow.calls.count()).toBe(1);
  });

  it('should maximize window', () => {
    component.maxWindow();
    expect(electronServiceSpy.maxWindow.calls.count()).toBe(1);
  });
});
