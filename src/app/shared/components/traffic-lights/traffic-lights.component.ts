import { Component, OnInit } from '@angular/core';

import { ElectronService } from 'app/core/services/electron/electron.service';

@Component({
  selector: 'app-traffic-lights',
  templateUrl: './traffic-lights.component.html',
  styleUrls: ['./traffic-lights.component.scss']
})
export class TrafficLightsComponent implements OnInit {

  constructor(
    private electron: ElectronService
  ) { }

  ngOnInit(): void {
  }

  closeWindow(): void {
    this.electron.closeWindow();
  };

  minWindow(): void {
    this.electron.minWindow();
  };
  
  maxWindow(): void {
    this.electron.maxWindow();
  };
}
