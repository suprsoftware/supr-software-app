import { CreditCardNumberPipe } from './credit-card-number.pipe';

describe('CreditCardNumberPipe', () => {
  const pipe = new CreditCardNumberPipe();

  it('pipes "" to ""', () => {
    expect(pipe.transform('')).toBe('x-');
  });

  it('pipes "123" to "123"', () => {
    expect(pipe.transform('123')).toBe('x-123');
  });

  it('pipes "0123456789101112" to "x-1112"', () => {
    expect(pipe.transform('0123456789101112')).toBe('x-1112');
  });

});