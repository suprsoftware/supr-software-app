import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'creditCardNumber'})
export class CreditCardNumberPipe implements PipeTransform {
  transform(value: string): string {
    return `x-${value.slice(-4)}`;
  };
};