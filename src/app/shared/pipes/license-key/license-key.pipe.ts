import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'licenseKey'})
export class LicenseKeyPipe implements PipeTransform {
  transform(value: string): string {
    let keyFrags = value.split('-');
    let keyFragLength = keyFrags.length;
    return keyFrags
      .map((frag: string, i: number) => {
        if (i === keyFragLength-1) return frag;
        return frag.split('').map(c => 'X').join('');
      })
      .join('-');
  };
}