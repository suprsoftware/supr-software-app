import { NgModule } from '@angular/core'; 
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { CoreModule } from 'app/core/core.module';

import { 
  PageNotFoundComponent, 
  TrafficLightsComponent, 
  SimpleSnackBarComponent,
  TextareaDialogComponent,
  AutocompleteProxiesComponent
} from './components/';

import { WebviewDirective } from './directives/';

import { 
  LicenseKeyPipe, 
  CreditCardNumberPipe 
} from './pipes';

import { DEFAULT_CONFIG } from 'app/lib/models/textarea-dialog.model';
import { MatAutocompleteModule } from '@angular/material/autocomplete';


@NgModule({
  declarations: [
    WebviewDirective,
    LicenseKeyPipe,
    CreditCardNumberPipe,
    PageNotFoundComponent, 
    TrafficLightsComponent, 
    SimpleSnackBarComponent,
    TextareaDialogComponent,
    AutocompleteProxiesComponent,
  ],
  imports: [
    CoreModule,
    MatIconModule,
    MatDialogModule,
    MatSnackBarModule,
    MatAutocompleteModule,
  ],
  exports: [
    WebviewDirective,
    LicenseKeyPipe,
    CreditCardNumberPipe,
    PageNotFoundComponent, 
    TrafficLightsComponent, 
    SimpleSnackBarComponent,
    TextareaDialogComponent,
    AutocompleteProxiesComponent
  ],
  providers: [
    {provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: DEFAULT_CONFIG}
  ]
})
export class SharedModule {}
